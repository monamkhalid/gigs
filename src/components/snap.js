import React from 'react';
import { TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class Snap extends React.Component {

  constructor(props){
    super(props);
    this.state={
      search: true
    }
  }

  componentDidMount(){
  }

  search_data = () =>{
    if (this.state.search){
      return(
        <View style={{flex:7}}>
          <View style={{flex:4.5,justifyContent:'center',alignItems:'center',width: windowWidth-45}}>
            <Image
              style={{
                height :90,
                width: 130
              }}
              source={require('../../assets/carcrash.png')}
            />
            <Text style={{marginTop:5,fontSize:20,fontWeight:'bold',color:'orange'}}>Oh Snap!</Text>
            <Text style={{marginTop:5,fontSize:10,fontWeight:'bold',color:'grey'}}>
              We haven't found a professional service in a category yet,However we are contantly adding new providers.
            </Text>
          </View>
          <View style={{flex:3,justifyContent:'center',alignItems:'center',width: windowWidth-45}}>
            <Image
              style={{
                height :90,
                width: 130
              }}
              source={require('../../assets/logo-with-text.png')}
            />
            <Text style={{marginTop:5,fontSize:10,fontWeight:'bold',color:'grey'}}>
              We haven't found a professional service in a category yet,However we are contantly adding new providers.
            </Text>
            <Text style={{marginTop:15,fontSize:20,fontWeight:'bold',color:'orange'}}>Recommend Provider</Text>
          </View>
          <View style={{flex:0.5}}>
          </View>
        </View>
      );
    }else{
      return(
        <View style={{flex:7}}>
          <View style={{flex:3.5,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :130,
                width: 150
              }}
              source={require('../../assets/sorrynotfound.png')}
            />
          </View>
          <View style={{flex:2.5,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:20,fontWeight:'bold'}}>Aw Shucks!</Text>
            <Text style={{fontSize:16}}>There are no result found.</Text>
            <Text style={{fontSize:16}}>Please search using another search word.</Text>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity style={{height:50,width:windowWidth-90,backgroundColor:'orange',justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:16,color:'white'}}>Search Again</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.5}}>
          </View>
        </View>
      );
    }
  }

  show_icon = () =>{
    return(
      <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
        <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
        <Icon
          name='highlight-off'
          size={30}
          containerStyle={{alignItems:'flex-end'}}
          color= 'grey' />
        </TouchableOpacity>
      </View>
    )
  }

  show_text = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
        </View>
      );
    }else{
      return null;
    }
  }

  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        
        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:1.15}}>
            {
              this.show_icon()
            }
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-70}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6}}>
            {this.show_text()}
          </View>
          <View style={{flex:0.2}}></View>
        </View>
        <View style={{flex:8}}>
          <View style={{flex:7}}>
            {this.search_data()}
          </View>
          <View style={{flex:1,flexDirection:'row',width: windowWidth-70}}>
            <View style={{flex:0.2}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-back'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
            <Icon
                name='home'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
            <Icon
                name='search'
                color= 'orange' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'orange'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
                name='favorite-border'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
            <Icon
                name='account-circle'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'}
});
