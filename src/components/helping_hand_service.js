import React from 'react';
import { ListView,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class HelpingHandServ extends React.Component {

  constructor(props){
    super(props);
    this.state={
      search: true,
      isLoading:false,
      modalVisible: false,
      description: "Hi i am John. if You need any help Putting up your sollar system. I can do it , i only ask for a donation of $ 50.00 . But would love to help, if i am in your area.My wife is a sapnish tutor if you need that service just ask We'll be a Southearn Arizona through 5/19.",
      first_para: "Helping Hand is a page dedicated to Nomads whoare willing to help ther Nomads by offerring whatever service or skill they possesss, at no charge or for a small donation to be listed in this page.",
      first_head: "You can not own a cpmpany that charges for your services.",
      second_head: "You can not charge any significient for your Help.",
      second_para: "There is no fee to list  your service on this page. Subscriber and Provider will be able to quickly find out if you are in close proximity  of eact other so you can hopefully connect.",
      third_para: "PAY IT FORWARD BY JUST OFFERING HELPNG HAND TO YOUR FOLLOW NAMAD!"
      // selected_item: this.props.navigation.state.param.id
    }
    // this.populateList()
  }

  set_token = async() =>{
    var tokken = await AsyncStorage.getItem('token') 
    await this.setState({token: tokken})
    this.get_data()
  }
  
  login_request = (id) =>{
    console.log(id)
    try{
      fetch(BasePath+'getallusers?cid='+id, {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.populateList(responseJson.result)
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  componentDidMount(){
  }

  populateList(dataSource) {
    
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.setState({dataSource:ds.cloneWithRows(dataSource),isLoading:true});
    this.state.db = dataSource
  };

  show_modal_data = () =>{
    return(
      <View style={styles.modal_container}>
        <View style={styles.modal}>
          <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
                <Icon
                  name='highlight-off'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
              <Text style={{textAlign:'center',fontSize:14,color:'orange'}}>What is Helping Hand?</Text>
              <View style={{margin:10}}>
                <Text style={{textAlign:'center',fontSize:10,color:'grey'}}>{this.state.first_para}</Text>
              </View>
              <View style={{margin:10,flexDirection:'row'}}>
                <Icon
                  name='keyboard-arrow-right'
                  iconStyle={{marginRight:5}}
                  size={14}
                  color= 'orange' />
                <Text style={{textAlign:'center',fontSize:10,color:'grey'}}>{this.state.first_head}</Text>
              </View>
              <View style={{margin:10,flexDirection:'row'}}>
                <Icon
                  name='keyboard-arrow-right'
                  iconStyle={{marginRight:5}}
                  size={14}
                  color= 'orange' />
                <Text style={{textAlign:'center',fontSize:10,color:'grey'}}>{this.state.second_head}</Text>
              </View>
              <View style={{margin:10}}>
                <Text style={{textAlign:'center',fontSize:10,color:'grey'}}>{this.state.second_para}</Text>
              </View>
              <View style={{margin:10}}>
                <Text style={{textAlign:'center',fontSize:12,color:'orange'}}>{this.state.third_para}</Text>
              </View>

            </View>
        </View>
      </View>
    )
  }
    

  search_data = () =>{
    if (this.state.search){
      return(
        <View style={{flex:7}}>
          <View style={{flex:3,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :100,
                width: 180
              }}
              source={require('../../assets/logo-with-text.png')}
            />
          </View>
          <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:20,fontWeight:'bold',color:'orange'}}>Search for a professional</Text>
            <Text style={{fontSize:20,fontWeight:'bold',color:'orange'}}>Provider near you</Text>
          </View>
          <View style={{flex:2}}>
            <Form>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{fontSize: 10,color: 'grey'}} >(Search by Name, Type of Services, Market or Zip Code)</Label>
                <Input />
              </Item>
              <Icon
                name='search'
                size={40}
                iconStyle={{position:'relative',top:-35}}
                containerStyle={{width:windowWidth-45,justifyContent:'flex-end',alignItems:'flex-end'}}
                color= 'orange' />
            </Form>
          </View>
          <View style={{flex:1}}>
          </View>
        </View>
      );
    }else{
      return(
        <View style={{flex:7}}>
          <View style={{flex:3.5,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :130,
                width: 150
              }}
              source={require('../../assets/sorrynotfound.png')}
            />
          </View>
          <View style={{flex:2.5,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:20,fontWeight:'bold'}}>Aw Shucks!</Text>
            <Text style={{fontSize:16}}>There are no result found.</Text>
            <Text style={{fontSize:16}}>Please search using another search word.</Text>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity style={{height:50,width:windowWidth-90,backgroundColor:'orange',justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:16,color:'white'}}>Search Again</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.5}}>
          </View>
        </View>
      );
    }
  }

  show_icon = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-start',marginBottom:4}}>
          <Image
            style={{
              height :40,
              width: 40
            }}
            source={require('../../assets/helping.png')}
          />
        </View>
      )
    }else{
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
          <Icon
            name='highlight-off'
            size={30}
            containerStyle={{alignItems:'flex-end'}}
            color= 'grey' />
        </View>
      )
    }
  }

  show_text = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
          <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
          <View><Text style={{fontSize: 14}}>Helping Hand</Text></View>
          <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
          <Text style={{fontSize: 10}}>Nomads Helping Nomads</Text>
        </View>
      );
    }else{
      return null;
    }
  }

  show_images = () =>{
    return(
      <View style={styles.containerWrap}>
        <ScrollView horizontal={true}>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :70,
                width: 70
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :70,
                width: 70
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :70,
                width: 70
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :100,
                width: 100
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :70,
                width: 70
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
        </ScrollView>
      </View>
    );
  }


  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => 
            this.setState({modalVisible:false})
          }
        >
          {this.show_modal_data()}
        </Modal>
        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:1.15}}>
            {
              this.show_icon()
            }
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6}}>
            {this.show_text()}
          </View>
          <View style={{flex:0.2,justifyContent: 'flex-start',alignItems: 'center'}}>
            <TouchableOpacity>
              <Icon
                name='arrow-forward'
                containerStyle={{}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Next</Text>
          </View>
        </View>
        
        <View style={{flex:8,backgroundColor:'lightgrey'}}>
          <View style={{justifyContent:'center',alignItems:'center',flex:1,backgroundColor:'grey',margin:10}}>
            <Text style={{color:'white'}}>Banner</Text>
          </View>
          <View style={{justifyContent:'center',alignItems:'center',flex:1,backgroundColor:'white'}}>
            <Text style={{color:'orange',fontSize:14}}>Need Professional  Help? Short on Cash?</Text>
            <TouchableOpacity onPress={() => this.setState({modalVisible: true})}>
              <Text style={{color:'orange',fontSize:11}}>What is Helping Hand?</Text>
            </TouchableOpacity>
          </View>
          <View style={{width:windowWidth,flex:5,marginTop:10,backgroundColor:'white'}}>
            <ScrollView style={{width: windowWidth}}>
              <View style={{flex:1,marginTop:10}}>
                <View style={{flex:1,flexDirection:'row'}}>
                  <View style={{flex: 0.3,justifyContent:'center',alignItems:'center'}}>
                    <Image
                      style={{
                        height :70,
                        width: 70
                      }}
                       source={{uri: "https://dummyimage.com/600x600/000/fff"}}
                    />
                  </View>
                  <View style={{flex: 0.7,}}>
                    <View style={{flex: 0.7,justifyContent:'flex-start',alignItems:'flex-start',flexDirection:'row'}}>
                      <View style={{flex: 0.5,justifyContent:'center',alignItems:'flex-start'}}>
                        <Text style={{fontWeight:'bold'}}>John Johnson</Text>
                      </View>
                      <View style={{flex: 0.5,justifyContent:'center',alignItems:'flex-start'}}>
                        <Text style={{fontSize:10,color: 'lightgrey',fontWeight:'bold'}}>Currently 5 miles away</Text>
                      </View>
                    </View>
                    <View style={{flex: 0.7,justifyContent:'flex-start',alignItems:'flex-start',flexDirection:'row'}}>
                      <View style={{flex: 0.5,justifyContent:'center',alignItems:'flex-start'}}>
                        <Text style={{fontSize:10,color: 'grey',fontWeight:'bold',marginTop:5}}>Solar . Spanish Tutor</Text>
                      </View>
                      <View style={{flex: 0.5,justifyContent:'center',alignItems:'flex-start'}}>
                        <Text style={{fontSize:10,color: 'lightgrey',fontWeight:'bold'}}>Type of Rig : <Text style={{fontSize:10,color: 'grey',fontWeight:'bold'}}>Class A</Text></Text>
                      </View>
                    </View>
                    <View style={{flex: 1,justifyContent:'center',alignItems:'flex-start'}}>
                      <Text style={{fontWeight:'bold'}}>HOW I CAN HELP: <Text style={{fontSize:10,color: 'grey',}}>{this.state.description}</Text></Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{flex:1.5,marginTop:15,flexDirection:'row'}}>
                <View style={{flex:0.45,justifyContent:'center',alignItems:'center'}}>
                  <View>
                    <View style={{flex:1,flexDirection:'row'}}>
                      <TouchableOpacity style={{flex:0.3}}>
                        <Text style={{fontSize:10,color: 'orange'}}>Call</Text>
                      </TouchableOpacity>
                      <View style={{flex:0.05}}></View>
                      <TouchableOpacity style={{flex:0.3}}>
                        <Text style={{fontSize:10,color: 'orange'}}>Email</Text>
                      </TouchableOpacity>
                      <View style={{flex:0.05}}></View>
                      <TouchableOpacity style={{flex:0.3}}>
                        <Text style={{fontSize:10,color: 'orange'}}>Text</Text>
                      </TouchableOpacity>
                    </View>
                    <Text style={{fontWeight:'bold'}}>John Johnson</Text>
                  </View>
                </View>
                <View style={{flex:0.05}}></View>
                <View style={{flex:0.50}}>
                  <TouchableOpacity style={{height: 40,justifyContent:'center',alignItems:'center',backgroundColor:'orange',width: (windowWidth-40)/2}} >
                    <Text style={{fontSize:10,color: 'white'}}>Share What you think of</Text>
                    <Text style={{color: 'white'}}>John Johnson</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
          <View style={{flex:1,flexDirection:'row',width: windowWidth,backgroundColor:'white'}}>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-start',marginLeft:15}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-back'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-end',marginRight:20}}>
              <TouchableOpacity >
                <Icon
                  name='arrow-forward'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Next</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
            <Icon
                name='home'
                color= 'orange' />
            </TouchableOpacity>
            <Text style={{fontSize:12,color:'orange'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
            <Icon
                name='search'

                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
                name='favorite-border'
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
            <Icon
                name='account-circle'
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer:{
    flex:8,
    backgroundColor:"white",
    paddingVertical:15,
    marginBottom:10,
    width:windowWidth
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  iconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'lightgrey'
  },
  fbiconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'orange'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:400,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  containerWrap: {
  marginTop:5,
  flex: 3,
  flexDirection: 'row',
  backgroundColor: 'white'
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'}
});
