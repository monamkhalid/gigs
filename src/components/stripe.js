import React from 'react';
import { AsyncStorage,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class Stripe extends React.Component {

  constructor(props){
    super(props);
    this.state={
      card: '',
      cvc: '',
      expiry: '',
      card_holder:'',
      postal:'',
      public_key: 'pk_test_LzNu9iJt1zxQRxxc20tGnPTo',
      show_error: false,
      error_message: '',
      status: false,
      modalVisible: false,
      modal_data: 'free',
      isloading:false,
      total_amount: 0,
      selected_type: 0
    }
    this.set_token()
  }

  set_token = async() =>{
    var tokken = await AsyncStorage.getItem('token') 
    await this.setState({token: tokken})
    console.log(tokken)
  }
  
  componentDidMount(){
  }

  cc_format_date = (value) => {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []
    for (i=0, len=match.length; i<len; i+=2) {
        parts.push(match.substring(i, i+2))
    }
    if (parts.length) {
      var vv =  parts.join('/')
      this.setState({ expiry: vv })
      return vv
    } else {
      var vv = value
      this.setState({ expiry: vv })
      return vv
    }
  }

  cc_format = (value) => {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []
    for (i=0, len=match.length; i<len; i+=4) {
        parts.push(match.substring(i, i+4))
    }
    if (parts.length) {
        var vv =  parts.join(' ')
        console.log(vv)
        this.setState({ card: vv })

        return vv
    } else {
        var vv = value
        console.log(vv)
        this.setState({ card: vv })
        return vv
    }
  }

  show_error_message = () =>{
    if(this.state.show_error){
      if(this.state.status){
        return(
          <Text style={{margin:10,color: 'green',textAlign:'center'}}>{this.state.error_message}</Text>
        )
      }else{
        return(
          <Text style={{margin:10,color: 'red',textAlign:'center'}}>{this.state.error_message}</Text>
        )
      }
    }
  }

  payment_submit = () => {
    this.props.navigation.push("Home")
    // if(this.state.card == '' || this.state.expiry == '' || this.state.cvc == ''){
    //   this.setState({show_error: true, error_message: "All Fields are Required"})
    // }else{
    //   this.setState({isloading: true })
    //   var cardNum = this.state.card
    //   cardNum=cardNum.replace(/ +/g, "");
    //   cardDates = this.state.expiry
    //   cardDates = cardDates.split('/')
    //   var month = cardDates[0]
    //   var year = cardDates[1]
    //   console.log(month)
    //   console.log(year)
    //   var stripe_url = 'https://api.stripe.com/v1/tokens?card[number]='+cardNum+'&card[exp_month]='+month+'&card[exp_year]='+year+'&card[cvc]='+this.state.cvc;
    //   this.stripePayment(stripe_url)
    // }
  }
  
  stripePayment = async (url) => {
    console.log(url)
    let response = await fetch(url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer "+this.state.public_key
      }
    });
    let responseJson = await response.json();
    if (responseJson.error){
      this.setState({isloading: false, show_error: true, error_message: responseJson.error.message})
    }else{
      console.log(responseJson)
      this.charge_request(responseJson.id)
    }
  }

  charge_request = (id) => {
    console.log("cahrge request")
    try{
      fetch(BasePath+'payment', {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            'Token': this.state.token
        },
        body: JSON.stringify({
          token: id,
          premium: this.state.selected_type,
          amount: this.state.total_amount
        })

      })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status){
          console.log(responseJson)
          this.setState({isloading: false,modal_data: 'success' ,modalVisible: true})
        }else{
          this.setState({isloading: false})
          Alert.alert(
            'Error',
            response.result.message,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  remove_modal = () => {
    this.setState({modalVisible:false})
    this.props.navigation.push("Home")
  }

  show_modal_data = () =>{
    if (this.state.modal_data === 'free'){
      return(
        <View style={styles.modal_container}>
          <View style={styles.modal}>
            <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress = {() => this.setState({modalVisible: false})} >
              <Icon
                name='highlight-off'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
                </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
              <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:18,color:'orange'}}>Free Trial</Text>
              </View>  
              <View style={{flex:3.5,justifyContent:'flex-start',alignItems:'center'}}>
                <Text style={{fontSize:10,color:'grey'}}>Subscribe for a 7 days Free Trial as our gift to use</Text>
                <Text style={{fontSize:10,color:'grey'}}>Your account will be deactivated on Day 8</Text>
                <Text style={{fontSize:10,color:'grey'}}>No credit card reqired</Text>
                <Image
                  style={{
                    height :50,
                    width: 50,
                    marginTop:10
                  }}
                  source={require('../../assets/gift.png')}
                />
                <Text style={{marginTop: 10,fontSize:10,color:'orange'}}>Try it, you'll like it!</Text>
                <TouchableOpacity style={{marginTop:15,justifyContent:'center',alignItems:'center',backgroundColor: 'orange',height:40,width: windowWidth-130}}>
                  <Text style={{fontSize:10,color:'white',fontWeight:'bold'}}>Upgrade to paid Subscription</Text>
                  <Text style={{fontSize:10,color:'white'}}>for less then 0.9/Days</Text>
                </TouchableOpacity>
              </View>  
            </View>  
          </View>
        </View>
      );
    }else if (this.state.modal_data === 'monthly'){
      return(
        <View style={styles.modal_container}>
          <View style={styles.modal}>
            <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress = {() => this.setState({modalVisible: false})} >
              <Icon
                name='highlight-off'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
                </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
              <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
                <Image
                  style={{
                    height :60,
                    width: 60
                  }}
                  source={require('../../assets/calendar.png')}
                />
              </View>
              <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:18,color:'orange'}}>Monthly Subscription</Text>
              </View>  
              <View style={{flex:2,justifyContent:'flex-start',alignItems:'center'}}>
                <Text style={{fontSize:10,color:'grey'}}>Subscribe for a 7 days Free Trial as our gift to use</Text>
                <Text style={{fontSize:10,color:'grey'}}>Your account will be deactivated on Day 8</Text>
                <Text style={{fontSize:10,color:'grey'}}>No credit card reqired</Text>
              </View>  
            </View>  
          </View>
        </View>
      );
    }else if (this.state.modal_data === 'annual'){
      return(
        <View style={styles.modal_container}>
          <View style={styles.modal}>
            <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress = {() => this.setState({modalVisible: false})} >
              <Icon
                name='highlight-off'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
                </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
              <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
                <Image
                  style={{
                    height :60,
                    width: 60
                  }}
                  source={require('../../assets/date.png')}
                />
              </View>
              <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:18,color:'orange'}}>Annual Subscription</Text>
              </View>  
              <View style={{flex:2,justifyContent:'flex-start',alignItems:'center'}}>
                <Text style={{fontSize:10,color:'grey'}}>Subscribe for a 7 days Free Trial as our gift to use</Text>
                <Text style={{fontSize:10,color:'grey'}}>Your account will be deactivated on Day 8</Text>
                <Text style={{fontSize:10,color:'grey'}}>No credit card reqired</Text>
              </View>  
            </View>  
          </View>
        </View>
      );
    }else if (this.state.modal_data === 'success'){
      return(
        <View style={styles.modal_container}>
            <View style={styles.modal}>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{marginLeft:15,flex:0.5,alignItems:'flex-start',justifyContent:'center'}}>
                  <TouchableOpacity onPress = {this.remove_modal} >
                  <Image
                    style={{
                      height :25,
                      width: 25
                    }}
                    source={require('../../assets/home-icon.png')}
                  />
                  </TouchableOpacity>
                </View>
                <View style={{flex:0.5,alignItems:'center',justifyContent:'flex-end'}}>
                    <Image
                      style={{
                        height :50,
                        width:100
                      }}
                      source={require('../../assets/logo.png')}
                    />
                </View>
                <View style={{marginRight:15,flex:0.5,alignItems:'flex-end',justifyContent:'center'}}>
                  <TouchableOpacity onPress = {this.remove_modal} >
                  <Icon
                    name='highlight-off'
                    containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                    color= 'grey' />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
                <Image
                  style={{
                    height :60,
                    width: 60
                  }}
                  source={require('../../assets/tick.png')}
                />
                <Text style={{marginTop:10,fontSize:18,color:'orange'}}>Thanks</Text>
              </View>
              <View style={{flex:2}}>
                <View style={{flex:4.5,justifyContent: 'center',alignItems:'center'}}>
                  <Image
                    style={{
                      height :120,
                      width: windowWidth-30
                    }}
                    source={require('../../assets/refer.png')}
                  />
                </View>  
              </View>  
            </View>
          </View>
      );
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => 
            this.setState({modalVisible:false})
          }
        >
          <TouchableWithoutFeedback>
            {this.show_modal_data()}
          </TouchableWithoutFeedback>
        </Modal>

        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:1.15,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :40,
              width: 40
            }}
            source={require('../../assets/cart.png')}
          />
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-70}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Signup')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6,justifyContent:'flex-start',alignItems:'center'}}>
            <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
            <View><Text style={{fontSize: 14}}>Subscriber Checkout</Text></View>
            <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
          </View>
          <View style={{flex:0.2}}></View>
        </View>
        <View style={{flex:9.5,backgroundColor:'#EEEEEE',width: windowWidth}}>
          <ScrollView>
            <View style={{flex:2,flexDirection: 'row',justifyContent: 'center',alignItems: 'center'}}>
              <View style={{flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
                <Image
                  style={{
                    height :150,
                    width: 150
                  }}
                  source={require('../../assets/monthly.png')}
                />
                <View style={{width: (windowWidth-60)/2,position: 'absolute',justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:16,color:'orange'}}>$<Text style={{fontSize:12,color:'orange'}}>2.99</Text></Text>
                  <Text style={{fontSize:8,textAlign:'center'}}>No CC Requered Free</Text>
                  <Text style={{fontSize:8,textAlign:'center'}}>7 Day Trial Subscription</Text>
                </View>

              </View>
              <View style={{flex:0.2}}></View>
              <View style={{flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
                <Image
                  style={{
                    height :150,
                    width: 150
                  }}
                  source={require('../../assets/annual.png')}
                />
                <View style={{width: (windowWidth-60)/2,position: 'absolute',justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:16,color:'orange'}}>$<Text style={{fontSize:12,color:'orange'}}>30.00</Text></Text>
                  <Text style={{fontSize:9}}>Recommended</Text>
                </View>
              </View>
            </View>
            <View style={{flex:10,margin: 15,backgroundColor: 'white'}}>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:0.20}}>
                  <CheckBox
                    checked={this.state.isFreeChecked}
                    checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                    uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                    onPress={() => this.setState({isFreeChecked: !this.state.isFreeChecked,isAnnChecked:false,isMonChecked:false,selected_type:0,total_amount:0})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
                <View style={{flexDirection:'row',flex:0.80}}>
                  <View style={{flexDirection:'row',flex:0.65,alignItems:'center',justifyContent: 'flex-start'}}>
                    <Text style={{fontSize:14,fontWeight:'bold'}}>Free Trial</Text>
                    <TouchableOpacity onPress={() => this.setState({modal_data: 'free' ,modalVisible: true})}>
                      <Icon name="info" size={15}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:0.05}}></View>
                  <View style={{flex:0.20,alignItems:'flex-end',justifyContent: 'center'}}>
                    <Text style={{color: 'orange',fontWeight: 'bold', fontSize:12}}>$0</Text>
                  </View>
                </View>
              </View>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:0.20}}>
                  <CheckBox
                    checked={this.state.isMonChecked}
                    checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                    uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                    onPress={() => this.setState({isMonChecked: !this.state.isMonChecked,isFreeChecked:false,isAnnChecked:false,selected_type:1,total_amount:2.99})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
                <View style={{flexDirection:'row',flex:0.80}}>
                  <View style={{flexDirection:'row',flex:0.65,alignItems:'center',justifyContent: 'flex-start'}}>
                    <Text style={{fontSize:14,fontWeight:'bold'}}>Monthly Subscription</Text>
                    <TouchableOpacity onPress={() => this.setState({modal_data: 'monthly' ,modalVisible: true})}>
                      <Icon name="info" size={15}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:0.05}}></View>
                  <View style={{flex:0.20,alignItems:'flex-end',justifyContent: 'center'}}>
                    <Text style={{color: 'orange',fontWeight: 'bold', fontSize:12}}>$2.99</Text>
                  </View>
                </View>
              </View>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:0.20}}>
                  <CheckBox
                    checked={this.state.isAnnChecked}
                    checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}} />}
                    uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}} />}
                    onPress={() => this.setState({isAnnChecked: !this.state.isAnnChecked,isMonChecked: false,isFreeChecked:false,selected_type:2,total_amount:30.00})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
                <View style={{flexDirection:'row',flex:0.80}}>
                  <View style={{flexDirection:'row',flex:0.65,alignItems:'center',justifyContent: 'flex-start'}}>
                    <Text style={{fontSize:14,fontWeight:'bold'}}>Annual Subscription</Text>
                    <TouchableOpacity onPress={() => this.setState({modal_data: 'annual' ,modalVisible: true})}>
                      <Icon name="info" size={15} />
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:0.05}}></View>
                  <View style={{flex:0.20,alignItems:'flex-end',justifyContent: 'center'}}>
                    <Text style={{color: 'orange',fontWeight: 'bold', fontSize:12}}>$30.00</Text>
                  </View>
                </View>
              </View>
              <View style={{marginTop: 15,alignItems:'center',justifyContent:'center'}}>
                <Text style={{fontWeight: 'bold'}}>IF FREE TRIAL SELECTED,</Text>
                <Text style={{fontWeight: 'bold'}}>GO DIRECTLY TO CHECKOUT</Text>
              </View>
              <View style={{marginTop: 15}}></View>
            </View>

            <View style={{height:500,flex:6,margin:15,backgroundColor: 'white'}}>
              <Text style={{fontWeight: 'bold',margin:10}}>Payment</Text>
              {this.show_error_message()}
              <Form >
                <Item style={{width:windowWidth-45,marginTop:20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >Card Holder Name</Label>
                  <Input
                    value={this.state.card_holder}
                    onChangeText={(text) => this.setState({card_holder: text})} 
                  />
                </Item>
                <Item style={{width:windowWidth-45,marginTop:20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >Card Number</Label>
                  <Input
                    value={this.state.card}
                    keyboardType = 'numeric'
                    onChangeText={(text) => this.cc_format(text)} 
                  />
                </Item>
                <Item style={{width:windowWidth-45,marginTop: 20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >CVC</Label>
                  <Input  keyboardType = 'numeric'  maxLength={4} onChangeText={(text) => this.setState({ cvc: text })} />
                </Item>
                <Item style={{width:windowWidth-45,marginTop: 20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >Expiration MM/YY</Label>
                  <Input value={this.state.expiry} maxLength={5} keyboardType = 'numeric' onChangeText={(text) =>  this.cc_format_date(text)} />
                </Item>
                <Item style={{width:windowWidth-45,marginTop: 20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >Billing Zip/Postal Code</Label>
                  <Input value={this.state.postal} maxLength={5} keyboardType = 'numeric' onChangeText={(text) =>  this.setState({postal: text})} />
                </Item>
              </Form>
              <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
              </View>
            </View>

            <View style={{flex:6,margin:10,backgroundColor: 'white'}}>
              {/*
                <View style={{flex:1,marginTop:5}}>
                  <Text style={{fontWeight:'bold',marginLeft: 15}}>Promo Code</Text>
                </View>
                <View style={{flex:1,marginTop:5}}>
                  <Text style={{fontWeight:'bold',fontSize: 12,color:'#EEEEEE',marginLeft: 15}}>Promo Code</Text>
                </View>
                <View style={{flexDirection:'row',flex:1,height:40,margin:10,borderBottomWidth: 1,borderColor: '#EEEEEE'}}>
                  <View style={{flex:0.5,alignItems: 'flex-start',justifyContent:'center'}}>
                    <Text style={{fontWeight:'bold',marginLeft: 15}}>Mobilegigs</Text>
                  </View>
                  <View style={{flex:0.5,alignItems: 'flex-end',justifyContent:'center'}}>
                    <Text style={{color:'orange',fontWeight:'bold',marginLeft: 15}}>Apply</Text>
                  </View>
                </View>
                <View style={{flexDirection:'row',flex:1,height:20,margin:10}}>
                  <View style={{flex:0.5,alignItems: 'flex-start',justifyContent:'center'}}>
                    <Text style={{color:'lightgrey',fontWeight:'bold',marginLeft: 15}}>Discount</Text>
                  </View>
                  <View style={{flex:0.5,alignItems: 'flex-end',justifyContent:'center'}}>
                    <Text style={{fontSize:12,color:'green',fontWeight:'bold',marginLeft: 15}}>10.00</Text>
                  </View>
                </View>
              */}
              <View style={{flexDirection:'row',flex:1,height:20,margin:10}}>
                <View style={{flex:0.5,alignItems: 'flex-start',justifyContent:'center'}}>
                  <Text style={{color:'lightgrey',fontWeight:'bold',marginLeft: 15}}>Total Amount</Text>
                </View>
                <View style={{flex:0.5,alignItems: 'flex-end',justifyContent:'center'}}>
                  <Text style={{color:'orange',fontWeight:'bold',marginLeft: 15}}>${this.state.total_amount}.00</Text>
                </View>
              </View>
            </View>
            <View style={{flex:1, alignItems: 'center',justifyContent: 'center'}}>
              { this.state.isloading ?
                <View style={{justifyContent: 'center',alignItems: 'center'}}>
                  <ActivityIndicator />
                </View> 
              :
                <TouchableOpacity style={{justifyContent: 'center',alignItems:'center',marginTop:20,height: 50,backgroundColor: 'orange',width: windowWidth-70}} onPress={this.payment_submit}>
                  <Text style={{color: 'white',fontSize: 18,fontWeight: 'bold'}}>Checkout</Text>
                </TouchableOpacity>
              }
            </View>
            <View style={{flex:1, height: 20, alignItems: 'center',justifyContent: 'center'}}>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'}
});
