import React from 'react';
import { ImageBackground,AsyncStorage,Image, KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item,Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class Refer extends React.Component {
  constructor(props){
    super(props);
    this.state={
      email: '',
      password: '',
      show_error: false,
      error_message: '',
      isLoading: false
    }
  }

  componentDidMount(){
  }

  show_error_message = () =>{
    if(this.state.show_error){
      return(
        <Text style={{color: 'red',textAlign: 'center'}}>{this.state.error_message}</Text>
      )
    }
  }

  storedate = async (response) =>{
    const { navigate } = this.props.navigation;

    console.log('**************************')
    console.log(response)
    try{
      await AsyncStorage.setItem('token', response.token);
      await AsyncStorage.setItem('full_name', response.full_name);
      this.props.navigation.push('Home')
    }catch(e){
      console.log(e)
    }

  }

  login_request = () =>{
    console.log(this.state.email)
    console.log(this.state.password)
    try{
      fetch(BasePath+'api/user/login', {
        method: 'POST',
        mode: 'same-origin',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password
        })
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({isLoading: false})
          this.storedate(responseJson.result)
        
        }else{
          this.setState({isLoading: false, error_message: responseJson.result.message, show_error: true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  form_validation = () => {
    if(this.state.email === '' || this.state.password === ''){
      this.setState({isLoading: false,error_message: 'All Fields are required', show_error: true})
    }else{
      this.login_request()
    }
  }

  login = () =>{
    this.setState({isLoading : true})
    this.form_validation()
  }

  forgot_password = () =>{
    console.log("start Contest")
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={{flex:2.5,flexDirection:'row'}}>
          <View style={{marginLeft:15,flex:1.5,justifyContent:'center',alignItems:'flex-start'}}>
            <Image
              style={{
                height :100,
                width: 100
              }}
              source={require('../../assets/logo.png')}
            />
          </View>
          <View style={{flex:1.5,marginRight:15,justifyContent:'center',alignItems:'flex-end'}}>
            <Icon
                name='highlight-off'
                color= 'grey' />
          </View>
        </View>
        <View style={{flex:1,width: windowWidth-45,backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: 'white',fontSize: 22}}>Banner</Text>
        </View>
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 22,textAlign: 'center'}}>You are invited to join</Text>
        </View>
        <View style={{flex:6.5}}>
          <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :80,
                width: 150
              }}
              source={require('../../assets/logo-with-text.png')}
            />
          </View>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :60,
                width: 60
              }}
              source={require('../../assets/heytext.png')}
            />
          </View>
          <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:11,color:'grey'}}>Thought you might like to know about</Text>
            <Text style={{fontSize:11,color:'grey'}}>a cool new app connecting to you</Text>
            <Text style={{fontSize:11,color:'grey'}}>Professional services....</Text>
            <Text style={{fontSize:11,color:'grey'}}>Whenever you are.</Text>
            <Text style={{fontSize:11,color:'orange'}}>Download on <Text style={{fontWeight:'bold'}}>Apple </Text>and <Text style={{fontWeight:'bold'}}>Android </Text></Text>
            <Text style={{fontSize:11,color:'orange'}}>MOBILEGIGS.COM</Text>

          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity style= {{backgroundColor:'orange',justifyContent:'center',alignItems:'center',width:windowWidth-80,height:40}}>
              <Text style={{color:'white',fontWeight:'bold'}}>Refer a Firend</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <Icon
                name='home'
                color= 'grey' />
            <Text style={{fontSize:12,color:'grey'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <Icon
                name='search'
                color= 'grey' />
            <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <Icon
                name='favorite-border'
                color= 'grey' />
            <Text style={{fontSize:12,color:'grey'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <Icon
                name='account-circle'
                color= 'grey' />
            <Text style={{fontSize:12,color:'grey'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    flex:1,
    justifyContent: 'flex-start', 
    alignItems: 'flex-end',
    flexDirection : 'row'
  },
  heading: {
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputField: {
    height: 50,
    width: windowWidth-80,
    marginTop: 10,
    backgroundColor: 'lightgrey'
  },
  login_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  signup_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'navy',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10,
    height: 50
  },
  icon:{
    height:50,
    width: 50
  }
});