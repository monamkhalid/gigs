import React from 'react';
import { AsyncStorage,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';
import Slideshow from 'react-native-image-slider-show';


var windowWidth = Dimensions.get('window').width
export default class AddReviews extends React.Component {

  constructor(props){
    super(props);
    this.state={
      isLoading: false,
      image: "http://inaction.brossardappdesign.com/site/inaction/public/assets/uploads/post_images//1556731055951366094.jpg",

    }
    // this.get_details(this.props.navigation.state.params.id)
  }

  // set_token = async() =>{
  //   // var tokken = await AsyncStorage.getItem('token') 
  //   // await this.setState({token: tokken})
  //   // console.log(tokken)
  //   this.get_details(this.props.navigation.state.params.id)
  // }

  get_details  =(id) => {
    console.log("id")
    try{
      fetch(BasePath+'getdetails?gid='+id, {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({services: responseJson.result,isLoading: false})
        console.log(responseJson)
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  
  componentDidMount(){
  }

  
  render() {
    const { navigate } = this.props.navigation;
    if (this.state.isLoading){
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <ActivityIndicator />
        </View>
      )
    }else{
      return (
        <View style={styles.container}>
          <View style={styles.heading}>
            <View style={{flex:0.85,justifyContent:'flex-end'}}>
            <Image
              style={{
                height :60,
                width: 60
              }}
              source={require('../../assets/logo.png')}
            />
            </View>
            <View style={{flex:1.15,justifyContent:'flex-end'}}>
            <Image
              style={{
                height :40,
                width: 40
              }}
              source={require('../../assets/review.png')}
            />
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row',width: windowWidth-70}}>
            <View style={{flex:0.2}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-back'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
            <View style={{flex:0.6,justifyContent:'flex-start',alignItems:'center'}}>
              <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
              <View><Text style={{fontSize: 14}}>ADD REVIEW</Text></View>
              <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
            </View>
            <View style={{flex:0.2}}></View>
          </View>
          <View style={{flex:8,width: windowWidth}}>
            <View style={{flex:2,flexDirection:'row',borderBottomWidth:1,borderColor:'lightgrey'}}>
              <View style={{flex: 0.2,justifyContent:'center',alignItems:'center'}}>
                <Image
                  style={{
                    height :70,
                    width: 70
                  }}
                   source={{uri: this.state.image}}
                />
              </View>
              <View style={{flex: 0.8,justifyContent:'center',alignItems:'flex-start'}}>
                <View style={{flexDirection:'row'}}>
                  <Text style={{fontWeight:'bold'}}>{'Service'}</Text>
                  <Icon
                    name='favorite'
                    size={16}
                  />
                </View>
                <Text style={{fontSize:10,color: 'grey',fontWeight:'bold',marginTop:5}}>Mechanical/Electrical Slides</Text>
              </View>
            </View>
            <View style={{flex:5,marginBottom:10}}>
              <ScrollView style={{width: windowWidth}}>
                
                <View style={styles.reviewsContainer}>
                  <Text style={styles.textStyle}>Price</Text>
                  <View style={styles.starsContainer}>
                    <Icon
                      name='star'
                      containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                      color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                      name='star'
                      containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                      color= 'black' />
                  </View>
                </View>

                <View style={styles.reviewsContainer}>
                  <Text style={styles.textStyle}>Availability/Promptness</Text>
                  <View style={styles.starsContainer}>
                    <Icon
                      name='star'
                      containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                      color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                      name='star'
                      containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                      color= 'black' />
                  </View>
                </View>
                
                <View style={styles.reviewsContainer}>
                  <Text style={styles.textStyle}>Customer Service</Text>
                  <View style={styles.starsContainer}>
                    <Icon
                      name='star'
                      containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                      color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                      name='star'
                      containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                      color= 'black' />
                  </View>
                </View>
                
                <View style={styles.reviewsContainer}>
                  <Text style={styles.textStyle}>Quality Service</Text>
                  <View style={styles.starsContainer}>
                    <Icon
                      name='star'
                      containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                      color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                        name='star'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'black' />
                    <Icon
                      name='star'
                      containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                      color= 'black' />
                  </View>
                </View>
                <View style={{height: 100,justifyContent:'center',alignItems:'center'}}>
                  <Item style={{width:windowWidth}} floatingLabel>
                    <Label style={{color: 'grey',paddingLeft:10}} >Add Comment</Label>
                    <Input style={{paddingLeft:10}} onChangeText={(text) => this.setState({ comment: text })} />
                  </Item>
                </View>
                <View style={{height: 100,justifyContent:'center',alignItems:'center'}}>
                  <Item style={{width:windowWidth}} floatingLabel >
                    <Label style={{color: 'grey',paddingLeft:10}} >Company Response</Label>
                    <Input style={{paddingLeft:10}} onChangeText={(text) => this.setState({ comp_resp: text })}  />
                  </Item>
                </View>    
                <View style={{height: 150,justifyContent:'center',alignItems:'center'}}>
                  <TouchableOpacity style={styles.submit_btn}>
                    <Text style={{color: 'white',fontWeight:'bold',fontSize:15}}>Submit</Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  submit_btn:{
    justifyContent:'center',
    alignItems:'center',
    height:60,
    width: windowWidth-80,
    backgroundColor:'orange'
  },
  textStyle:{
    marginLeft:15
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  reviewsContainer:{
    height:100,
    justifyContent:'center',
    alignItems:'flex-start',
    borderBottomWidth:1,
    borderColor: 'lightgrey'
  },
  starsContainer:{
    marginLeft:15,
    flexDirection: 'row'
  },
  redirectContainer:{
    backgroundColor:'white',marginTop:15,flexDirection:'row',height: 120, width:windowWidth,borderColor:'lightgrey',borderBottomWidth:1
  },
  redirectSContainer:{
    backgroundColor:'white',marginTop:15,flexDirection:'row', width:windowWidth,borderColor:'lightgrey',borderBottomWidth:1
  },
  redirectSTextContainer:{
    marginTop: 15,flex:1,width:windowWidth
  },
  redirectTextContainer:{
    marginTop: 15,marginLeft:10,flex:0.7,alignItems:'flex-start',justifyContent:'flex-start'
  },
  redirectIconContainer:{
    marginTop: 15,marginRight:10,flex:0.3,alignItems:'flex-end',justifyContent:'flex-start'
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'}
});
