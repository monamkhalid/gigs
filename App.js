import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import HomeScreen from './src/components/home.js';
import ProviderCheckoutScreen from './src/components/provider_checkout.js';
import LoginScreen from './src/components/auth/login.js';
import ForgotPasswordScreen from './src/components/forgot_password.js';
import VerifyScreen from './src/components/verify.js';
import CodeScreen from './src/components/verification_code.js';
import SignupScreen from './src/components/auth/signup.js';
import StripeScreen from './src/components/stripe.js';
import HelpingHandScreen from './src/components/helping_hands.js';
import ReferScreen from './src/components/refer.js';
import ProviderProfileScreen from './src/components/provider_profile.js';
import ServiceScreen from './src/components/service.js';
// import AuthLoadingScreen from './src/auth_loading.js';
import SearchScreen from './src/components/search.js';
import FavoriteScreen from './src/components/favorites.js';
import SnapScreen from './src/components/snap.js';
import FavoriteOtherScreen from './src/components/favorites_other.js';
import ProfileScreen from './src/components/subscriber_dashboard.js';
import ProfileDetailScreen from './src/components/profile_detail.js';
import ModifySubScreen from './src/components/modify_subscription.js';
import PaymentDetailsScreen from './src/components/payment_details.js';
import AccountSettingScreen from './src/components/account_setting.js';
import GeneralScreen from './src/components/general.js';
import ProviderDashboardScreen from './src/components/provider_dashboard.js';
import ContactScreen from './src/components/proContact_detail.js';
import ProModifyScreen from './src/components/proModifySub.js';
import ProBusinessScreen from './src/components/business_sub.js';
import MoreDetailScreen from './src/components/more_details.js';
import AddReviewsScreen from './src/components/add_reviews.js';
import HelpingServicesScreen from './src/components/helping_hand_service.js';
import HelpingCommentScreen from './src/components/helping_hand_comment.js';
import MainSearchScreen from './src/components/searchMainScreen';
// import ImageScreen from './src/post_image.js';
// import PreviewScreen from './src/preview_image.js';
import {store} from './src/store';
import { Provider } from 'react-redux';

import {
  createAppContainer,
  createSwitchNavigator
} from 'react-navigation';
import {createStackNavigator}from 'react-navigation-stack'
const AppNavigator = createStackNavigator({
  Login: { screen: LoginScreen },
  Home: { screen: HomeScreen },
  ProviderCheckout: { screen: ProviderCheckoutScreen },
  Stripe: { screen: StripeScreen },
  HelpingComment: { screen: HelpingCommentScreen },
  Search: { screen: SearchScreen },
  HelpingService: { screen: HelpingServicesScreen },
  ProBusiness: { screen: ProBusinessScreen},
  FavoriteOther: { screen: FavoriteOtherScreen },
  AddReviews: { screen: AddReviewsScreen },
  HelpingHand: { screen: HelpingHandScreen },
  ReferFriend: { screen: ReferScreen },
  ProviderProfile: { screen: ProviderProfileScreen },
  MoreDetail: { screen: MoreDetailScreen },
  Profile: { screen: ProfileScreen },
  ProModify: { screen: ProModifyScreen},
  ProContact: { screen: ContactScreen },
  ProProfile: { screen: ProviderDashboardScreen },
  Snap: { screen: SnapScreen },
  Signup: { screen: SignupScreen },
  ProfileDetail: { screen: ProfileDetailScreen },
  General: { screen: GeneralScreen },
  PaymentDetails: { screen: PaymentDetailsScreen },
  AccountSettingDetails: { screen: AccountSettingScreen },
  ModifySubscription: { screen: ModifySubScreen },
  Favorite: { screen: FavoriteScreen },
  Service: { screen: ServiceScreen },
  Forgot: { screen: ForgotPasswordScreen },
  Verify: { screen: VerifyScreen },
  Code: { screen: CodeScreen },
  MainSearchScreen: { screen: MainSearchScreen },

}, {headerMode: 'none'});

const AppContainer = createAppContainer(AppNavigator);

const App = () =>{
  return (

      <Provider store={store}>
        <AppContainer />
      </Provider>

  )
};

export default App;
