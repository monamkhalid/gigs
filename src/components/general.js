import React from 'react';
import { ListView,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class General extends React.Component {

  constructor(props){
    super(props);
    this.state={
      search: true,
      name: 'Saif Abou Goura',
      membership: 'Free',
      phone_number: '123456789',
      seePrivay:false
    }
    this.populateList()
  }
  togglePrivacy = ()=>{
    this.setState({seePrivay :!this.state.seePrivay})
  }

  componentDidMount(){
  }

  populateList() {
    var dataSource=
    [ 
      {id:1,email: 'ahtasham@email.com',show_image:true,duration:'Now',name:'Bobs RV Repair',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:2,email: 'ahtasham1@email.com',show_image:false,duration:'1 days ago',name:'OHN RV Repair',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:3,email: 'ahtasham2@email.com',show_image:true,duration:'2 days ago',name:'Bobs RV Solar',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:4,email: 'ahtasham3@email.com',show_image:false,duration:'3 days ago',name:'Ohm RV Solar',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'}
    ]
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state.dataSource = ds.cloneWithRows(dataSource);
    this.state.db = dataSource
  };
  
  show_icon = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-end',alignItems:'center'}}>
          <Icon
            name='person'
            color='orange'
            containerStyle={{alignItems:'flex-start'}}
            size={40}
            />
        </View>
      )
    }else{
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
          <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
          <Icon
            name='highlight-off'
            size={30}
            containerStyle={{alignItems:'flex-end'}}
            color= 'grey' />
            </TouchableOpacity>
        </View>
      )
    }
  }

  show_text = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
          <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
          <View><Text style={{fontSize: 14}}>General</Text></View>
          <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
        </View>
      );
    }else{
      return null;
    }
  }
  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        
        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:0.8}}>
            {
              this.show_icon()
            }
          </View>
          <View style={{flexDirection:'row',flex:0.8,width:windowWidth,marginRight:15,justifyContent: 'center', alignItems: 'center'}}>
            <Text >Contact Us </Text>
            <Icon
              name='email'
              size={20}
              />
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6}}>
            {this.show_text()}
          </View>
          <View style={{flex:0.2,justifyContent: 'flex-start',alignItems: 'center'}}>
          </View>
        </View>
        
        <View style={{flex:8,backgroundColor:'#F0F0F0',width: windowWidth}}>
          <View style={{flex:4,width: windowWidth,marginTop:10}}>
            <View style={styles.redirectContainer}>
              <View style={styles.redirectTextContainer}>
                <Text style={{fontSize:18,marginBottom: 10}}>Refer a Friend</Text>
              </View>
              <View style={styles.redirectIconContainer}>
                <TouchableOpacity>
                  <Icon
                    name='keyboard-arrow-right'
                    size={30}
                    color= 'orange'
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.redirectContainer}>
              <View style={styles.redirectTextContainer}>
                <Text style={{fontSize:18,marginBottom: 10}}>Privacy Policy</Text>
              </View>
              <View style={styles.redirectIconContainer}>
                <TouchableOpacity
                onPress ={this.togglePrivacy}
                >
                  <Icon
                    name='keyboard-arrow-right'
                    color= 'orange'
                    size={30}
                  />
                </TouchableOpacity>
              </View>
            </View>
            {/* {this.state.seePrivay?
             <View style = {{display:'flex'}}>
             <Image
             style = {{width:'100%',height:"100%"}}
             source = {require('../assets/carcrash.png')}
             resizeMode = "contain"
             />

           </View>
           :
           <View/>
            
          } */}

            <View style={styles.redirectContainer}>
              <View style={styles.redirectTextContainer}>
                <Text style={{fontSize:18,marginBottom: 10}}>Terms and Conditions</Text>
              </View>
              <View style={styles.redirectIconContainer}>
                <TouchableOpacity>
                  <Icon
                    name='keyboard-arrow-right'
                    size={30}
                    color= 'orange'
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{flex:4,width: windowWidth}}>
          </View>
        </View>
        


        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-start',marginLeft:15}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
                <Icon
                  name='arrow-back'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-end'}}>
            </View>
          </View>

        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
            <Icon
                name='home'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
            <Icon
                name='search'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
                name='favorite-border'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
            <Icon
                name='account-circle'
                color= 'orange' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'orange'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer:{
    flex:8,
    backgroundColor:"lightgrey",
    width:windowWidth
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  iconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'lightgrey'
  },
  icon:{
    height: 50,
    width: 50  
  },
  sub_container:{
    backgroundColor:'white',borderBottomWidth: 1,borderColor:'grey',flex:1,width: windowWidth,flexDirection:'row'
  },
  sub_text_container:{
    flex:0.9,width: windowWidth,flexDirection:'row',justifyContent:'flex-start',alignItems:'center'
  },
  sub_text:{
    fontWeight: 'bold'
  },
  upgrade_button:{
    height: 35,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: 'orange',
    width: 90
  },
  sub_btn_text:{
    color: 'white',
    fontWeight:'bold'
  },
  redirectContainer:{
    backgroundColor:'white',flexDirection:'row',flex:1, width:windowWidth,borderColor:'lightgrey',borderBottomWidth:1
  },
  redirectTextContainer:{
    marginTop: 15,marginLeft:10,flex:0.7,alignItems:'flex-start',justifyContent:'center'
  },
  redirectIconContainer:{
    marginTop: 15,marginRight:10,flex:0.3,alignItems:'flex-end',justifyContent:'flex-start'
  }

});
