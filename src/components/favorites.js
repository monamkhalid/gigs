import React from 'react';
import { ListView,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class Favorite extends React.Component {

  constructor(props){
    super(props);
    this.state={
      search: true
    }
    this.populateList()
  }

  componentDidMount(){
  }

  populateList() {
    var dataSource=
    [ 
      {id:1,email: 'ahtasham@email.com',show_image:true,likes:10,name:'Bobs RV Repair',detail:'Mechanical',address: '15 c Judicial calony' ,desc: 'hello',miles: '1.3 Miles away'},
      {id:2,email: 'ahtasham1@email.com',show_image:false,likes:11,name:'OHN RV Repair',detail:'Electrical',address: '15 c Judicial calony',desc: 'hello1',miles: '1.4 Miles away'},
      {id:3,email: 'ahtasham2@email.com',show_image:true,likes:15,name:'Bobs RV Solar',detail:'Oalar System',address: '15 c Judicial calony' ,desc: 'hello2',miles: '1.5 Miles away'},
      {id:4,email: 'ahtasham3@email.com',show_image:false,likes:1,name:'Ohm RV Solar',detail:'Olar System',address: '15 c Judicial calony',desc: 'hello3',miles: '1.6 Miles away'}
    ]
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state.dataSource = ds.cloneWithRows(dataSource);
    this.state.db = dataSource
  };
  

  search_data = () =>{
    if (this.state.search){
      return(
        <View style={{flex:7}}>
          <View style={{flex:3,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :100,
                width: 180
              }}
              source={require('../../assets/logo-with-text.png')}
            />
          </View>
          <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:20,fontWeight:'bold',color:'orange'}}>Search for a professional</Text>
            <Text style={{fontSize:20,fontWeight:'bold',color:'orange'}}>Provider near you</Text>
          </View>
          <View style={{flex:2}}>
            <Form>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{fontSize: 10,color: 'grey'}} >(Search by Name, Type of Services, Market or Zip Code)</Label>
                <Input />
              </Item>
              <Icon
                name='search'
                size={40}
                iconStyle={{position:'relative',top:-35}}
                containerStyle={{width:windowWidth-45,justifyContent:'flex-end',alignItems:'flex-end'}}
                color= 'orange' />
            </Form>
          </View>
          <View style={{flex:1}}>
          </View>
        </View>
      );
    }else{
      return(
        <View style={{flex:7}}>
          <View style={{flex:3.5,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :130,
                width: 150
              }}
              source={require('../../assets/sorrynotfound.png')}
            />
          </View>
          <View style={{flex:2.5,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:20,fontWeight:'bold'}}>Aw Shucks!</Text>
            <Text style={{fontSize:16}}>There are no result found.</Text>
            <Text style={{fontSize:16}}>Please search using another search word.</Text>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity style={{height:50,width:windowWidth-90,backgroundColor:'orange',justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:16,color:'white'}}>Search Again</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.5}}>
          </View>
        </View>
      );
    }
  }

  show_icon = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-start'}}>
          <Icon
            name='favorite'
            containerStyle={{alignItems:'flex-start'}}
            size={40}
            />
        </View>
      )
    }else{
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
          <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
          <Icon
            name='highlight-off'
            size={30}
            containerStyle={{alignItems:'flex-end'}}
            color= 'grey' />
            </TouchableOpacity>
        </View>
      )
    }
  }

  show_text = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
          <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
          <View><Text style={{fontSize: 14}}>Favorites</Text></View>
          <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
        </View>
      );
    }else{
      return null;
    }
  }

  show_images = () =>{
    return(
      <View style={styles.containerWrap}>
        <ScrollView horizontal={true}>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :70,
                width: 70
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :70,
                width: 70
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :70,
                width: 70
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :100,
                width: 100
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{marginRight: 15,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :70,
                width: 70
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
        </ScrollView>
      </View>
    );
  }

  

  _renderRow(follow, sectionID, rowID){
    return(
      <View style={styles.listContainer}>
        <View style={{flex:3.5,flexDirection:'row'}}>
          <View style={{flex: 0.3,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :50,
                width: 50
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{flex: 0.45,justifyContent:'center',alignItems:'flex-start'}}>
            <View style={{flexDirection:'row'}}>
              <Text style={{fontWeight:'bold'}}>{follow.name}</Text>
              <Icon
                name='favorite'
                size={16}
              />
            </View>
            <Text style={{fontSize:10,color: 'grey',fontWeight:'bold',marginTop:5}}>{follow.detail}</Text>
            <View style={{flexDirection:'row',marginTop:5}}>
              <View style={{flexDirection:'row',height:20,width:60,justifyContent:'center',alignItems:'center',backgroundColor:'lightgreen',borderRadius:5}}>
                <Icon
                  name='star'
                  size= {10} />
                <Text style={{fontSize:10,fontWeight:'bold',marginLeft:10}}>5.0</Text>
              </View>
              <Text style={{marginLeft:5,marginTop:5,fontSize:10,color: 'grey',fontWeight:'bold'}}>{follow.likes} Reviews</Text>
            </View>
            <Text style={{fontSize:10,color: 'grey',fontWeight:'bold',marginTop:5}}>{follow.address}</Text>
          </View>
          <View style={{flex: 0.25}}>
            <Text style={{fontSize:10,color: 'grey',fontWeight:'bold'}}>{follow.miles}</Text>
            <Icon
              name='phone-in-talk'
              containerStyle={{marginTop:5,justifyContent: 'center',alignItems: 'center'}}
              color= 'grey' />
          </View>
        </View>
        <View style={{flex:3,flexDirection:'row',height: 70}}>
          <View style={{flex: 0.3,justifyContent:'center',alignItems:'center',backgroundColor:'grey'}}>
            <Text style={{color:'lightgrey',fontSize: 11}}>Markets Served</Text>
            <Text style={{fontWeight:'bold'}}>NY, CT, NJ</Text>
          </View>
          <View style={{flex: 0.45,marginTop:10}}>
            <View style={{flexDirection:'row',marginLeft: 5}}>
              <Icon
                name='comment'
                containerStyle={{marginTop:5,justifyContent: 'center',alignItems: 'center'}}
                />
              <Text style={{margin:10,fontSize:10,color: 'grey',fontWeight:'bold'}}>Text</Text>
            </View>
            <View style={{flexDirection:'row',marginLeft: 5}}>
              <Icon
                name='email'
                containerStyle={{marginTop:5,justifyContent: 'center',alignItems: 'center'}}
                />
              <Text style={{margin:10,fontSize:10,color: 'grey',fontWeight:'bold'}}>{follow.email}</Text>
            </View>
          </View>
        </View>
        <View style={{margin: 10,flex:2,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
          <View style={styles.fbiconContainer}>
            <Image
              style={{
                height :15,
                width: 15
              }}
              source={require('../../assets/fb.png')}
            />
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height :15,
                width: 15
              }}
              source={require('../../assets/ttr.png')}
            />
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height :15,
                width: 15
              }}
              source={require('../../assets/youtube.png')}
            />
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height :15,
                width: 15
              }}
              source={require('../../assets/insta.png')}
            />
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height :15,
                width: 15
              }}
              source={require('../../assets/in.png')}
            />
          </View>
        </View>
        <View style={{flex:1.5,flexDirection:'row'}}>
          <View style={{flex:0.45,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity>
              <Text style={{color: 'orange',fontWeight:'bold'}}>MORE DETAILS</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.10}}></View>
          <View style={{flex:0.45}}>
            <TouchableOpacity style={{height: 40,justifyContent:'center',alignItems:'center',backgroundColor:'orange',width: (windowWidth-40)/2,flexDirection:'row'}}>
              <Icon
                name='add-circle'
                size={12}
              />
              <Text style={{marginLeft: 5,color: 'white',fontWeight:'bold'}}>Add REVIEW</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        
        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:1.15}}>
            {
              this.show_icon()
            }
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6}}>
            {this.show_text()}
          </View>
          <View style={{flex:0.2,justifyContent: 'flex-start',alignItems: 'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                name='arrow-forward'
                containerStyle={{}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Next</Text>
          </View>
        </View>
        <View  style={{flex:0.5,justifyContent:'center', alignItems: 'center',flexDirection:'row'}}>
          <Text style={{color:'orange',fontWeight: 'bold'}}>ALL</Text>
          <Icon
            name='arrow-drop-down'
            containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
            color= 'orange' />
        </View>
        <View style={{flex:7.5}}>
          <View style={{flex:7,backgroundColor:'lightgrey'}}>
             <View style={{flex:3}}>
              {this.show_images()}
            </View>
            <View style={{flex:4.5}}>
              <ListView
                style={{width:'100%'}}
                dataSource={this.state.dataSource}
                renderRow={this._renderRow.bind(this)}
              />
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-start',marginLeft:15}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-back'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-end'}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-forward'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Next</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
            <Icon
                name='home'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
            <Icon
                name='search'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
                name='favorite-border'
                color= 'orange' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'orange'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
            <Icon
                name='account-circle'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer:{
    flex:8,
    backgroundColor:"white",
    paddingVertical:15,
    marginBottom:10,
    width:windowWidth
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  iconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'lightgrey'
  },
  fbiconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'orange'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  containerWrap: {
  marginTop:5,
  flex: 3,
  flexDirection: 'row',
  backgroundColor: 'white'
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'}
});
