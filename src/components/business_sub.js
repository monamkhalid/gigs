import React from 'react';
import { Switch,ListView,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Picker,Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import { ImagePicker,Permissions } from 'expo';

import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class BusinessSubscription extends React.Component {

  constructor(props){
    super(props);
    this.state={
      search: true,
      name: 'Saif Abou Goura',
      membership: 'Free',
      phone_number: '123456789',
      twitter_url: '',
      facebook_url: '',
      in_url: '',
      youtube_url: '',
      insta_url: '',
      images: [],
      base64_str: null,
      modalVisible: false,
      modalScheduleVisible: false,
      local_image_url: null,
      service_site: 'No',
      service_toogle: false,
      available_array:  
      [
        {id: 1,availability :true,day: "Mon",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 2,availability :false,day: "Tue",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 3,availability :true,day: "Wed",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 4,availability :true,day: "Thu",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 5,availability :false,day: "Fri",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 6,availability :true,day: "Sat",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 7,availability :true,day: "Sun",start_time : '10:10 AM',end_time: '10:10 PM'}
      ],
      schedule_time:[
        {id: 1,name:"09:00 AM"},
        {id: 2,name:"09:30 AM"},
        {id: 3,name:"10:00 AM"},
        {id: 4,name:"10:30 AM"},
        {id: 5,name:"11:00 AM"},
        {id: 6,name:"11:30 AM"},
        {id: 7,name:"12:00 PM"},
        {id: 8,name:"12:30 PM"},
        {id: 9,name:"01:00 PM"},
        {id: 10,name:"01:30 PM"},
        {id: 11,name:"02:00 PM"},
        {id: 12,name:"02:30 PM"},
        {id: 13,name:"03:00 PM"},
        {id: 14,name:"03:30 PM"},
        {id: 15,name:"04:00 PM"},
        {id: 16,name:"04:30 PM"},
        {id: 17,name:"05:00 PM"},
        {id: 18,name:"05:30 PM"},
        {id: 19,name:"06:00 PM"},
        {id: 20,name:"06:30 PM"},
        {id: 21,name:"07:00 PM"},
        {id: 22,name:"07:30 PM"},
        {id: 23,name:"08:00 PM"},
        {id: 24,name:"08:30 PM"},
        {id: 25,name:"09:00 PM"},
        {id: 26,name:"09:30 PM"}
      ],
      tags:[
        {id: 0, tag: "Mechanical"},
        {id: 1, tag: "Electrical"},
        {id: 2, tag: "Mechanical/Generator"},
        {id: 3, tag: "Mechanical"},
        {id: 4, tag: "Inspactions"},
        {id: 5, tag: "Solar Panal"},
        {id: 6, tag: "Mechanical"}
      ]
    }
    this.get_categories()
    this.populateList()
  }

  componentDidMount(){
  }

  populateList() {
    var dataSource=
    [ 
      {id:1,email: 'ahtasham@email.com',show_image:true,duration:'Now',name:'Bobs RV Repair',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:2,email: 'ahtasham1@email.com',show_image:false,duration:'1 days ago',name:'OHN RV Repair',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:3,email: 'ahtasham2@email.com',show_image:true,duration:'2 days ago',name:'Bobs RV Solar',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:4,email: 'ahtasham3@email.com',show_image:false,duration:'3 days ago',name:'Ohm RV Solar',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'}
    ]
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state.dataSource = ds.cloneWithRows(dataSource);
    this.state.db = dataSource
  };

  facebook_social = () =>{
    if(this.state.facebook_url == ''){
      return(
        <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'facebook',modalText: 'Facebook Url',modalVisible: true})}>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Icon
              type="font-awesome"
              name='facebook'
              iconStyle={{color:'white'}}
            />
          </View>
          <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
            <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Facebook</Text>
          </View>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Image
              style={{
                height :25,
                width: 25
              }}
              source={require('../../assets/cross.png')}
            />
          </View>
        </TouchableOpacity>
      )
    }else{
      return(
        <View style={styles.otherdis_btn}>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Icon
              type="font-awesome"
              name='facebook'
            />
          </View>
          <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
            <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Facebook</Text>
          </View>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Image
              style={{
                height :25,
                width: 25
              }}
              source={require('../../assets/cross.png')}
            />
          </View>
        </View>
      )
    }
  }

  twitter_social = () =>{
      if(this.state.twitter_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'twitter',modalText: 'Twitter Url',modalVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='twitter'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Twitter</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='twitter'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Twitter</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    insta_social = () =>{
      if(this.state.insta_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'insta',modalText: 'Instagram Url',modalVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='instagram'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Instagram</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn} >
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='instagram'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Instagram</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    in_social = () =>{
      if(this.state.in_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'in',modalText: 'Linkedin Url',modalVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='linkedin'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Linkedin</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='linkedin'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Linkedin</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    youtube_social = () =>{
      if(this.state.youtube_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'youtube',modalText: 'Youtube Url',modalVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='youtube'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Youtube</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn} >
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='youtube'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Youtube</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

  onServiceChange(value: string) {
      this.setState({
        service: value
      });
  }

  onCategoryChange(value: string) {
      this.setState({
        category: value
      });
      this.get_services(value)
  }

  get_categories = () =>{
    try{
      fetch(BasePath+'getcategory', {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({catdata: responseJson.result,show_category:true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }



  showcat = () =>{
    if(this.state.show_category){
      return this.state.catdata.map((item, key) =>{
        return(
          <Picker.Item key={item.id} label={item.name} value={item.id} />
        )
      })
    }else{
      return null
    }
  }

  showser = () =>{
    if(this.state.show_service){
      return this.state.servdata.map((item, key) =>{
        return(
          <Picker.Item key={item.id} label={item.name} value={item.id} />
        )
      })
    }else{
      return null
    }
  }



  get_services = (value) =>{
    console.log(value)
    try{
      fetch(BasePath+'getservice?sid='+value, {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({servdata: responseJson.result,show_service:true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  
  show_icon = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-end',alignItems:'center'}}>
          <Icon
            name='person'
            color='orange'
            containerStyle={{alignItems:'flex-start'}}
            size={40}
            />
        </View>
      )
    }else{
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
          <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
          <Icon
            name='highlight-off'
            size={30}
            containerStyle={{alignItems:'flex-end'}}
            color= 'grey' />
            </TouchableOpacity>
        </View>
      )
    }
  }

  toggleSwitch = (value, key) => {
      // //onValueChange of the switch this function will be called
      console.log("********************************************")
      console.log(key)
      this.state.available_array[key].availability = !this.state.available_array[key].availability
      // item.availability = !item.availability
      this.setState({})
      // //state changes according to switch
      // //which will result in re-render the text
  }

  toggleServiceSwitch = (value) => {
      this.setState({service_site: this.state.service_toogle ? "No" : "Yes" ,service_toogle: !this.state.service_toogle})
      
  }


  show_schedule = () =>{
      if (this.state.available_array){
        return(
          this.state.available_array.map((item, key) =>{
            return(
              <View key={item.id} style={{height: 60,width: windowWidth-20,flex:2,flexDirection:'row'}}>
                <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                  <Text>{item.day}</Text>
                  <Switch
                    onValueChange = {(value) => this.toggleSwitch(value,key)}
                    value = {item.availability}/>
                </View>
                <View style={{flex:0.3}}></View>
                <View style={{flexDirection:'row',borderBottomWidth:1,borderColor: 'lightgrey',flex:0.5,justifyContent:'center',alignItems:'center'}}>
                  <View style={{flex:0.7}}>
                    <Text>{item.start_time}</Text>
                  </View>
                  <View style={{flex:0.3}}>
                    <TouchableOpacity onPress={() => this.setState({selected_date: "start",selected_schedule: key, modalScheduleVisible: true})} >
                      <Icon
                        name='unfold-more'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'red' />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{flex:0.2}}></View>
                <View style={{flexDirection:'row',borderBottomWidth:1,borderColor: 'lightgrey',flex:0.5,justifyContent:'center',alignItems:'center'}}>
                  <View style={{flex:0.7}}>
                    <Text>{item.end_time}</Text>
                  </View>
                  <View style={{flex:0.3}}>
                   <TouchableOpacity  onPress={() => this.setState({selected_date: "end",selected_schedule: key, modalScheduleVisible: true})}>
                      <Icon
                        name='unfold-more'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'red' />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            )
          })
        );
      }else{
        return null;
      }
  } 


  show_text = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
          <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
          <View><Text style={{fontSize: 14}}>Business Subscription</Text></View>
          <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
        </View>
      );
    }else{
      return null;
    }
  }

  _pickImage = async () => {
    console.log("P")
    var imageArr = this.state.images;
    const { status: cameraRollPerm } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if (cameraRollPerm === 'granted') {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
        base64: true
      });
      if (!result.cancelled) {
        await imageArr.push(result)
        await this.setState({ images: imageArr });
        console.log("**********************************")
        console.log(this.state.images)
        console.log("**********************************")
      }
    }
  }


  show_uploaded = () =>{
    return(
      this.state.images.map((item, key) =>{
        console.log("Image Uploader")
        return(
          <View key={key} style={{marginRight: 7,marginLeft: 7,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :100,
                width: 100
              }}
               source={{ uri: item.uri }}
            />
          </View>
        )
      })
    )
  }

  show_url_input = () =>{
    if (this.state.selected_social === 'facebook'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ facebook_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'twitter'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ twitter_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'insta'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ insta_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'in'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ in_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'youtube'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ youtube_url: text })} />
        </Item>
      )
    }
  }

  showshow_tags = () =>{
     return this.state.tags.map((item, key) =>{
        return(
          <View key={item.id} style={{height: 30,marginTop: 5,borderRadius:20,backgroundColor:'lightgrey',justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Text style={{marginLeft:10,marginRight: 10}}>{item.tag}</Text>
            <Icon
                name='clear'
                iconStyle={{paddingLeft:10,paddingRight:10}}
                color= 'red' />
          </View>
        )
     })
  }
  
  show_social_modal_data = () =>{
    return(
      <View style={styles.modal_container}>
        <View style={styles.modal}>
          <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
            <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
              <Icon
                name='highlight-off'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
          </View>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontWeight:'bold',fontSize:18}}>{this.state.modalText}</Text>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <Form>
              {this.show_url_input()}
            </Form>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
          </View>
        </View>
      </View>
    );
  }

  show_schedule_modal_data = () =>{
    return(
      <View style={styles.modal_container}>
        <View style={styles.modalSchedule}>
          <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
            <TouchableOpacity onPress={() => this.setState({modalScheduleVisible: false})}>
              <Icon
                name='highlight-off'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
          </View>
          <View style={{flex:3.5,justifyContent:'center',alignItems:'center'}}>
            <ScrollView style={{width: windowWidth}}>
              {this.show_schedule_data()}
            </ScrollView>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
          </View>
        </View>
      </View>
    );
  }

  show_schedule_data = () =>{
    return this.state.schedule_time.map((item, key) =>{
      return(
        <View key={item.id} style={{height: 50,justifyContent:'center',alignItems:'center'}}>
          <TouchableOpacity onPress = {() => this.pick_schedule(item)}>  
            <Text style={{fontSize:15, fontWeight:'bold'}}>{item.name}</Text>
          </TouchableOpacity>       
        </View>
      )
    })
  }

  pick_schedule = (item) =>{
    if(this.state.selected_date == 'start'){
      this.state.available_array[this.state.selected_schedule].start_time = item.name
      this.setState({modalScheduleVisible: false})
    }else{
      this.state.available_array[this.state.selected_schedule].end_time = item.name
      this.setState({modalScheduleVisible: false})
    }
  }




  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => 
            this.setState({modalVisible:false})
          }
        >
          {this.show_social_modal_data()}
          
        </Modal>
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.modalScheduleVisible}
          onRequestClose={() => 
            this.setState({modalScheduleVisible: false})
          }
        >
          {this.show_schedule_modal_data()}
        </Modal>
          
        
        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:0.8}}>
            {
              this.show_icon()
            }
          </View>
          <View style={{flexDirection:'row',flex:0.8,width:windowWidth,marginRight:15,justifyContent: 'center', alignItems: 'center'}}>
            <Text >Contact Us </Text>
            <Icon
              name='email'
              size={20}
              />
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6}}>
            {this.show_text()}
          </View>
          <View style={{flex:0.2,justifyContent: 'flex-start',alignItems: 'center'}}>
          </View>
        </View>
        
        <View style={{flex:8,backgroundColor:'#F0F0F0',width: windowWidth}}>
              <ScrollView>
                <View style={{flex:5,justifyContent: 'center', alignItems: 'center'}}>
                  <View style={styles.redirectContainer}>
                    <View style={styles.redirectTextContainer}>
                      <Text style={{fontSize:18,fontWeight:'bold',marginBottom: 10}}>Business Information</Text>
                      <Item style={{width:windowWidth}} floatingLabel>
                        <Label style={{fontSize: 14,color: 'black'}} >Company Name</Label>
                        <Input />
                      </Item>
                      <Item style={{width:windowWidth}} floatingLabel>
                        <Label style={{fontSize: 14,color: 'black'}} >Address</Label>
                        <Input />
                      </Item>
                      <View style={{flexDirection:'row'}}>
                        <Item style={{width:(windowWidth)/3}} floatingLabel>
                          <Label style={{fontSize: 14,color: 'black'}} >City</Label>
                          <Input />
                        </Item>
                        <Item style={{width:(windowWidth)/3}} floatingLabel>
                          <Label style={{fontSize: 10,color: 'black'}} >State/Province</Label>
                          <Input />
                        </Item>
                        <Item style={{width:(windowWidth)/3}} floatingLabel>
                          <Label style={{fontSize: 10,color: 'black'}} >Zip/Postal Code</Label>
                          <Input />
                        </Item>
                      </View>
                    </View>
                    <View style={styles.redirectIconContainer}>
                      <TouchableOpacity>
                        <Icon
                          name='keyboard-arrow-down'
                          color='orange'
                          size={30}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom: 10}}>Services Provided</Text>
                    <View style={{flexGrow:1,flexWrap: 'wrap',flexDirection:'row',marginBottom:15,width:windowWidth}}>
                      { this.showshow_tags()}
                    </View>
                    <View style={{marginTop:5,borderBottomWidth:1,borderBottomColor: 'lightgrey',width: windowWidth-30,height: 40}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={  <Icon active name='arrow-drop-down' color= 'orange' />}
                      placeholder="Category"
                      placeholderStyle={{color: 'grey',fontSize: 14}}
                      placeholderTextColor='grey'
                      selectedValue={this.state.category}
                      onValueChange={this.onCategoryChange.bind(this)}
                    >
                      {this.showcat()}
                    </Picker>
                  </View>
                  <View style={{marginTop:5,borderBottomWidth:1,borderBottomColor: 'lightgrey',width: windowWidth-30,height: 40}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={  <Icon active name='arrow-drop-down' color= 'orange' />}
                      placeholder="Service"
                      placeholderStyle={{color: 'grey',fontSize: 14}}
                      placeholderTextColor='grey'
                      selectedValue={this.state.service}
                      onValueChange={this.onServiceChange.bind(this)}
                    >
                       {this.showser()}
                    </Picker>
                  </View>
                  </View>
                </View>
                
                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom: 10}}>Hours of Operation</Text>
                    <View style={{marginTop:15,flex:1,width:windowWidth}}>
                      {this.show_schedule()}
                    </View>
                  </View>
                  <View style={styles.redirectIconContainer}>
                    <TouchableOpacity>
                      <Icon
                        name='keyboard-arrow-down'
                        color='orange'
                        size={30}
                      />
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom: 10}}>Website Url</Text>
                    <Item style={{width:windowWidth}} floatingLabel>
                      <Label style={{fontSize: 14,color: 'black'}} >Website Url</Label>
                      <Input />
                    </Item>
                  </View>
                  <View style={styles.redirectIconContainer}>
                    <TouchableOpacity>
                      <Icon
                        name='keyboard-arrow-down'
                        color='orange'
                        size={30}
                      />
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom: 10}}>On Site Service?</Text>
                  </View>
                  <View style={styles.redirectIconContainer}>
                    <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                      <Text>{this.state.service_site}</Text>
                      <Switch
                        onValueChange = {(value) => this.toggleServiceSwitch(value)}
                        value = {this.state.service_toogle}/>
                </View>
                  </View>
                </View>


                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom: 10}}>Change Social Media Link</Text>
                    <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row',width: windowWidth-10}}>
                      {this.facebook_social()}
                      <View style={{width: 5}}></View>
                      {this.twitter_social()}
                    </View>
                    <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row',width: windowWidth-10}}>
                      {this.insta_social()}
                      <View style={{width: 5}}></View>
                      {this.in_social()}
                    </View>
                    <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row',width: windowWidth-10}}>
                      {this.youtube_social()}
                      <View style={{flex:1,width:40}}>
                      </View>
                    </View>
                  </View>
                  <View style={styles.redirectIconContainer}>
                    <TouchableOpacity>
                      <Icon
                        name='create'
                      />
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom: 10}}>Payment Accepted</Text>
                    <View style={{marginTop:15,flex:2,flexDirection: 'row'}}>
                      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text>Cash</Text>
                      </View>
                      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text>Master Card</Text>
                      </View>
                    </View>
                    <View style={{marginTop:15,flex:2,flexDirection: 'row'}}>
                      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text>Amex</Text>
                      </View>
                      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text>Visa</Text>
                      </View>
                    </View>
                    <View style={{marginTop:15,flex:2,flexDirection: 'row'}}>
                      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text>Discover</Text>
                      </View>
                      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text>Paypal</Text>
                      </View>
                    </View>
                  </View>
                </View>

                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom: 10}}>Modify Photo</Text>
                    <View style={styles.containerWrap}>
                      <ScrollView horizontal={true}>
                        <View style={{marginLeft:10,flex:1,justifyContent:'center',alignItems:'center',borderColor:'lightgreen',borderWidth:1}}>
                          <TouchableOpacity style={{justifyContent:'center',alignItems:'center'}} onPress={this._pickImage}>
                            <Icon active name='add-circle' iconStyle={{color:'orange'}}/>
                            <Text>  Upload Image  </Text>
                          </TouchableOpacity>
                        </View>
                        {this.show_uploaded()}
                      </ScrollView>
                    </View>
                  </View>
                </View>
                
                <View style={{marginTop:15,flex:2,justifyContent:'center',alignItems:'center'}}>
                  <TouchableOpacity style={{backgroundColor:'orange',height: 50,width: windowWidth-100,flex:1,justifyContent:'center',alignItems:'center'}} onPress={() => this.setState({modalVisible: false})}>
                    <Text style={{color:'white'}}>Save</Text>
                  </TouchableOpacity>
                </View>
                <View style={{marginTop:15,height: 30}}>
                </View>
              </ScrollView>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-start',marginLeft:15}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-back'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-end'}}>
            </View>
          </View>

        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
            <Icon
                name='home'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
            <Icon
                name='search'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
                name='favorite-border'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
            <Icon
                name='account-circle'
                color= 'orange' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'orange'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer:{
    flex:8,
    backgroundColor:"lightgrey",
    width:windowWidth
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  iconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'lightgrey'
  },
  icon:{
    height: 50,
    width: 50  
  },
  sub_container:{
    backgroundColor:'white',borderBottomWidth: 1,borderColor:'grey',flex:1,width: windowWidth,flexDirection:'row'
  },
  sub_text_container:{
    flex:0.4,width: windowWidth,flexDirection:'row',justifyContent:'flex-start',alignItems:'center'
  },
  sub_button_container:{
    flex:0.4,width: windowWidth,flexDirection:'row',justifyContent:'flex-end',alignItems:'center'
  },
  sub_text:{
    fontWeight: 'bold'
  },
  upgrade_button:{
    height: 35,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: 'orange',
    width: 90
  },
  sub_btn_text:{
    color: 'white',
    fontWeight:'bold'
  },
  containerWrap: {
  flex: 1,
  height:120,
  marginTop:10,
  flexDirection: 'row',
  },
  other_btn:{
    flex:1,
    marginLeft:5,
    marginRight:5,
    width: 30,
    backgroundColor: 'orange',
    height:50,
    flexDirection:'row'
  },
  otherdis_btn:{
    flex:1,
    marginLeft:5,
    marginRight:5,
    width: 30,
    backgroundColor: 'lightgrey',
    height:50,
    flexDirection:'row'
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  modalSchedule:{
    height:300,
    width: windowWidth-100 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  redirectContainer:{
    backgroundColor:'white',flexDirection:'row',flex:1.5, width:windowWidth,borderColor:'lightgrey',borderBottomWidth:1,borderTopWidth:1,paddingBottom:15
  },
  redirectTextContainer:{
    marginTop: 15,marginLeft:10,flex:0.7,alignItems:'flex-start',justifyContent:'flex-start'
  },
  redirectIconContainer:{
    marginTop: 15,marginRight:10,flex:0.3,alignItems:'flex-end',justifyContent:'flex-start'
  }

});
