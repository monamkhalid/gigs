import React from 'react';
import { AsyncStorage,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';
import Slideshow from 'react-native-image-slider-show';


var windowWidth = Dimensions.get('window').width
export default class MoreDetails extends React.Component {

  constructor(props){
    super(props);
    this.state={
      name: "BOBs RV Repair",
      service: "Solar Service",
      address: "25 48th St, Brooklyn, NY 11312, USA",
      position: 1,
      image: "http://inaction.brossardappdesign.com/site/inaction/public/assets/uploads/post_images//1556731055951366094.jpg",
      miles: 5,
      detail: "A style galary is a great a new way for you to share outfit photos",
      interval: null,
      isLoading:  true,
      icon_array:[
        {id: 0, tag: "cc-visa"},
        {id: 1, tag: "cc-amex"},
        {id: 2, tag: "cc-mastercard"},
        {id: 3, tag: "cc-discover"},
        {id: 4, tag: "cc-paypal"}
      ],
      tags:[
        {id: 0, tag: "Mechanical"},
        {id: 1, tag: "Electrical"},
        {id: 2, tag: "Mechanical/Generator"},
        {id: 3, tag: "Mechanical"},
        {id: 4, tag: "Inspactions"},
        {id: 5, tag: "Solar Panal"},
        {id: 6, tag: "Mechanical"}
      ],
      schedule:[
        {id: 0, name:"Monday",time:"10AM-5PM"},
        {id: 1, name:"Tuesday",time:"10AM-5PM"},
        {id: 2, name:"Wednesday",time:"10AM-5PM"},
        {id: 3, name:"Thursday",time:"10AM-5PM"},
        {id: 4, name:"Friday",time:"10AM-5PM"},
        {id: 5, name:"Saturday",time:"10AM-5PM"},
        {id: 6, name:"Sunday",time:"Closed"}
      ],
      dataSource: [
        {
          url: 'http://inaction.brossardappdesign.com/site/inaction/public/assets/uploads/post_images//1556731055951366094.jpg',
        }, {
          url: 'http://inaction.brossardappdesign.com/site/inaction/public/assets/uploads/post_images//15567305292118880388.jpg',
        }, {
          url: 'http://inaction.brossardappdesign.com/site/inaction/public/assets/uploads/post_images//155724651896591445.jpg',
        },
      ],
      
    }
    this.set_token()
  }

  set_token = async() =>{
    // var tokken = await AsyncStorage.getItem('token') 
    // await this.setState({token: tokken})
    // console.log(tokken)
    this.get_details(this.props.navigation.state.params.id)
  }

  get_details  =(id) => {
    console.log(id)
    try{
      fetch(BasePath+'getdetails?gid='+id, {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({services: responseJson.result,isLoading: false})
        console.log(responseJson)
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  
  componentDidMount(){
  }

  show_schedule = () =>{
     return this.state.services.schedule.map((item, key) =>{
        return(
          <View key={item.id} style={{width:windowWidth,height: 40,flexDirection:'row'}}>
            <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-start',marginLeft:10}}>
              <Text>{item.name}</Text>
            </View>
            <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-end',marginRight:10}}>
              <Text>{item.time}</Text>
            </View>
          </View>
        )
     })
  }

  showshow_tags = () =>{
     return this.state.services.tags.map((item, key) =>{
        return(
          <View key={item.id} style={{height: 30,marginLeft: 5,marginTop: 5,marginRight:5,borderRadius:20,backgroundColor:'grey',justifyContent:'center',alignItems:'center'}}>
            <Text>{item.tag}</Text>
          </View>
        )
     })
  }

  icon_uploaded = () =>{
    return(
      this.state.icon_array.map((item, key) =>{
        return(
          <View key={key} style={{marginRight: 7,marginLeft: 7,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Icon
              name={item.tag}
              type="font-awesome"
              size={40}
              containerStyle={{marginRight: 10,justifyContent: 'center',alignItems: 'flex-start'}}
            />
          </View>
        )
      })
    )
  }

  show_slider = () =>{
    if(this.state.services.datasource.length > 0 ){
      return(
        <Slideshow 
          dataSource={this.state.services.datasource}
          position={this.state.position}
          onPositionChanged={position => this.setState({ position })} />
      )
    }else{
      return null;
    }

  }

  render() {
    const { navigate } = this.props.navigation;
    if (this.state.isLoading){
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <ActivityIndicator />
        </View>
      )
    }else{
      return (
        <View style={styles.container}>
          <View style={styles.heading}>
            <View style={{flex:0.85,justifyContent:'flex-end'}}>
            <Image
              style={{
                height :60,
                width: 60
              }}
              source={require('../../assets/logo.png')}
            />
            </View>
            <View style={{flex:1.15,justifyContent:'flex-end'}}>
            <Image
              style={{
                height :40,
                width: 40
              }}
              source={require('../../assets/cart.png')}
            />
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row',width: windowWidth-70}}>
            <View style={{flex:0.2}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-back'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
            <View style={{flex:0.6,justifyContent:'flex-start',alignItems:'center'}}>
              <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
              <View><Text style={{fontSize: 14}}>{this.state.services.servicename}</Text></View>
              <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
            </View>
            <View style={{flex:0.2}}></View>
          </View>
          <View style={{flex:8,backgroundColor:'#EEEEEE',width: windowWidth}}>
            <ScrollView>
              <View style={{flex:4,justifyContent: 'center',alignItems: 'center'}}>
                {
                  this.show_slider()
                }
              </View>
              <View style={{flex:6,backgroundColor: 'white'}}>
                <View style={{flex:3.5,flexDirection:'row',marginTop:15,marginBottom:15}}>
                  <View style={{flex: 0.3,justifyContent:'center',alignItems:'center'}}>
                    <Image
                      style={{
                        height :50,
                        width: 50
                      }}
                       source={{uri: this.state.image}}
                    />
                  </View>
                  <View style={{flex: 0.45,justifyContent:'center',alignItems:'flex-start'}}>
                    <View style={{flexDirection:'row'}}>
                      <Text style={{fontWeight:'bold'}}>{this.state.services.servicename}</Text>
                      <Icon
                        name='favorite'
                        size={16}
                      />
                    </View>
                    <Text style={{fontSize:10,color: 'grey',fontWeight:'bold',marginTop:5}}>{this.state.service}</Text>
                    <View style={{flexDirection:'row',marginTop:5}}>
                      <View style={{flexDirection:'row',height:20,width:60,justifyContent:'center',alignItems:'center',backgroundColor:'lightgreen',borderRadius:5}}>
                        <Icon
                          name='star'
                          size= {10} />
                        <Text style={{fontSize:10,fontWeight:'bold',marginLeft:10}}>5.0</Text>
                      </View>
                      <Text style={{marginLeft:5,marginTop:5,fontSize:10,color: 'grey',fontWeight:'bold'}}>{this.state.reviews} Reviews</Text>
                    </View>
                    <Text style={{fontSize:10,color: 'grey',fontWeight:'bold',marginTop:5}}>{this.state.services.address}</Text>
                  </View>
                  <View style={{flex: 0.25}}>
                    <Text style={{fontSize:10,color: 'grey',fontWeight:'bold', textAlign: 'center'}}>{this.state.services.miles} miles away</Text>
                    <Icon
                      name='phone-in-talk'
                      containerStyle={{marginTop:5,justifyContent: 'center',alignItems: 'center'}}
                      color= 'grey' />
                      <Text style={{fontSize:10,color: 'grey',fontWeight:'bold', textAlign: 'center'}}>Call</Text>
                  </View>
                </View>
              </View>
              <View style={{flex:2,backgroundColor: 'white'}}>
                <View style={{flex:1}}>
                  <Text style={{marginLeft: 10,fontWeight: 'bold',fontSize:15,color:'grey'}}>SERVICE PROVIDED</Text>
                </View>
                <View style={{flexGrow:1,flexWrap: 'wrap',flexDirection:'row',marginBottom:15}}>
                  { this.showshow_tags()}
                </View>
              </View>

              <View style={{flex:1, height:80,borderWidth:1,borderColor:'grey',backgroundColor:'white', borderStyle:'dashed',flexDirection:'row',justifyContent: 'center'}}>
                <View style={{flex:0.2,alignItems:'center',justifyContent: 'center'}}>
                  <Icon
                  name='cog'
                  type="font-awesome"
                  color= 'grey' />
                </View>
                <View style={{flex:0.5,alignItems:'center',justifyContent: 'center',alignItems:'flex-start'}}>
                  <Text style={{fontWeight: 'bold',color:'grey'}}>On Site Service</Text>
                </View>
                <View style={{flex:0.3,alignItems:'center',justifyContent: 'center'}}>
                  <Text style={{fontWeight: 'bold',fontSize:12,color:'grey'}}>{this.state.services.on_site}</Text>
                </View>
              </View>
              <View style={{flex:1, height: 20,backgroundColor:'white'}}>
              </View>
              <View style={{flex:1, height: 100,flexDirection:'row',backgroundColor:'white'}}>
                <View style={{flex:0.5,justifyContent:'center',borderWidth: 2,borderColor: 'grey',alignItems:'center'}}>
                  <Icon
                  name='whatsapp'
                  type="font-awesome"
                  color= 'green' />
                  <Text style={{fontWeight: 'bold',fontSize:12,color:'grey'}}>WHATSAPP</Text>
                </View>
                <View style={{flex:0.5, justifyContent:'center',alignItems:'center',borderWidth: 2,borderColor: 'grey'}}>
                  <Icon
                  name='globe'
                  type="font-awesome"
                  color= '#4dabf7' />
                  <Text style={{fontWeight: 'bold',fontSize:12,color:'grey'}}>WEBSITE</Text>
                </View>
              </View>
              <View style={styles.redirectSContainer}>
                <View style={styles.redirectSTextContainer}>
                  <Text style={{fontSize:18,fontWeight:'bold'}}>Hours of Operation</Text>
                  {this.show_schedule()} 
                </View>
                <View style={styles.redirectIconContainer}>
                  <TouchableOpacity>
                    <Icon
                      name='keyboard-arrow-up'
                      color='orange'
                      size={30}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.redirectContainer}>
                <View style={styles.redirectTextContainer}>
                  <Text style={{fontSize:18,fontWeight:'bold'}}>A Little About Our Service</Text>
                  <Text>{this.state.services.detail}</Text>
                </View>
                <View style={styles.redirectIconContainer}>
                  <TouchableOpacity>
                    <Icon
                      name='keyboard-arrow-up'
                      color='orange'
                      size={30}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{flex:1, marginTop:15,height: 130, backgroundColor:'white'}}>
                <View style={{flex:1,margin:15}}>
                  <Text style={{fontSize:18,fontWeight:'bold'}}>Payment Options</Text>
                </View>
                <ScrollView horizontal={true}>
                  {this.icon_uploaded()}
                </ScrollView>
              </View>
              <View style={{flex:1, height: 20, alignItems: 'center',justifyContent: 'center'}}>
              </View>
            </ScrollView>
          </View>
          <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
            <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                  name='home'
                  color= 'orange' />
              </TouchableOpacity>
              <Text style={{fontSize:12,color:'orange'}}>HOME</Text>
            </View>
            <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
              <Icon
                  name='search'

                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
            </View>
            <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
              <Icon
                  name='favorite-border'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize:12,color:'grey'}}>FAVORITES</Text>
            </View>
            <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
              <Icon
                  name='account-circle'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize:12,color:'grey'}}>PROFILE</Text>
            </View>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  redirectContainer:{
    backgroundColor:'white',marginTop:15,flexDirection:'row',height: 120, width:windowWidth,borderColor:'lightgrey',borderBottomWidth:1
  },
  redirectSContainer:{
    backgroundColor:'white',marginTop:15,flexDirection:'row', width:windowWidth,borderColor:'lightgrey',borderBottomWidth:1
  },
  redirectSTextContainer:{
    marginTop: 15,flex:1,width:windowWidth
  },
  redirectTextContainer:{
    marginTop: 15,marginLeft:10,flex:0.7,alignItems:'flex-start',justifyContent:'flex-start'
  },
  redirectIconContainer:{
    marginTop: 15,marginRight:10,flex:0.3,alignItems:'flex-end',justifyContent:'flex-start'
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'}
});
