import React from 'react';
import { ListView, Platform, Modal, ImageBackground, Image, KeyboardAvoidingView, ScrollView, TextInput, ActivityIndicator, TouchableOpacity, Dimensions, StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox, Icon } from 'react-native-elements';
import { WP, HP } from './responsive';
import BaseUrl from '../../config/path.js';
var windowWidth = Dimensions.get('window').width
export default class Service extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      search: true,
      isLoading: false,
      img: [{}, {}, {}, {}, {}, {}, {}, {}]
      // selected_item: this.props.navigation.state.param.id
    }
    // this.populateList()
    this.login_request(this.props.navigation.state.params.id)
  }

  set_token = async () => {
    var tokken = await AsyncStorage.getItem('token')
    await this.setState({ token: tokken })
    this.get_data()
  }

  login_request = (id) => {
    console.log(id)
    try {
      fetch(BasePath + 'getallusers?cid=' + id, {
        method: 'GET',
        mode: 'same-origin',

      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if (responseJson.status) {
            this.populateList(responseJson.result)
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } catch (e) {
      console.log('error', e);
    }
  }

  componentDidMount() {
  }

  populateList(dataSource) {

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.setState({ dataSource: ds.cloneWithRows(dataSource), isLoading: true });
    this.state.db = dataSource
  };


  search_data = () => {
    if (this.state.search) {
      return (
        <View style={{ flex: 7 }}>
          <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{
                height: 100,
                width: 180
              }}
              source={require('../../assets/logo-with-text.png')}
            />
          </View>
          <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'orange' }}>Search for a professional</Text>
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'orange' }}>Provider near you</Text>
          </View>
          <View style={{ flex: 2 }}>
            <Form>
              <Item style={{ width: windowWidth - 45 }} floatingLabel>
                <Label style={{ fontSize: 10, color: 'grey' }} >(Search by Name, Type of Services, Market or Zip Code)</Label>
                <Input />
              </Item>
              <Icon
                name='search'
                size={40}
                iconStyle={{ position: 'relative', top: -35 }}
                containerStyle={{ width: windowWidth - 45, justifyContent: 'flex-end', alignItems: 'flex-end' }}
                color='orange' />
            </Form>
          </View>
          <View style={{ flex: 1 }}>
          </View>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 7 }}>
          <View style={{ flex: 3.5, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{
                height: 130,
                width: 150
              }}
              source={require('../../assets/sorrynotfound.png')}
            />
          </View>
          <View style={{ flex: 2.5, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Aw Shucks!</Text>
            <Text style={{ fontSize: 16 }}>There are no result found.</Text>
            <Text style={{ fontSize: 16 }}>Please search using another search word.</Text>
          </View>
          <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity style={{ height: 50, width: windowWidth - 90, backgroundColor: 'orange', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 16, color: 'white' }}>Search Again</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.5 }}>
          </View>
        </View>
      );
    }
  }

  show_icon = () => {
    if (this.state.search) {
      return (
        <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-start' }}>
          <Icon
            name='search'
            containerStyle={{ alignItems: 'flex-start' }}
            size={40}
            color='orange' />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
          <Icon
            name='highlight-off'
            size={30}
            containerStyle={{ alignItems: 'flex-end' }}
            color='grey' />
        </View>
      )
    }
  }

  show_text = () => {
    if (this.state.search) {
      return (
        <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center' }}>
          <View style={{ width: 70, borderWidth: 1, borderColor: 'lightgrey', justifyContent: 'center', alignItems: 'center' }}></View>
          <View><Text style={{ fontSize: 14 }}>Search</Text></View>
          <View style={{ marginTop: 2, width: 70, borderWidth: 1, borderColor: 'lightgrey' }}></View>
        </View>
      );
    } else {
      return null;
    }
  }

  show_images = () => {
    return (
      <View style={styles.containerWrap}>
        <ScrollView horizontal={true}>
          <View style={{ marginRight: 15, flex: 0.8, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{
                height: 70,
                width: 70
              }}
              source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
            />
          </View>
          <View style={{ marginRight: 15, flex: 0.8, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{
                height: 70,
                width: 70
              }}
              source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
            />
          </View>
          <View style={{ marginRight: 15, flex: 0.8, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{
                height: 70,
                width: 70
              }}
              source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
            />
          </View>
          <View style={{ marginRight: 15, flex: 0.8, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{
                height: 100,
                width: 100
              }}
              source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
            />
          </View>
          <View style={{ marginRight: 15, flex: 0.8, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{
                height: 70,
                width: 70
              }}
              source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }



  _renderRow(follow, sectionID, rowID) {
    console.log()
    return (
      <View style={styles.listContainer}>
        <View style={{ flex: 3.5, flexDirection: 'row' }}>
          <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
            {/* <Image
              style={{
                height :50,
                width: 50
              }}
               source={{uri: }}
            /> */}
          </View>
          <View style={{ flex: 0.45, justifyContent: 'center', alignItems: 'flex-start' }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ fontWeight: 'bold' }}></Text>
              <Icon
                name='favorite'
                size={16}
              />
            </View>
            <Text style={{ fontSize: 10, color: 'grey', fontWeight: 'bold', marginTop: 5 }}>{'service'}</Text>
            <View style={{ flexDirection: 'row', marginTop: 5 }}>
              <View style={{ flexDirection: 'row', height: 20, width: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: 'lightgreen', borderRadius: 5 }}>
                <Icon
                  name='star'
                  size={10} />
                <Text style={{ fontSize: 10, fontWeight: 'bold', marginLeft: 10 }}>5.0</Text>
              </View>
              <Text style={{ marginLeft: 5, marginTop: 5, fontSize: 10, color: 'grey', fontWeight: 'bold' }}>{'reviews'} Reviews</Text>
            </View>
            <Text style={{ fontSize: 10, color: 'grey', fontWeight: 'bold', marginTop: 5 }}>{'lahore'}</Text>
          </View>
          <View style={{ flex: 0.25 }}>
            <Text style={{ fontSize: 10, color: 'grey', fontWeight: 'bold' }}>{'miles'}</Text>
            <Icon
              name='phone-in-talk'
              containerStyle={{ marginTop: 5, justifyContent: 'center', alignItems: 'center' }}
              color='grey' />
          </View>
        </View>
        <View style={{ flex: 3, flexDirection: 'row', height: 70 }}>
          <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center', backgroundColor: 'grey' }}>
            <Text style={{ color: 'lightgrey', fontSize: 11 }}>Markets Served</Text>
            <Text style={{ fontWeight: 'bold' }}>NY, CT, NJ</Text>
          </View>
          <View style={{ flex: 0.45, marginTop: 10 }}>
            <View style={{ flexDirection: 'row', marginLeft: 5 }}>
              <Icon
                name='comment'
                containerStyle={{ marginTop: 5, justifyContent: 'center', alignItems: 'center' }}
              />
              <Text style={{ margin: 10, fontSize: 10, color: 'grey', fontWeight: 'bold' }}>Text</Text>
            </View>
            <View style={{ flexDirection: 'row', marginLeft: 5 }}>
              <Icon
                name='email'
                containerStyle={{ marginTop: 5, justifyContent: 'center', alignItems: 'center' }}
              />
              <Text style={{ margin: 10, fontSize: 10, color: 'grey', fontWeight: 'bold' }}>{'usama@gmail.com'}</Text>
            </View>
          </View>
        </View>
        <View style={{ margin: 10, flex: 2, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height: 15,
                width: 15
              }}
              source={require('../../assets/fb.png')}
            />
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height: 15,
                width: 15
              }}
              source={require('../../assets/ttr.png')}
            />
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height: 15,
                width: 15
              }}
              source={require('../../assets/youtube.png')}
            />
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height: 15,
                width: 15
              }}
              source={require('../../assets/insta.png')}
            />
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={{
                height: 15,
                width: 15
              }}
              source={require('../../assets/in.png')}
            />
          </View>
        </View>
        <View style={{ flex: 1.5, flexDirection: 'row' }}>
          <View style={{ flex: 0.45, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.push('MoreDetail', { id: '1' })}>
              <Text style={{ color: 'orange', fontWeight: 'bold' }}>MORE DETAILS</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.10 }}></View>
          <View style={{ flex: 0.45 }}>
            <TouchableOpacity style={{ height: 40, justifyContent: 'center', alignItems: 'center', backgroundColor: 'orange', width: (windowWidth - 40) / 2, flexDirection: 'row' }} onPress={() => this.props.navigation.push('AddReviews', { id: '1' })}>
              <Icon
                name='add-circle'
                size={12}
              />
              <Text style={{ marginLeft: 5, color: 'white', fontWeight: 'bold' }}>Add REVIEW</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  show_list_data = () => {
    if (this.state.isLoading) {
      return (
        <ListView
          style={{ width: '100%' }}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow.bind(this)}
        />
      )
    } else {
      return (
        <View styke={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator color="black" />
        </View>
      )
    }
  }


  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>

        <View style={styles.heading}>
          <View style={{ flex: 0.85, justifyContent: 'flex-end' }}>
            <Image
              style={{
                height: 60,
                width: 60
              }}
              source={require('../../assets/logo.png')}
            />
          </View>
          <View style={{ flex: 1.15 }}>
            {
              this.show_icon()
            }
          </View>
        </View>
        <View style={{ flex: 1, flexDirection: 'row', width: windowWidth - 40 }}>
          <View style={{ flex: 0.2 }}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                name='arrow-back'
                containerStyle={{ justifyContent: 'center', alignItems: 'flex-start' }}
                color='grey' />
            </TouchableOpacity>
            <Text style={{ fontSize: 10 }}>Previous</Text>
          </View>
          <View style={{ flex: 0.6 }}>
            {this.show_text()}
          </View>
          <View style={{ flex: 0.2, justifyContent: 'flex-start', alignItems: 'center' }}>
            <TouchableOpacity>
              <Icon
                name='arrow-forward'
                containerStyle={{}}
                color='grey' />
            </TouchableOpacity>
            <Text style={{ fontSize: 10 }}>Next</Text>
          </View>
        </View>
        <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Text style={{ color: 'orange', fontWeight: 'bold' }}>ALL</Text>
          <Icon
            name='arrow-drop-down'
            containerStyle={{ justifyContent: 'center', alignItems: 'flex-start' }}
            color='orange' />
        </View>
        <View style={{ height: HP('68') }}>

          <ScrollView style={{ height: HP('60'), backgroundColor: '#f4f4f4' }}>
            <View style={{  backgroundColor: '#fff', marginVertical: 5, shadowOpacity: 0.5 }}>
              <View style={{ width: '100%', flexDirection: 'row', marginTop: 5 }}>
                <ScrollView
                  horizontal
                  showsHorizontalScrollIndicator={false}
                >
                  {
                    this.state.img.map((item, key) => {
                      return (
                        <View style={{ height: HP('10'), width: 100, marginHorizontal: 3, marginVertical: 2, backgroundColor: '#fff', elevation: 1.5, shadowOpacity: 0.15, alignItems: 'center', justifyContent: 'center', borderRadius: 2 }}>
                          <Image
                            style={{
                              height: 50,
                              width: 50
                            }}
                            source={require('../../assets/rocket.png')}
                          />
                        </View>
                      )
                    })
                  }
                </ScrollView>
              </View>
              <View style={{ height: '6%', width: '100%', flexDirection: 'row', marginVertical: 5 }}>
                <View style={{ flexDirection: 'row', width: '50%' }}>
                  <Text style={{ fontSize: 14, color: 'black', fontWeight: '500', alignSelf: 'center', marginLeft: 5 }}>Bob's RV Repair</Text>
                  <Image
                    style={{
                      marginHorizontal: 5,
                      alignSelf: 'center',
                      height: 20,
                      width: 20
                    }}
                    source={require('../../assets/rocket.png')}
                  />
                </View>
                <View style={{ width: '50%', justifyContent: 'center' }}>
                  <Text style={{ fontSize: 13, color: 'gray', fontWeight: '400', alignSelf: 'flex-end', marginRight: 5 }}>1.3 Miles away</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginLeft: 5 }}>
                <Text style={{ fontSize: 13, color: 'gray' }}>Mechanical</Text>
                <Text style={{ fontSize: 13, color: 'gray', marginLeft: 10 }}>Electrical Slides</Text>
              </View>
              <View style={{ flexDirection: 'row', marginVertical: 3 }}>
                <View style={{ backgroundColor: 'green', borderRadius: 2, marginHorizontal: 5 }}>
                  <Text style={{ color: '#fff', marginHorizontal: 10, marginVertical: 2, alignSelf: 'center' }}>* 5.0</Text>
                </View>
                <Text style={{ flex: 1, alignSelf: 'center', color: 'gray', fontSize: 12 }}>21 Reviews</Text>
                <Image
                  style={{
                    marginHorizontal: 5,
                    alignSelf: 'center',
                    height: 20,
                    width: 20
                  }}
                  source={require('../../assets/rocket.png')}
                />
              </View>
              <View style={{ flexDirection: 'row', marginHorizontal: 5, justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 12, color: 'black' }}>Lahore pakistan</Text>
                <Text style={{ fontSize: 12, color: 'black' }}>Call</Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: '1%' }}>
                <View style={{ height: 35, width: '22%', backgroundColor: '#f0f0f0', borderBottomRightRadius: 4, borderTopRightRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 10, color: 'gray' }}>Market Served</Text>
                  <Text style={{ fontSize: 13, color: 'black', fontWeight: '500' }}>NY. CT. NJ</Text>
                </View>
                <View style={{ marginHorizontal: 10 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon
                      type='antdesign'
                      name='message1'
                      color='orange'
                      size={16}
                    />
                    <Text style={{ fontSize: 12, color: 'black', marginHorizontal: 7 }}>Text</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon
                      type='entypo'
                      name='mail'
                      color='orange'
                      size={16}
                    />
                    <Text style={{ fontSize: 12, color: 'black', marginHorizontal: 7 }}>test@test.com</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon
                      reverse
                      name='facebook'
                      type='font-awesome'
                      color='orange'
                      size={13}
                      iconStyle={{}}
                      reverseColor='#fff'
                    />
                    <Icon
                      reverse
                      name='twitter'
                      type='font-awesome'
                      color='orange'
                      size={13}
                      iconStyle={{}}
                      reverseColor='#fff'
                    />
                    <Icon
                      reverse
                      name='instagram'
                      type='font-awesome'
                      color='orange'
                      size={13}
                      iconStyle={{}}
                      reverseColor='#fff'
                    />
                    <Icon
                      reverse
                      name='youtube'
                      type='font-awesome'
                      color='orange'
                      size={13}
                      iconStyle={{}}
                      reverseColor='#fff'
                    />
                    <Icon
                      reverse
                      name='linkedin'
                      type='font-awesome'
                      color='orange'
                      size={13}
                      iconStyle={{}}
                      reverseColor='#fff'
                    />
                  </View>
                </View>
              </View>
              <View style={{ height: HP('6'), width: '100%', flexDirection: 'row', borderTopColor: '#c4c4c4', borderTopWidth: 0.6 }}>
                <View style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 16, color: 'orange' }} >MORE DETAILS</Text>
                </View>
                <TouchableOpacity style={{ width: '50%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'orange' }}
                  onPress={() => this.props.navigation.push('AddReviews')}
                >
                  <Text style={{ fontSize: 16, color: 'white' }} >ADD REVIEW</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginBottom: 10,width: '100%', shadowOpacity: 0.5, elevation: 2, backgroundColor: '#f9f9f9' }}>
              <View style={{ marginVertical: 10,width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                <View style={{width: '23%', backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', marginHorizontal: 10 }}>
                  <Image
                    style={{
                      marginHorizontal: 5,
                      alignSelf: 'center',
                      height: 60,
                      width: 60
                    }}
                    source={require('../../assets/rocket.png')}
                  />
                </View>
                <View style={{ width: '70%' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '95%' }}>
                    <Text style={{ color: 'black' }}>John's RV Solar</Text>
                    <Text style={{ color: 'gray' }}>5 miles away</Text>
                  </View>
                  <Text style={{ color: 'gray' }}>Solar System</Text>
                  <View style={{ flexDirection: 'row', marginVertical: 3 }}>
                    <View style={{ backgroundColor: 'green', borderRadius: 2, marginHorizontal: 5 }}>
                      <Text style={{ color: '#fff', marginHorizontal: 10, marginVertical: 2, alignSelf: 'center' }}>* 5.0</Text>
                    </View>
                    <Text style={{ flex: 1, alignSelf: 'center', color: 'gray', fontSize: 12 }}>21 Reviews</Text>
                    <Image
                      style={{
                        marginHorizontal: 5,
                        alignSelf: 'center',
                        height: 20,
                        width: 20
                      }}
                      source={require('../../assets/rocket.png')}
                    />
                  </View>
                  <View style={{ flexDirection: 'row', marginHorizontal: 5, justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 12, color: 'black' }}>Lahore pakistan</Text>
                    <Text style={{ fontSize: 12, color: 'black' }}>Call</Text>
                  </View>
                </View>

              </View>
              <View style={{ flexDirection: 'row', height: '35%' }}>
                <View style={{ height: 40, width: '26%', backgroundColor: '#f0f0f0', marginTop: 5, borderBottomRightRadius: 4, borderTopRightRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 10, color: 'gray' }}>Market Served</Text>
                  <Text style={{ fontSize: 13, color: 'black', fontWeight: '500' }}>NY. CT. NJ</Text>
                </View>
                <View style={{ height: '100%', width: '74%', alignItems: 'flex-end' }}>
                  <TouchableOpacity style={{ height: HP('6'), width: '65%', marginTop: WP('1'),justifyContent: 'center', alignItems: 'center', backgroundColor: 'orange' }}
                    onPress={() => this.props.navigation.push('AddReviews')}
                  >
                    <Text style={{ fontSize: 16, color: 'white' }} >ADD REVIEW</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ScrollView>

          <View style={{ height: HP('6'), flexDirection: 'row', width: windowWidth - 40 }}>
            <View style={{ flex: 0.5, justifyContent: 'flex-end', alignItems: 'flex-start', marginLeft: 15 }}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-back'
                  color='grey' />
              </TouchableOpacity>
              <Text style={{ fontSize: 10 }}>Previous</Text>
            </View>
            <View style={{ flex: 0.5, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
              <TouchableOpacity >
                <Icon
                  name='arrow-forward'
                  color='grey' />
              </TouchableOpacity>
              <Text style={{ fontSize: 10 }}>Next</Text>
            </View>
          </View>
        </View>
        <View style={{ flexDirection: 'row', height: HP('10'), justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                name='home'
                color='orange' />
            </TouchableOpacity>
            <Text style={{ fontSize: 12, color: 'orange' }}>HOME</Text>
          </View>
          <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
              <Icon
                name='search'

                color='grey' />
            </TouchableOpacity>
            <Text style={{ fontSize: 12, color: 'grey' }}>SEARCH</Text>
          </View>
          <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
              <Icon
                name='favorite-border'
                color='grey' />
            </TouchableOpacity>
            <Text style={{ fontSize: 12, color: 'grey' }}>FAVORITES</Text>
          </View>
          <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
              <Icon
                name='account-circle'
                color='grey' />
            </TouchableOpacity>
            <Text style={{ fontSize: 12, color: 'grey' }}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    flex: 8,
    backgroundColor: "white",
    paddingVertical: 15,
    marginBottom: 10,
    width: windowWidth
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    // flex: 1.5,
    height: HP('12'),
    flexDirection: 'row',
    width: windowWidth - 20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  iconContainer: {
    borderRadius: 30 / 2,
    height: 30,
    width: 30,
    overflow: 'hidden',
    marginLeft: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'lightgrey'
  },
  fbiconContainer: {
    borderRadius: 30 / 2,
    height: 30,
    width: 30,
    overflow: 'hidden',
    marginLeft: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'orange'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth - 45,
    marginHorizontal: 20,

  },
  contest_button: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth - 45,
    marginTop: 15,
    borderRadius: 10
  },
  icon: {
    height: 50,
    width: 50
  },
  modal: {
    height: 300,
    width: windowWidth - 40,
    backgroundColor: 'white',
    borderRadius: 10
  },
  containerWrap: {
    marginTop: 5,
    flex: 3,
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  modal_container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'
  }
});
