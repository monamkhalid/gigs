import React from 'react';
import { ImageBackground,AsyncStorage,Image, KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left,Picker } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import { ImagePicker,Permissions } from 'expo';
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class ProviderProfile extends React.Component {
  constructor(props){
    super(props);
    this.state={
      email: '',
      password: '',
      company_name:'',
      address:'',
      city:'',
      state:'',
      zip:'',
      phone:'',
      contact:'',
      category: undefined,
      service: undefined,
      show_error: false,
      error_message: '',
      isLoading: false,
      base64_str: null,
      local_image_url: null,
      isNoChecked: false,
      isYChecked: false,
      show_category: false,
      show_service: false
    }
    this.get_categories()
  }

  get_categories = () =>{
    try{
      fetch(BasePath+'getcategory', {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({catdata: responseJson.result,show_category:true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  componentDidMount(){
  }

  show_error_message = () =>{
    if(this.state.show_error){
      return(
        <Text style={{margin:10,color: 'red',textAlign: 'center'}}>{this.state.error_message}</Text>
      )
    }
  }

  storedate = async (response) =>{
    const { navigate } = this.props.navigation;

    console.log('**************************')
    console.log(response)
    try{
      await AsyncStorage.setItem('token', response.token);
      await AsyncStorage.setItem('role', response.role);
      this.props.navigation.push('ProviderCheckout')
    }catch(e){
      console.log(e)
    }

  }

  _pickImage = async () => {
    const { status: cameraRollPerm } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if (cameraRollPerm === 'granted') {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
        base64: true
      });
      if (!result.cancelled) {
        console.log(result)
        // filename = result.uri.substring(result.uri.lastIndexOf('/')+1);
        // this.uploadImage(result.uri, filename)
        this.setState({ base64_str: result.base64 });
        this.setState({ local_image_url: result.uri, show_image: true });
      }
    }
  }

  login_request = () =>{
    console.log(this.state.email)
    console.log(this.state.password)
    try{
      fetch(BasePath+'signup', {
        method: 'POST',
        mode: 'same-origin',
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password,
          company_name: this.state.company_name,
          phone: this.state.phone,
          contact: this.state.contact,
          category: this.state.category,
          service: this.state.service,
          address: this.state.address,
          city: this.state.city,
          state: this.state.state,
          zip: this.state.zip,
          onsite: this.state.rv,
          profileimg: this.state.base64_str,
          role: 1

        })
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({isLoading: false})
          this.storedate(responseJson)
        
        }else{
          this.setState({isLoading: false, error_message: responseJson.result.message, show_error: true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  set_rv = () =>{
    if(this.state.isNoChecked){
      this.setState({rv: 'no'})
    }
    if(this.state.isYChecked){
      this.setState({rv: 'yes'})
    }
  }

  onCategoryChange(value: string) {
    this.setState({
      category: value
    });
    this.get_services(value)
  }

  get_services = (value) =>{
    console.log(value)
    try{
      fetch(BasePath+'getservice?sid='+value, {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({servdata: responseJson.result,show_service:true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  onServiceChange(value: string) {
    this.setState({
      service: value
    });
  }

  form_validation = () => {
    if(this.state.email === '' || this.state.password === '' || this.state.company_name === '' || this.state.address === '' || this.state.city === '' || this.state.state === '' || this.state.zip === '' || this.state.phone === '' || this.state.contact === '' ){
      this.setState({isLoading: false,error_message: 'All Fields are required', show_error: true})
    }else if (!this.state.base64_str){
      this.setState({isLoading: false,error_message: 'Please pick an Image', show_error: true})
    }else{
      this.login_request()
    }
  }

  showcat = () =>{
    if(this.state.show_category){
      return this.state.catdata.map((item, key) =>{
        return(
          <Picker.Item key={item.id} label={item.name} value={item.id} />
        )
      })
    }else{
      return null
    }
  }

  showser = () =>{
    if(this.state.show_service){
      return this.state.servdata.map((item, key) =>{
        return(
          <Picker.Item key={item.id} label={item.name} value={item.id} />
        )
      })
    }else{
      return null
    }
  }

  provider_signup = async() =>{
    this.setState({isLoading : true})
    await this.set_rv()
    this.form_validation()
  }

  forgot_password = () =>{
    console.log("start Contest")
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View style={styles.container}>
        <View style={{flex:2,flexDirection:'row',height :120}}>
          <View style={{flex:1.2,justifyContent: 'flex-end', alignItems: 'flex-end'}}>
            <Image
              style={{
                height :60,
                width: 100
              }}
              source={require('../../assets/logo.png')}
            />
          </View>
          <View style={{flex:0.8,width:windowWidth,marginRight:15,justifyContent: 'center', alignItems: 'flex-end'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Login')}>
              <Icon
                name='highlight-off'
                color= 'grey' />
            </TouchableOpacity>
            </View>
        </View>
        <View style={{flex:0.6,marginTop:10,justifyContent:'flex-start',alignItems:'center'}}>
          <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
          <View><Text style={{fontSize: 10,fontWeight:'bold'}}>Provider Profile</Text></View>
          <View style={{marginTop:3,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
        </View>
        <View style={{flex:1,marginTop: 10, justifyContent: 'center', alignItems: 'center',flexDirection: 'row'}}>
          <Text style={{fontSize: 10}}>Already subscribed? Click here to </Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={{textDecorationLine: 'underline',color: 'orange',fontSize: 10,textAlign: 'center'}}>Sign in</Text>
          </TouchableOpacity>
        </View>
        <View style={{marginTop:10,justifyContent: 'center',alignItems: 'center',}}>
          <View style={{justifyContent: 'center',alignItems: 'center', width: 80,height: 80,borderRadius:80/2,backgroundColor:'#F0F0F0',overflow: 'hidden'}}>
            { !this.state.show_image ? 
              <Image
                style={{height:40,width:40}}
                source={require('../../assets/icon.png')}
              /> : 
               <Image source={{ uri: this.state.local_image_url }} style={{ width: 40, height: 40 }} />
            }
            
          </View>
          <TouchableOpacity  onPress={this._pickImage}>
            <Icon active name='add-circle' iconStyle={{position: 'relative',left: 30,top:-30,color:'orange'}}/>
          </TouchableOpacity>
          <Text style={{position:'relative',top:-20,fontSize:10,textAlign: 'center'}}>Profile Photo or Logo</Text>
        </View>
        <View style={{position:'relative',top:-30,flex:5,justifyContent: 'center', alignItems: 'center'}}>
          <Content>
            {this.show_error_message()}
            <Form>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{fontSize: 14,color: 'grey'}} >Email</Label>
                <Input onChangeText={(text) => this.setState({ email: text })} />
              </Item>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{fontSize: 14,color: 'grey'}} >Password</Label>
                <Input onChangeText={(text) => this.setState({ password: text })} secureTextEntry={true} />
              </Item>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{fontSize: 14,color: 'grey'}} >Company Name</Label>
                <Input onChangeText={(text) => this.setState({ company_name: text })} />
              </Item>
              <Item style={{width:windowWidth-45}} floatingLabel >
                <Label style={{fontSize: 14,color: 'grey'}} >Address</Label>
                <Input onChangeText={(text) => this.setState({ address: text })} />
              </Item>
              <View style ={{flexDirection: 'row'}}>
                <Item style={{width:(windowWidth-45)/4}} floatingLabel>
                  <Label style={{fontSize: 12,color: 'grey'}} >City</Label>
                  <Input onChangeText={(text) => this.setState({ city: text })} />
                </Item>
                <Item style={{width:(windowWidth-45)/4}} floatingLabel>
                  <Label style={{fontSize: 10,color: 'grey'}} >State/Province</Label>
                  <Input onChangeText={(text) => this.setState({ state: text })} />
                </Item>
                <Item style={{width:(windowWidth-45)/3}} floatingLabel>
                  <Label style={{fontSize: 12,color: 'grey'}} >Zip/Postal Code</Label>
                  <Input onChangeText={(text) => this.setState({ zip: text })} />
                </Item>
              </View>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{fontSize: 14,color: 'grey'}} >Mobile Number</Label>
                <Input onChangeText={(text) => this.setState({ phone: text })} />
              </Item>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{fontSize: 14,color: 'grey'}} >Contact Person</Label>
                <Input onChangeText={(text) => this.setState({ contact: text })} />
              </Item>
              <View style={{marginLeft:10,marginTop:15,flex:1}}>
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Services Provided</Text>
                  </View>
                  <View style={{marginTop:15,borderBottomWidth:1,borderBottomColor: 'lightgrey',width: windowWidth-30,height: 40}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={  <Icon active name='arrow-drop-down' color= 'orange' />}
                      placeholder="Category"
                      placeholderStyle={{color: 'grey',fontSize: 14}}
                      placeholderTextColor='grey'
                      selectedValue={this.state.category}
                      onValueChange={this.onCategoryChange.bind(this)}
                    >
                      {this.showcat()}
                    </Picker>
                  </View>
                  <View style={{marginTop:15,borderBottomWidth:1,borderBottomColor: 'lightgrey',width: windowWidth-30,height: 40}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={  <Icon active name='arrow-drop-down' color= 'orange' />}
                      placeholder="Service"
                      placeholderStyle={{color: 'grey',fontSize: 14}}
                      placeholderTextColor='grey'
                      selectedValue={this.state.service}
                      onValueChange={this.onServiceChange.bind(this)}
                    >
                      {this.showser()}
                      
                    </Picker>
                  </View>
              <View style={{marginLeft:10,marginTop:15,flex:1}}>
                <Text style={{fontSize:12,fontWeight:'bold'}}>Offer on Site Services?</Text>
              </View>
              <View style={{flex:1,width:windowWidth-10,flexDirection: 'row'}}>
                <View style={{flex:0.5}}>
                  <CheckBox
                    checked={this.state.isNoChecked}
                    checkedColor='orange'
                    uncheckedColor='orange'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    title="No"
                    textStyle={{fontSize:18,color: 'grey'}}
                    onPress={() => this.setState({isNoChecked: !this.state.isNoChecked,isYChecked: false})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
                <View style={{flex:0.5}}>
                  <CheckBox
                    checked={this.state.isYChecked}
                    checkedColor='orange'
                    uncheckedColor='orange'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    title="Yes"
                    textStyle={{fontSize:18,color: 'grey'}}
                    onPress={() => this.setState({isYChecked: !this.state.isYChecked,isNoChecked: false})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
              </View>
            </Form>
          </Content>
        </View>
        <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center'}}>
          { this.state.isLoading ?
            <View style={{justifyContent: 'center',alignItems: 'center'}}>
              <ActivityIndicator />
            </View> 
            :
            <TouchableOpacity style={styles.login_btn} onPress={this.provider_signup}>
              <Text style={{fontWeight: 'bold', color: 'white',fontSize: 18}}>Next</Text>
            </TouchableOpacity>
          }
          <View style={{flex:0.5,height:20}}>
          </View>
        </View>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    flex:1,
    justifyContent: 'flex-start', 
    alignItems: 'flex-end',
    flexDirection : 'row'
  },
  heading: {
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputField: {
    height: 50,
    width: windowWidth-80,
    marginTop: 10,
    backgroundColor: 'lightgrey'
  },
  login_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  other_btn:{
    flex:1,
    width: 40,
    backgroundColor: 'orange',
    height:50,
    flexDirection:'row'
  },
  signup_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'navy',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10,
    height: 50
  },
  icon:{
    height:50,
    width: 50
  }
});