import React from 'react';
import { AsyncStorage,Switch,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import { ImagePicker,Permissions } from 'expo';
// import { Switch } from 'react-native-switch';
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class ProviderCheckout extends React.Component {

  constructor(props){
    super(props);
    this.state={
      card: '',
      cvc: '',
      expiry: '',
      selected_type: 0,
      total_amount: 0,
      detail:'',
      public_key: 'pk_test_LzNu9iJt1zxQRxxc20tGnPTo',
      email: '',
      whatsapp: '',
      weblink: '',
      password: '',
      show_error: false,
      error_message: '',
      status: false,
      modalVisible: false,
      modalSocialVisible: false,
      modalScheduleVisible: false,
      modal_data: 'free',
      isloading: false,
      twitter_url: '',
      facebook_url: '',
      in_url: '',
      youtube_url: '',
      insta_url: '',
      images: [],
      base64_str: null,
      local_image_url: null,
      ispremiumLoading: false,
      isCashChecked:false,
      isAmexChecked:false,
      isMasterChecked:false,
      isPaypalChecked:false,
      isVisaChecked:false,
      isDiscoverChecked:false,
      payment_selected: null,
      available_array:  
      [
        {id: 1,availability :false,day: "Mon",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 2,availability :false,day: "Tue",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 3,availability :false,day: "Wed",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 4,availability :false,day: "Thu",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 5,availability :false,day: "Fri",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 6,availability :false,day: "Sat",start_time : '10:10 AM',end_time: '10:10 PM'},
        {id: 7,availability :false,day: "Sun",start_time : '10:10 AM',end_time: '10:10 PM'}
      ],
      schedule_time:[
        {id: 1,name:"09:00 AM"},
        {id: 2,name:"09:30 AM"},
        {id: 3,name:"10:00 AM"},
        {id: 4,name:"10:30 AM"},
        {id: 5,name:"11:00 AM"},
        {id: 6,name:"11:30 AM"},
        {id: 7,name:"12:00 PM"},
        {id: 8,name:"12:30 PM"},
        {id: 9,name:"01:00 PM"},
        {id: 10,name:"01:30 PM"},
        {id: 11,name:"02:00 PM"},
        {id: 12,name:"02:30 PM"},
        {id: 13,name:"03:00 PM"},
        {id: 14,name:"03:30 PM"},
        {id: 15,name:"04:00 PM"},
        {id: 16,name:"04:30 PM"},
        {id: 17,name:"05:00 PM"},
        {id: 18,name:"05:30 PM"},
        {id: 19,name:"06:00 PM"},
        {id: 20,name:"06:30 PM"},
        {id: 21,name:"07:00 PM"},
        {id: 22,name:"07:30 PM"},
        {id: 23,name:"08:00 PM"},
        {id: 24,name:"08:30 PM"},
        {id: 25,name:"09:00 PM"},
        {id: 26,name:"09:30 PM"}
      ],
    }
    this.set_token()
  }

   _pickImage = async () => {
    console.log("P")
    var imageArr = this.state.images;
    const { status: cameraRollPerm } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if (cameraRollPerm === 'granted') {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
        base64: true
      });
      if (!result.cancelled) {
        await imageArr.push(result)
        await this.setState({ images: imageArr });
        console.log("**********************************")
        console.log(this.state.images)
        console.log("**********************************")
      }
    }
  }

  set_token = async() =>{
    var tokken = await AsyncStorage.getItem('token') 
    await this.setState({token: tokken})
  }

  componentDidMount(){
  }

  toggleSwitch = (value, key) => {
      // //onValueChange of the switch this function will be called
      console.log("********************************************")
      console.log(key)
      this.state.available_array[key].availability = !this.state.available_array[key].availability
      // item.availability = !item.availability
      this.setState({})
      // //state changes according to switch
      // //which will result in re-render the text
  }

  cc_format_date = (value) => {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []
    for (i=0, len=match.length; i<len; i+=2) {
        parts.push(match.substring(i, i+2))
    }
    if (parts.length) {
      var vv =  parts.join('/')
      console.log(vv)
      this.setState({ expiry: vv })

      return vv
    } else {
      var vv = value
      console.log(vv)
      this.setState({ expiry: vv })
      return vv
    }
    console.log(vv)
  }

  cc_format = (value) => {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []
    for (i=0, len=match.length; i<len; i+=4) {
        parts.push(match.substring(i, i+4))
    }
    if (parts.length) {
        var vv =  parts.join(' ')
        console.log(vv)
        this.setState({ card: vv })

        return vv
    } else {
        var vv = value
        console.log(vv)
        this.setState({ card: vv })
        return vv
    }
    console.log(vv)
  }

  facebook_social = () =>{
    if(this.state.facebook_url == ''){
      return(
        <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'facebook',modalText: 'Facebook Url',modalSocialVisible: true})}>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Icon
              type="font-awesome"
              name='facebook'
              iconStyle={{color:'white'}}
            />
          </View>
          <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
            <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Facebook</Text>
          </View>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Image
              style={{
                height :25,
                width: 25
              }}
              source={require('../../assets/cross.png')}
            />
          </View>
        </TouchableOpacity>
      )
    }else{
      return(
        <View style={styles.otherdis_btn}>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Icon
              type="font-awesome"
              name='facebook'
            />
          </View>
          <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
            <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Facebook</Text>
          </View>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Image
              style={{
                height :25,
                width: 25
              }}
              source={require('../../assets/cross.png')}
            />
          </View>
        </View>
      )
    }
  }

  twitter_social = () =>{
      if(this.state.twitter_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'twitter',modalText: 'Twitter Url',modalSocialVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='twitter'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Twitter</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='twitter'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Twitter</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    insta_social = () =>{
      if(this.state.insta_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'insta',modalText: 'Instagram Url',modalSocialVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='instagram'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Instagram</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn} >
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='instagram'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Instagram</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    in_social = () =>{
      if(this.state.in_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'in',modalText: 'Linkedin Url',modalSocialVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='linkedin'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Linkedin</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='linkedin'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Linkedin</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    youtube_social = () =>{
      if(this.state.youtube_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'youtube',modalText: 'Youtube Url',modalSocialVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='youtube'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Youtube</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn} >
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='youtube'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:10,fontWeight:'bold'}}>Youtube</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }


  payment_submit = () => {
    if(this.state.card == '' || this.state.expiry == '' || this.state.cvc == ''){
      this.setState({show_error: true, error_message: "All Fields are Required"})
    }else{
      this.setState({isloading: true })
      var cardNum = this.state.card
      cardNum=cardNum.replace(/ +/g, "");
      cardDates = this.state.expiry
      cardDates = cardDates.split('/')
      var month = cardDates[0]
      var year = cardDates[1]
      console.log(month)
      console.log(year)
      var stripe_url = 'https://api.stripe.com/v1/tokens?card[number]='+cardNum+'&card[exp_month]='+month+'&card[exp_year]='+year+'&card[cvc]='+this.state.cvc;
      this.stripePayment(stripe_url)
    }
  }
  
  stripePayment = async (url) => {
    console.log(url)
    let response = await fetch(url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer "+this.state.public_key
      }
    });
    let responseJson = await response.json();
    if (responseJson.error){
      this.setState({isloading: false, show_error: true, error_message: responseJson.error.message})
    }else{
      this.charge_request(responseJson.id)
    }
  }

  charge_request = (id) => {
    console.log("cahrge request")
    try{
      fetch(BasePath+'payment', {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            'Token': this.state.token
        },
        body: JSON.stringify({
          token: id,
          premium: this.state.selected_type,
          amount: this.state.total_amount
        })

      })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status){
          console.log(responseJson)
          this.setState({isloading: false})
          this.props.navigation.push("Home")
        }else{
          this.setState({isloading: false})
          Alert.alert(
            'Error',
            response.result.message,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
        }
        // this.storedate(responseJson.result)
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }



  show_error_message = () =>{
    if(this.state.show_error){
      if(this.state.status){
        return(
          <Text style={{margin:10,color: 'green',textAlign:'center'}}>{this.state.error_message}</Text>
        )
      }else{
        return(
          <Text style={{margin:10,textAlign:'center',color: 'red'}}>{this.state.error_message}</Text>
        )
      }
    }
  }

  reset_password = () =>{
    try{
      fetch(BasePath+'api/user/reset', {
        method: 'POST',
        mode: 'same-origin',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: JSON.stringify({
          username: this.state.email,
        })
      })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({error_message: responseJson.result.message, status: responseJson.status, show_error: true})
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  form_validation = () => {
    if(this.state.email === ''){
      this.setState({error_message: 'All Fields are required', show_error: true})
    }else{
      this.reset_password()
    }
  }

  show_schedule = () =>{
      if (this.state.available_array){
        return(
          this.state.available_array.map((item, key) =>{
            return(
              <View key={item.id} style={{height: 60,width: windowWidth-80,flex:2,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Text>{item.day}</Text>
                  <Switch
                    onValueChange = {(value) => this.toggleSwitch(value,key)}
                    value = {item.availability}/>
                </View>
                <View style={{flex:0.2}}></View>
                <View style={{flexDirection:'row',borderBottomWidth:1,borderColor: 'lightgrey',flex:1,justifyContent:'center',alignItems:'center'}}>
                  <View style={{flex:0.7}}>
                    <Text>{item.start_time}</Text>
                  </View>
                  <View style={{flex:0.3}}>
                    <TouchableOpacity onPress={() => this.setState({selected_date: "start",selected_schedule: key, modalScheduleVisible: true})}>
                      <Icon
                        name='unfold-more'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'red' />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{flex:0.2}}></View>
                <View style={{flexDirection:'row',borderBottomWidth:1,borderColor: 'lightgrey',flex:1,justifyContent:'center',alignItems:'center'}}>
                  <View style={{flex:0.7}}>
                    <Text>{item.end_time}</Text>
                  </View>
                  <View style={{flex:0.3}}>
                   <TouchableOpacity onPress={() => this.setState({selected_date: "end",selected_schedule: key, modalScheduleVisible: true})}>
                      <Icon
                        name='unfold-more'
                        containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                        color= 'red' />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            )
          })
        );
      }else{
        return null;
      }
  } 

  show_uploaded = () =>{
    return(
      this.state.images.map((item, key) =>{
        console.log("Image Uploader")
        return(
          <View key={key} style={{marginRight: 7,marginLeft: 7,flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
            <Image
              style={{
                height :100,
                width: 100
              }}
               source={{ uri: item.uri }}
            />
          </View>
        )
      })
    )
  }

  show_modal_data = () =>{
    if (this.state.modal_data === 'free'){
      return(
        <View style={styles.modal_container}>
          <View style={styles.modal}>
            <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
                <Icon
                  name='highlight-off'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
              <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:18,color:'orange'}}>Free Listing</Text>
              </View>  
              <View style={{flex:3.5,justifyContent:'flex-start',alignItems:'center'}}>
                <Text style={{fontSize:10,color:'grey'}}>Providers recieve 1 free  listing per business.There</Text>
                <Text style={{fontSize:10,color:'grey'}}>is no fee for the basic listing which has </Text>
                <Text style={{fontSize:10,color:'grey'}}>limited information</Text>
                <Image
                  style={{
                    height :50,
                    width: 50,
                    marginTop:10
                  }}
                  source={require('../../assets/rocket.png')}
                />
                <Text style={{marginTop: 10,fontSize:10,color:'orange'}}>Want increased sales?</Text>
                <TouchableOpacity style={{marginTop:15,justifyContent:'center',alignItems:'center',backgroundColor: 'orange',height:40,width: windowWidth-130}}>
                  <Text style={{fontSize:10,color:'white',fontWeight:'bold'}}>Upgrade To Premium Listing</Text>
                  <Text style={{fontSize:10,color:'white'}}>for onlt 33/Days ($9.99/month)</Text>
                </TouchableOpacity>
              </View>  
            </View>  
          </View>
        </View>
      );
    }else if (this.state.modal_data === 'helping_hand'){
      return(
        <View style={styles.modal_container}>
          <View style={styles.modal}>
            <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
                <Icon
                  name='highlight-off'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
              <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
                <Image
                  style={{
                    height :60,
                    width: 60
                  }}
                  source={require('../../assets/helping.png')}
                />
              </View>
              <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:18,color:'orange'}}>Helping Hand</Text>
              </View>  
              <View style={{flex:2,justifyContent:'flex-start',alignItems:'center'}}>
                <Text style={{fontSize:10,color:'grey'}}>The listing is for namads who help other nomads by</Text>
                <Text style={{fontSize:10,color:'grey'}}>providing helping hand for free , or for small</Text>
                <Text style={{fontSize:10,color:'grey'}}>donation,if provider charges a nything beyond a</Text>
                <Text style={{fontSize:10,color:'grey'}}>nominal fess for his/her services,this.account will</Text>
                <Text style={{fontSize:10,color:'grey'}}>be terminated.if there is a fee for the services.</Text>
                <Text style={{fontSize:10,color:'grey'}}>Register for free listing</Text>
              </View>  
            </View>
          </View>
        </View>
      );

    }else if (this.state.modal_data === 'monthly'){
      return(
        <View style={styles.modal_container}>
          <View style={styles.modal}>
            <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
                <Icon
                  name='highlight-off'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image
                  style={{
                    height :60,
                    width: 60
                  }}
                  source={require('../../assets/calendar.png')}
                />
              </View>
              <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:18,color:'orange'}}>Monthly Premium Listing</Text>
              </View>  
              <View style={{flex:2.5,justifyContent:'flex-start',alignItems:'center'}}>
                <Text style={{fontSize:10,color:'grey'}}>Access to the services for 30 days from payment</Text>
                <Text style={{fontSize:10,color:'grey'}}>date of initial monthly payment of $ 0.99.Next</Text>
                <Text style={{fontSize:10,color:'grey'}}>month's service will automatically renew at</Text>
                <Text style={{fontSize:10,color:'grey'}}>$9.99/month</Text>
                <Text style={{fontSize:14,color:'orange'}}>Enjoy two month free by</Text>
                <Text style={{fontSize:14,color:'orange'}}>subscribing to annual service.</Text>
              </View>
                
            </View>  
          </View>
        </View>
      );
    }else if (this.state.modal_data === 'annual'){
      return(
        <View style={styles.modal_container}>
          <View style={styles.modal}>
            <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
                <Icon
                  name='highlight-off'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
            </View>
            <View style={{flex:4}}>
              <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
                <Image
                  style={{
                    height :60,
                    width: 60
                  }}
                  source={require('../../assets/date.png')}
                />
              </View>
              <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:18,color:'orange'}}>Annual Premium Listing</Text>
              </View>  
              <View style={{flex:2,justifyContent:'flex-start',alignItems:'center'}}>
                <Text style={{fontSize:10,color:'grey'}}>An annual charge of $100.00 for the premium listing;</Text>
                <Text style={{fontSize:10,color:'grey'}}>Access provided for 365 days from payment day</Text>
                <Text style={{fontSize:10,color:'grey'}}>$100.00/year will be automatically charged unless </Text>
                <Text style={{fontSize:10,color:'grey'}}>cencelled at least 30 days prior to renewal.</Text>
              </View>  
            </View>  
          </View>
        </View>
      );
    }else if (this.state.modal_data === 'premium_listing'){
      return(
        <View style={styles.modal_container}>
          <Modal
            animationType="none"
            transparent={true}
            visible={this.state.modalSocialVisible}
            onRequestClose={() => 
              this.setState({modalSocialVisible:false})
            }
          >
            {this.show_social_modal_data()}
          </Modal>
          <Modal
            animationType="none"
            transparent={true}
            visible={this.state.modalScheduleVisible}
            onRequestClose={() => 
              this.setState({modalScheduleVisible: false})
            }
          >
            {this.show_schedule_modal_data()}
          </Modal>
          <View style={styles.modal_premium}>
            <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
              <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
                <Icon
                  name='highlight-off'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </TouchableOpacity>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
              <View><Text style={{fontWeight:'bold',fontSize: 14,color:'orange'}}>Premium Upgrade Information</Text></View>
              <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
            </View>
            <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
              <Text style={{fontSize: 14}}>Complete the following Infromation</Text>
              <Text style={{fontSize: 14}}>for an Upgraded Profile</Text>
            </View>
            <View style={{flex:10}}>
              <ScrollView>
                <View style={{flex:5,justifyContent: 'center', alignItems: 'center'}}>
                  <Content>
                    {this.show_error_message()}
                    <Form>
                      <Item style={{width:windowWidth-70}} floatingLabel>
                        <Label style={{fontSize: 14,color: 'black'}} >Email</Label>
                        <Input onChangeText={(text) => this.setState({ email: text })} />
                      </Item>
                      <Item style={{width:windowWidth-70}} floatingLabel>
                        <Label style={{fontSize: 14,color: 'black'}} >What's app</Label>
                        <Input onChangeText={(text) => this.setState({ whatsapp: text })} />
                      </Item>
                      <Item style={{width:windowWidth-70}} floatingLabel>
                        <Label style={{fontSize: 14,color: 'black'}} >Website Link</Label>
                        <Input onChangeText={(text) => this.setState({ website: text })} />
                      </Item>
                    </Form>
                  </Content>
                </View>
                <View style={{marginLeft:10,marginTop:15,flex:1}}>
                  <Text style={{fontSize:12,fontWeight:'bold'}}>Upload additional photos (up to 5)</Text>
                </View>
                <View style={styles.containerWrap}>
                  <ScrollView horizontal={true}>
                    <View style={{marginLeft:10,flex:1,justifyContent:'center',alignItems:'center',borderColor:'lightgreen',borderWidth:1}}>
                      <TouchableOpacity style={{justifyContent:'center',alignItems:'center'}} onPress={this._pickImage}>
                        <Icon active name='add-circle' iconStyle={{color:'orange'}}/>
                        <Text>  Upload Image  </Text>
                      </TouchableOpacity>
                    </View>
                    {this.show_uploaded()}
                  </ScrollView>
                </View>
                <View style={{marginLeft:10,marginTop:15,flex:1}}>
                  <Text style={{fontSize:12,fontWeight:'bold'}}>Share Media Links</Text>
                </View>
                <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                  {this.facebook_social()}
                  <View style={{width: 5}}></View>
                  {this.twitter_social()}
                </View>
                <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                  {this.insta_social()}
                  <View style={{width: 5}}></View>
                  {this.in_social()}
                </View>
                <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                  {this.youtube_social()}
                  <View style={{flex:1,width:40}}>
                  </View>
                </View> 
              
                <View style={{margin:15,flex:1}}>
                  <Item style={{width:windowWidth-70}} floatingLabel >
                    <Label style={{fontSize: 14,color: 'grey'}} >Brief Narrative about Services Provided</Label>
                    <Input onChangeText={(text) => this.setState({ detail: text })} />
                  </Item>
                </View>
                <View style={{marginTop:15,flex:1}}>
                  {this.show_schedule()}
                </View>
                <View style={{marginLeft:10,marginTop:15,flex:1}}>
                  <Text style={{fontSize:12,fontWeight:'bold'}}>Payment Accepted</Text>
                </View>
                <View style={{marginTop:15,flex:2,flexDirection: 'row'}}>
                  <View style={{flex:1,width: windowWidth,flexDirection:"row"}}>
                    <View style={{flex:0.5}}>                   
                      <CheckBox
                        checked={this.state.isCashChecked}
                        checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                        uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                        onPress={() => this.setState({isCashChecked: !this.state.isCashChecked})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                    <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-start'}}>
                    <Text style={styles.payment_accepted_text}>Cash</Text>
                    </View>
                  </View>
                  <View style={{flex:1,width: windowWidth,flexDirection:'row'}}>
                    <View style={{flex:0.5}}>
                      <CheckBox
                        checked={this.state.isMasterChecked}
                        checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                        uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                        onPress={() => this.setState({isMasterChecked: !this.state.isMasterChecked})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                    <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-start'}}>
                      <Text style={styles.payment_accepted_text}>Master Card</Text>
                    </View>
                  </View>
                </View>
                <View style={{marginTop:15,flex:2,flexDirection: 'row'}}>
                  <View style={{flex:1, width: windowWidth,flexDirection:'row'}}>
                    <View style={{flex:0.5}}>
                      <CheckBox
                        checked={this.state.isAmexChecked}
                        checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                        uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                        onPress={() => this.setState({isAmexChecked: !this.state.isAmexChecked})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                    <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-start'}}>
                      <Text style={styles.payment_accepted_text}>Amex</Text>
                    </View>
                  </View>
                  <View style={{flex:1,width: windowWidth,flexDirection:'row'}}>
                    <View style={{flex:0.5}}>
                      <CheckBox
                        checked={this.state.isVisaChecked}
                        checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                        uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                        onPress={() => this.setState({isVisaChecked: !this.state.isVisaChecked})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                    <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-start'}}>
                      <Text style={styles.payment_accepted_text}>Visa</Text>
                    </View>
                  </View>
                </View>
                <View style={{marginTop:15,flex:2,flexDirection: 'row'}}>
                  <View style={{flex:1,flexDirection:'row',width: windowWidth}}>
                    <View style={{flex:0.5}}>
                      <CheckBox
                        checked={this.state.isDiscoverChecked}
                        checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                        uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                        onPress={() => this.setState({isDiscoverChecked: !this.state.isDiscoverChecked})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                    <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-start'}}>
                      <Text style={styles.payment_accepted_text}>Discover</Text>
                    </View>
                  </View>
                  <View style={{flex:1,flexDirection:'row',width:windowWidth}}>
                    <View style={{flex:0.5}}>
                      <CheckBox
                        checked={this.state.isPaypalChecked}
                        checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                        uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                        onPress={() => this.setState({isPaypalChecked: !this.state.isPaypalChecked})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                    <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-start'}}>
                      <Text style={styles.payment_accepted_text}>Paypal</Text>
                    </View>
                  </View>
                </View>
                <View style={{marginTop:15,flex:2,justifyContent:'center',alignItems:'center'}}>
                  { this.state.ispremiumLoading ?
                    <View style={{justifyContent: 'center',alignItems: 'center'}}>
                      <ActivityIndicator />
                    </View> 
                  :
                    <TouchableOpacity style={{justifyContent: 'center',alignItems:'center',marginTop:20,height: 50,backgroundColor: 'orange',width: windowWidth-70}} onPress={this.premium_save}>
                      <Text style={{color: 'white',fontSize: 18,fontWeight: 'bold'}}>Save</Text>
                    </TouchableOpacity>
                  }    
                </View>
                <View style={{marginTop:15,height: 30}}>
                </View>

              </ScrollView>
            </View>
          </View>
        </View>
      );
    }
  }

  premium_save = async() =>{
    this.setState({ispremiumLoading: true})
    await this.set_payment_selected()
    this.save_premium_request()
  }

  set_payment_selected = async() => {
    var payment_array = {
      visa: this.state.isVisaChecked,
      cash: this.state.isCashChecked,
      discover: this.state.isDiscoverChecked,
      amex: this.state.isAmexChecked,
      master: this.state.isMasterChecked,
      paypal: this.state.isPaypalChecked
    }
    await this.setState({payment_selected: payment_array})
  }

  save_premium_request = () =>{
    console.log("arrow-back request")
    try{
      fetch(BasePath+'addservices', {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            'Token': this.state.token
        },
        body: JSON.stringify({
          website: this.state.website,
          email: this.state.email,
          whatsapp: this.state.whatsapp,
          hour: this.state.available_array,
          facebook: this.state.facebook_url,
          linkedin: this.state.in_url,
          twitter: this.state.twitter_url,
          youtube: this.state.youtube_url,
          insta: this.state.insta_url,
          detail: this.state.detail,
          image: this.state.images,
          accept: this.state.payment_selected
        })

      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          console.log(responseJson)
          this.setState({ispremiumLoading: false})
          // this.props.navigation.push("Home")
        }else{
          this.setState({ispremiumLoading: false})
          Alert.alert(
            'Error',
            response.result.message,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
        }
        // this.storedate(responseJson.result)
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  show_url_input = () =>{
    if (this.state.selected_social === 'facebook'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ facebook_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'twitter'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ twitter_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'insta'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ insta_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'in'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ in_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'youtube'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ youtube_url: text })} />
        </Item>
      )
    }
  }

  pick_schedule = (item) =>{
    if(this.state.selected_date == 'start'){
      this.state.available_array[this.state.selected_schedule].start_time = item.name
      this.setState({modalScheduleVisible: false})
    }else{
      this.state.available_array[this.state.selected_schedule].end_time = item.name
      this.setState({modalScheduleVisible: false})
    }
  }

  show_schedule_data = () =>{
    return this.state.schedule_time.map((item, key) =>{
      return(
        <View key={item.id} style={{height: 50,justifyContent:'center',alignItems:'center'}}>
          <TouchableOpacity onPress = {() => this.pick_schedule(item)}>  
            <Text style={{fontSize:15, fontWeight:'bold'}}>{item.name}</Text>
          </TouchableOpacity>       
        </View>
      )
    })
  }

  show_social_modal_data = () =>{
    return(
      <View style={styles.modal_container}>
        <View style={styles.modal}>
          <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
            <TouchableOpacity onPress={() => this.setState({modalSocialVisible: false})}>
              <Icon
                name='highlight-off'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
          </View>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontWeight:'bold',fontSize:18}}>{this.state.modalText}</Text>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <Form>
              {this.show_url_input()}
            </Form>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
          </View>
        </View>
      </View>
    );
  }

  show_schedule_modal_data = () =>{
    return(
      <View style={styles.modal_container}>
        <View style={styles.modalSchedule}>
          <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
            <TouchableOpacity onPress={() => this.setState({modalScheduleVisible: false})}>
              <Icon
                name='highlight-off'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
          </View>
          <View style={{flex:3.5,justifyContent:'center',alignItems:'center'}}>
            <ScrollView style={{width: windowWidth}}>
              {this.show_schedule_data()}
            </ScrollView>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
          </View>
        </View>
      </View>
    );
  }


  login = () =>{
    this.form_validation()
  }

  forgot_password = () =>{
    console.log("start Contest")
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => 
            this.setState({modalVisible:false})
          }
        >
          {this.show_modal_data()}
          
        </Modal>

        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:1.15,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :40,
              width: 40
            }}
            source={require('../../assets/cart.png')}
          />
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-70}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('ProviderProfile')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6,justifyContent:'flex-start',alignItems:'center'}}>
            <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
            <View><Text style={{fontSize: 14}}>Provider Checkout</Text></View>
            <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
          </View>
          <View style={{flex:0.2}}></View>
        </View>
        <View style={{flex:9.5,backgroundColor:'#EEEEEE',width: windowWidth}}>
          <ScrollView>
            <View style={{flex:2,flexDirection: 'row',justifyContent: 'center',alignItems: 'center'}}>
              <View style={{flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
                <Image
                  style={{
                    height :150,
                    width: 150
                  }}
                  source={require('../../assets/annual.png')}
                />
                <View style={{width: (windowWidth-60)/2,position: 'absolute',justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:16,color:'orange'}}>$<Text style={{fontSize:12,color:'orange'}}>30.00</Text></Text>
                  <Text style={{fontSize:9}}>Recommended</Text>
                </View>
              </View>
              <View style={{flex:0.2}}></View>
              <View style={{flex:0.8,justifyContent: 'center',alignItems: 'center'}}> 
                <Image
                  style={{
                    height :150,
                    width: 150
                  }}
                  source={require('../../assets/monthly.png')}
                />
                <View style={{width: (windowWidth-60)/2,position: 'absolute',justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:16,color:'orange'}}>$<Text style={{fontSize:12,color:'orange'}}>2.99</Text></Text>
                  <Text style={{fontSize:8,textAlign:'center'}}>No CC Requered Free</Text>
                  <Text style={{fontSize:8,textAlign:'center'}}>7 Day Trial Subscription</Text>
                </View>
              </View>
            </View>
            <View style={{flex:10,margin: 15,backgroundColor: 'white'}}>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:0.20}}>
                  <CheckBox
                    checked={this.state.isFChecked}
                    checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                    uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                    onPress={() => this.setState({isFChecked: !this.state.isFChecked,selected_type:0,total_amount:0,isHChecked:false,isMChecked:false,isAChecked: false})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
                <View style={{flexDirection:'row',flex:0.80}}>
                  <View style={{flexDirection:'row',flex:0.65,alignItems:'center',justifyContent: 'flex-start'}}>
                    <Text style={{fontSize:14,fontWeight:'bold'}}>Free Listing</Text>
                    <TouchableOpacity onPress={() => this.setState({modal_data: 'free' ,modalVisible: true})}>
                      <Icon name="info" size={20}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:0.05}}></View>
                  <View style={{flex:0.20,alignItems:'flex-end',justifyContent: 'center'}}>
                    <Text style={{color: 'orange',fontWeight: 'bold', fontSize:12}}>$0</Text>
                  </View>
                </View>
              </View>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:0.20}}>
                  <CheckBox
                    checked={this.state.isHChecked}
                    checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                    uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                    onPress={() => this.setState({isHChecked: !this.state.isHChecked,selected_type:3,total_amount:0,isFChecked:false,isMChecked:false,isAChecked: false})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
                <View style={{flexDirection:'row',flex:0.80}}>
                  <View style={{flexDirection:'row',flex:0.65,alignItems:'center',justifyContent: 'flex-start'}}>
                    <Text style={{fontSize:14,fontWeight:'bold'}}>Helping Hand</Text>
                    <TouchableOpacity onPress={() => this.setState({modal_data: 'helping_hand' ,modalVisible: true})}>
                      <Icon name="info" size={20}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:0.05}}></View>
                  <View style={{flex:0.20,alignItems:'flex-end',justifyContent: 'center'}}>
                    <Text style={{color: 'orange',fontWeight: 'bold', fontSize:12}}>$0</Text>
                  </View>
                </View>
              </View>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:0.20}}>
                  <CheckBox
                    checked={this.state.isMChecked}
                    checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}}/>}
                    uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}}/>}
                    onPress={() => this.setState({isMChecked: !this.state.isMChecked,selected_type:1,total_amount:9.99,isHChecked:false,isFChecked:false,isAChecked: false})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
                <View style={{flexDirection:'row',flex:0.80}}>
                  <View style={{flexDirection:'row',flex:0.65,alignItems:'center',justifyContent: 'flex-start'}}>
                    <Text style={{fontSize:14,fontWeight:'bold'}}>Monthly Premium Listing</Text>
                    <TouchableOpacity onPress={() => this.setState({modal_data: 'monthly' ,modalVisible: true})}>
                      <Icon name="info" size={20}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:0.05}}></View>
                  <View style={{flex:0.20,alignItems:'flex-end',justifyContent: 'center'}}>
                    <Text style={{color: 'orange',fontWeight: 'bold', fontSize:12}}>$9.99</Text>
                  </View>
                </View>
              </View>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:0.20}}>
                  <CheckBox
                    checked={this.state.isAChecked}
                    checkedIcon={<Icon name="radio-button-checked" color='orange' iconStyle={{height:25,width:25}} />}
                    uncheckedIcon={<Icon name="radio-button-unchecked" color='orange' iconStyle={{height:25,width:25}} />}
                    onPress={() => this.setState({isAChecked: !this.state.isAChecked,selected_type:2,total_amount:100,isHChecked:false,isMChecked:false,isFChecked: false})}
                    containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                  />
                </View>
                <View style={{flexDirection:'row',flex:0.80}}>
                  <View style={{flexDirection:'row',flex:0.65,alignItems:'center',justifyContent: 'flex-start'}}>
                    <Text style={{fontSize:14,fontWeight:'bold'}}>Annual Premium Listing</Text>
                    <TouchableOpacity onPress={() => this.setState({modal_data: 'annual' ,modalVisible: true})}>
                      <Icon name="info" size={20} />
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:0.05}}></View>
                  <View style={{flex:0.20,alignItems:'flex-end',justifyContent: 'center'}}>
                    <Text style={{color: 'orange',fontWeight: 'bold', fontSize:12}}>$100</Text>
                  </View>
                </View>
              </View>
              <View style={{marginTop: 15,alignItems:'center',justifyContent:'center'}}>
                <Text style={{fontSize: 10,fontWeight: 'bold'}}>IF FREE LISTING AND HELPING HAND SELECTED,</Text>
                <Text style={{fontSize: 10,fontWeight: 'bold'}}>GO DIRECTLY TO CHECKOUT</Text>
              </View>
              <View style={{marginTop: 15,flex:1,alignItems:'center',justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => this.setState({modal_data: 'premium_listing',modalVisible: true})}>
                  <Text style={{color:'orange',fontWeight: 'bold'}}>Upgrade to Premium Listing</Text>
                </TouchableOpacity>
              </View>
              <View style={{marginTop: 15}}></View>
            </View>

            <View style={{height:500,flex:6,margin:15,backgroundColor: 'white'}}>
              <Text style={{fontWeight: 'bold',margin:10}}>Payment</Text>
              {this.show_error_message()}
              <Form >
                <Item style={{width:windowWidth-45,marginTop:20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >Card Holder Name</Label>
                  <Input
                    value={this.state.card_holder}
                    onChangeText={(text) => this.setState({card_holder: text})} 
                  />
                </Item>
                <Item style={{width:windowWidth-45,marginTop:20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >Card Number</Label>
                  <Input
                    value={this.state.card}
                    keyboardType = 'numeric'
                    onChangeText={(text) => this.cc_format(text)} 
                  />
                </Item>
                <Item style={{width:windowWidth-45,marginTop: 20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >CVC</Label>
                  <Input  keyboardType = 'numeric'  maxLength={4} onChangeText={(text) => this.setState({ cvc: text })} />
                </Item>
                <Item style={{width:windowWidth-45,marginTop: 20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >Expiration MM/YY</Label>
                  <Input value={this.state.expiry} maxLength={5} keyboardType = 'numeric' onChangeText={(text) =>  this.cc_format_date(text)} />
                </Item>
                <Item style={{width:windowWidth-45,marginTop: 20}} floatingLabel>
                  <Label style={{fontSize: 14,color: 'grey'}} >Billing Zip/Postal Code</Label>
                  <Input value={this.state.postal} maxLength={5} keyboardType = 'numeric' onChangeText={(text) =>  this.setState({postal: text})} />
                </Item>
              </Form>
              <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
              </View>
            </View>

            <View style={{flex:6,margin:10,backgroundColor: 'white'}}>
              <View style={{flex:1,marginTop:5}}>
                <Text style={{fontWeight:'bold',marginLeft: 15}}>Promo Code</Text>
              </View>
              <View style={{flex:1,marginTop:5}}>
                <Text style={{fontWeight:'bold',fontSize: 12,color:'#EEEEEE',marginLeft: 15}}>Promo Code</Text>
              </View>
              <View style={{flexDirection:'row',flex:1,height:40,margin:10,borderBottomWidth: 1,borderColor: '#EEEEEE'}}>
                <View style={{flex:0.5,alignItems: 'flex-start',justifyContent:'center'}}>
                  <Text style={{fontWeight:'bold',marginLeft: 15}}>Mobilegigs</Text>
                </View>
                <View style={{flex:0.5,alignItems: 'flex-end',justifyContent:'center'}}>
                  <Text style={{color:'orange',fontWeight:'bold',marginLeft: 15}}>Apply</Text>
                </View>
              </View>
              <View style={{flexDirection:'row',flex:1,height:20,margin:10}}>
                <View style={{flex:0.5,alignItems: 'flex-start',justifyContent:'center'}}>
                  <Text style={{color:'lightgrey',fontWeight:'bold',marginLeft: 15}}>Discount</Text>
                </View>
                <View style={{flex:0.5,alignItems: 'flex-end',justifyContent:'center'}}>
                  <Text style={{fontSize:12,color:'green',fontWeight:'bold',marginLeft: 15}}>$10.00</Text>
                </View>
              </View>
              <View style={{flexDirection:'row',flex:1,height:20,margin:10}}>
                <View style={{flex:0.5,alignItems: 'flex-start',justifyContent:'center'}}>
                  <Text style={{color:'lightgrey',fontWeight:'bold',marginLeft: 15}}>Total Amount</Text>
                </View>
                <View style={{flex:0.5,alignItems: 'flex-end',justifyContent:'center'}}>
                  <Text style={{color:'orange',fontWeight:'bold',marginLeft: 15}}>$20.00</Text>
                </View>
              </View>
            </View>
            <View style={{flex:1, alignItems: 'center',justifyContent: 'center'}}>
              { this.state.isloading ?
                <View style={{justifyContent: 'center',alignItems: 'center'}}>
                  <ActivityIndicator />
                </View> 
              :
                <TouchableOpacity style={{justifyContent: 'center',alignItems:'center',marginTop:20,height: 50,backgroundColor: 'orange',width: windowWidth-70}} onPress={this.payment_submit}>
                  <Text style={{color: 'white',fontSize: 18,fontWeight: 'bold'}}>Checkout</Text>
                </TouchableOpacity>
              }         
            </View>
            <View style={{flex:1, height: 20, alignItems: 'center',justifyContent: 'center'}}>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  payment_accepted_text:{
    fontSize: 13,
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  modalSchedule:{
    height:300,
    width: windowWidth-100 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'
  },
  modal_premium:{
    height:Dimensions.get('window').height-50,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  containerWrap: {
  flex: 1,
  height:120,
  marginTop:10,
  flexDirection: 'row',
  },
  other_btn:{
    flex:1,
    marginLeft:5,
    marginRight:5,
    width: 30,
    backgroundColor: 'orange',
    height:50,
    flexDirection:'row'
  },
  otherdis_btn:{
    flex:1,
    marginLeft:5,
    marginRight:5,
    width: 30,
    backgroundColor: 'lightgrey',
    height:50,
    flexDirection:'row'
  }
});
