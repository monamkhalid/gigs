import React from 'react';
import { ListView,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';
import { WP } from './responsive.js';

var windowWidth = Dimensions.get('window').width
export default class ProfileDetail extends React.Component {

  constructor(props){
    super(props);
    this.state={
      search: true,
      name: 'Saif Abou Goura',
      membership: 'Free',
      phone_number: '123456789',
    }
    this.populateList()
  }

  componentDidMount(){
  }

  populateList() {
    var dataSource=
    [ 
      {id:1,email: 'ahtasham@email.com',show_image:true,duration:'Now',name:'Bobs RV Repair',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:2,email: 'ahtasham1@email.com',show_image:false,duration:'1 days ago',name:'OHN RV Repair',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:3,email: 'ahtasham2@email.com',show_image:true,duration:'2 days ago',name:'Bobs RV Solar',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:4,email: 'ahtasham3@email.com',show_image:false,duration:'3 days ago',name:'Ohm RV Solar',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'}
    ]
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state.dataSource = ds.cloneWithRows(dataSource);
    this.state.db = dataSource
  };
  
  show_icon = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-end',alignItems:'center'}}>
          <Icon
            name='person'
            color='orange'
            containerStyle={{alignItems:'flex-start'}}
            size={40}
            />
        </View>
      )
    }else{
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
          <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
          <Icon
            name='highlight-off'
            size={30}
            containerStyle={{alignItems:'flex-end'}}
            color= 'grey' />
          </TouchableOpacity>
        </View>
      )
    }
  }

  show_text = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
          <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
          <View><Text style={{fontSize: 14}}>Profile Details</Text></View>
          <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
        </View>
      );
    }else{
      return null;
    }
  }
  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        
        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:0.8}}>
            {
              this.show_icon()
            }
          </View>
          <View style={{flexDirection:'row',flex:0.8,width:windowWidth,marginRight:15,justifyContent: 'center', alignItems: 'center'}}>
            <Text >Contact Us </Text>
            <Icon
            color = "orange"
              name='email'
              size={20}
              />
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6}}>
            {this.show_text()}
          </View>
          <View style={{flex:0.2,justifyContent: 'flex-start',alignItems: 'center'}}>
          </View>
        </View>
        <View style={{width: windowWidth,flex:3,backgroundColor:'#F0F0F0',alignItems:'center',justifyContent:'center'}}>
          <Image
            style={{
              height :100,
              width: 100,
              borderRadius :WP('100')
            }}
             source={{uri: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}}
          />
          <Icon
            name='add-circle'
            containerStyle={{position: 'relative',top:-30,left:32}}
            color= 'lightgreen' />
        </View>
        <View style={{flex:5,backgroundColor:'#F0F0F0'}}>
          <ScrollView style={{width: windowWidth}}>
            <Content>
              <Form>
                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:WP('4'),fontWeight:'bold'}}>Change Name</Text>
                    <Item style={{width:windowWidth-30}} floatingLabel>
                      <Label style={{color: 'grey',fontSize:WP('3')}} >Name</Label>
                      <Input
                        value ={this.state.name} 
                      />
                    </Item>
                  </View>
                  <View style={styles.redirectIconContainer}>
                    <TouchableOpacity>
                      <Icon
                        name='keyboard-arrow-down'
                        color='orange'
                        size={30}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.redirectContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:WP('4'),fontWeight:'bold'}}>Change Mobile Number</Text>
                    <Item style={{width:windowWidth-45}} floatingLabel>
                      <Label style={{color: 'grey',fontSize:WP('3')}} >Mobile Number</Label>
                      <Input
                        value ={this.state.phone_number} 
                      />
                    </Item>
                  </View>
                  <View style={styles.redirectIconContainer}>
                    <TouchableOpacity>
                    <Icon
                      name='keyboard-arrow-down'
                      color='orange'
                      size={30}
                    />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.redirectSocialContainer}>
                  <View style={styles.redirectTextContainer}>
                    <Text style={{fontSize:WP('4'),fontWeight:'bold'}}>Change Social Media Links</Text>    
                  </View>
                  <View style={styles.redirectIconContainer}>
                    <TouchableOpacity>
                    <Icon
                      name='keyboard-arrow-down'
                      color='orange'
                      size={30}
                    />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{backgroundColor:'white',borderColor:'grey',borderBottomWidth:1}}>
                <View style={{flex:1.5,margin: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                  <TouchableOpacity style={styles.other_btn} onPress={this.login}>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/fb.png')}
                      />
                    </View>
                    <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
                      <Text style={{ color: 'white',fontSize:11,fontWeight:'bold'}}>Facebook</Text>
                    </View>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/cross.png')}
                      />
                    </View>
                  </TouchableOpacity>
                  <View style={{width: 5}}></View>
                  <TouchableOpacity style={styles.other_btn} onPress={this.login}>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/ttr.png')}
                      />
                    </View>
                    <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
                      <Text style={{ color: 'white',fontSize:11,fontWeight:'bold'}}>Twitter</Text>
                    </View>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/cross.png')}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1.5,margin: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                  <TouchableOpacity style={styles.other_btn} onPress={this.login}>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/insta.png')}
                      />
                    </View>
                    <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
                      <Text style={{ color: 'white',fontSize:11,fontWeight:'bold'}}>Instagram</Text>
                    </View>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/cross.png')}
                      />
                    </View>
                  </TouchableOpacity>
                  <View style={{width: 5}}></View>
                  <TouchableOpacity style={styles.other_btn} onPress={this.login}>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/in.png')}
                      />
                    </View>
                    <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
                      <Text style={{ color: 'white',fontSize:11,fontWeight:'bold'}}>Linkedin</Text>
                    </View>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/cross.png')}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1.5,margin: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                  <TouchableOpacity style={styles.other_btn} >
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/youtube.png')}
                      />
                    </View>
                    <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
                      <Text style={{ color: 'white',fontSize:11,fontWeight:'bold'}}>Youtube</Text>
                    </View>
                    <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
                      <Image
                        style={{
                          height :25,
                          width: 25
                        }}
                        source={require('../../assets/cross.png')}
                      />
                    </View>
                  </TouchableOpacity>
                  <View style={{flex:1,width:40}}>
                  </View>
                </View>
                </View>
              </Form>
            </Content>
          </ScrollView>
        </View>

        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-start',marginLeft:15}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-back'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-end'}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
                <Icon
                  name='arrow-forward'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Next</Text>
            </View>
          </View>

        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
            <Icon
                name='home'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
            <Icon
                name='search'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
                name='favorite-border'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
            <Icon
                name='account-circle'
                color= 'orange' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'orange'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer:{
    flex:8,
    backgroundColor:"lightgrey",
    width:windowWidth
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  iconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'lightgrey'
  },
  fbiconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'orange'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  containerWrap: {
  marginTop:5,
  flex: 3,
  flexDirection: 'row',
  backgroundColor: 'white'
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'
  },
  redirectContainer:{
    backgroundColor:'white',flexDirection:'row',height: 120, width:windowWidth,borderColor:'lightgrey',borderBottomWidth:1
  },
  redirectTextContainer:{
    marginTop: 15,marginLeft:10,flex:0.7,alignItems:'flex-start',justifyContent:'flex-start'
  },
  redirectIconContainer:{
    marginTop: 15,marginRight:10,flex:0.3,alignItems:'flex-end',justifyContent:'flex-start'
  },
  redirectSocialContainer:{
    backgroundColor:'white',flexDirection:'row', width:windowWidth
  },
  other_btn:{
    flex:1,
    width: 40,
    backgroundColor: 'orange',
    height:50,
    flexDirection:'row'
  }
});
