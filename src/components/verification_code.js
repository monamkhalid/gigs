import React from 'react';
import { ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class Code extends React.Component {

  constructor(props){
    super(props);
    this.state={
      code1: '',
      code2: '',
      code3: '',
      code4: '',
      status: false
    }
  }

  componentDidMount(){
  }

  show_error_message = () =>{
    if(this.state.show_error){
      if(this.state.status){
        return(
          <Text style={{color: 'green'}}>{this.state.error_message}</Text>
        )
      }else{
        return(
          <Text style={{color: 'red'}}>{this.state.error_message}</Text>
        )
      }
    }
  }

  reset_password = () =>{
    try{
      fetch(BasePath+'api/user/reset', {
        method: 'POST',
        mode: 'same-origin',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: JSON.stringify({
          username: this.state.email,
        })
      })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({error_message: responseJson.result.message, status: responseJson.status, show_error: true})
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  form_validation = () => {
    if(this.state.email === ''){
      this.setState({error_message: 'All Fields are required', show_error: true})
    }else{
      this.reset_password()
    }
  }

  login = () =>{
    this.form_validation()
  }

  forgot_password = () =>{
    console.log("start Contest")
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.heading}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-70}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Forgot')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6,justifyContent:'flex-start',alignItems:'center'}}>
          </View>
          <View style={{flex:0.2}}></View>
        </View>
        <View style={{flex:9,justifyContent: 'center', alignItems: 'center'}}>
          <Content>
            <View style={{marginTop:10,flex:3,justifyContent: 'center', alignItems: 'center'}}>
              <Image
                style={{
                  height :100,
                  width: 100
                }}
                source={require('../../assets/message.png')}
              />
            </View>
            <View style={{flex:2,marginTop:10,justifyContent: 'flex-start', alignItems: 'center'}}>
              <Text style={{fontSize:18}}>Verification Code</Text>
              <Text style={{fontSize:12}}>Please enter your verification code sent to </Text>
              <Text style={{fontSize:12}}>your registered mobile number</Text>
            </View>
            <View style={{width: windowWidth-120,flex:4,marginTop:20,justifyContent: 'center', alignItems: 'center'}}>
                <View style={{flex:1.5, flexDirection:'row'}}>
                  <View style={{flex:0.4, height: 40,width:20, backgroundColor:'#EEEEEE',alignItems: 'center',justifyContent: 'center'}}>
                    <TextInput
                      onChangeText={value => this.setState({code1: value})}
                      value={this.state.code1}
                      keyboardType='phone-pad'
                      maxLength={1}
                      underlineColorAndroid='transparent' 
                    />
                  </View>
                  <View style={{flex:0.1}}>
                  </View>
                  <View style={{flex:0.4, height:40,width:20,backgroundColor:'#EEEEEE',alignItems: 'center',justifyContent: 'center'}}>
                    <TextInput
                      onChangeText={value => this.setState({code1: value})}
                      value={this.state.code1}
                      keyboardType='phone-pad'
                      maxLength={1}
                      underlineColorAndroid='transparent' 
                    />
                  </View>
                  <View style={{flex:0.1}}>
                  </View>
                  <View style={{flex:0.4,height: 40,width:20,backgroundColor:'#EEEEEE',alignItems: 'center',justifyContent: 'center'}}>
                    <TextInput
                      onChangeText={value => this.setState({code1: value})}
                      value={this.state.code1}
                      keyboardType='phone-pad'
                      maxLength={1}
                      underlineColorAndroid='transparent' 
                    />
                  </View>
                  <View style={{flex:0.1}}>
                  </View>
                  <View style={{flex:0.4,height: 40,width:20,backgroundColor:'#EEEEEE',alignItems: 'center',justifyContent: 'center'}}>
                    <TextInput
                      onChangeText={value => this.setState({code1: value})}
                      value={this.state.code1}
                      keyboardType='phone-pad'
                      maxLength={1}
                      underlineColorAndroid='transparent' 
                    />
                  </View>
                  <View style={{flex:0.1}}>
                  </View>
                </View>
                <View style={{flex:1.5}}>
                  <TouchableOpacity style={{justifyContent: 'center',alignItems:'center',marginTop:50,height: 50,backgroundColor: 'orange',width: windowWidth-70}} onPress={() => navigate('Verify')}>
                    <Text style={{color: 'white',fontSize: 18,fontWeight: 'bold'}}>Login</Text>
                  </TouchableOpacity>
                  <TouchableOpacity >
                    <Text style={{marginTop:5,textDecorationLine: 'underline',color: 'orange',fontSize: 14,textAlign: 'center'}}>Resend</Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
                </View>
            </View>
          </Content>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 2,
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  }

});
