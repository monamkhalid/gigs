import React from 'react';
import { FlatList,ImageBackground,AsyncStorage,Image, KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item,Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import { WP, HP } from './responsive';
import BaseUrl from '../../config/path.js';
import {loginUser} from "../actions/auth";
import {connect} from "react-redux";

var windowWidth = Dimensions.get('window').width
class Home extends React.Component {
  constructor(props){
    super(props);
    this.state={
      email: '',
      password: '',
      show_error: false,
      error_message: '',
      isLoading: false,
      data: [
        {id:1,name: "RV Repair",image:"",service:"3"},
        {id:2,name: "Electronic Repairs",image:"",service:"4"},
        {id:3,name: "RV Cleaning",image:"",service:"5"},
        {id:4,name: "RV Contractor",image:"",service:"6"},
        {id:5,name: "Nomad Emergency Financial Assistance",image:"",service:"7"},
        {id:6,name: "Medical Services",image:"",service:"7"},
        {id:7,name: "Eat Me!",image:"",service:"3"},
        {id:8,name: "Grooming",image:"",service:"2"}
      ]
    }
    // this.set_token();
    // this.login_request();
  }

  set_token = async() =>{
    var tokken = await AsyncStorage.getItem('token') 
    await this.setState({token: tokken})
    this.get_data()
  }
  

  get_data = async() => {
    var data = await [
      {id:1,name: "RV Repair",image:"",service:"3"},
      {id:2,name: "Electronic Repairs",image:"",service:"4"},
      {id:3,name: "RV Cleaning",image:"",service:"5"},
      {id:4,name: "RV Contractor",image:"",service:"6"},
      {id:5,name: "Nomad Emergency Financial Assistance",image:"",service:"7"},
      {id:6,name: "Medical Services",image:"",service:"7"},
      {id:7,name: "Eat Me!",image:"",service:"3"},
      {id:8,name: "Grooming",image:"",service:"2"}
    ];
    this.login_request()
    this.setState({data: data,isLoading: false})
  }

  show_error_message = () =>{
    if(this.state.show_error){
      return(
        <Text style={{color: 'red',textAlign: 'center'}}>{this.state.error_message}</Text>
      )
    }
  }


  login_request = () =>{
    console.log("Get Data")
    try{
      fetch(BasePath+'getservices', {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({data: responseJson.result,isLoading: false})
        
        }else{
          this.setState({isLoading: false, error_message: responseJson.result.message, show_error: true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }

  form_validation = () => {
    if(this.state.email === '' || this.state.password === ''){
      this.setState({isLoading: false,error_message: 'All Fields are required', show_error: true})
    }else{
      this.login_request()
    }
  }

  login = () =>{
    this.setState({isLoading : true})
    this.form_validation()
  }

  forgot_password = () =>{
    console.log("start Contest")
  }

  renderItem = ({ item, index}) =>{
    return (
      <TouchableOpacity key={item.id} style={{ justifyContent: 'center',alignItems: "center",width: windowWidth/2-15,height: 200,margin: 5,backgroundColor: '#fff',shadowOpacity: 0.15, elevation: 2}} onPress={() => this.props.navigation.push('Service',{id:item.id})}>
        <Image
          style={{
            height :'50%',
            width: '50%',
            resizeMode:'contain'
          }}
          source={require('../../assets/rocket.png')}
        />
        <Text style={{marginTop:10, marginHorizontal: 8}}>{item.name}</Text>
        <Text style={{marginTop:10,color:'orange'}}>( {item.service} services)</Text>
      </TouchableOpacity>
    )
  }

  show_list = () =>{
    if (this.state.isLoading){
      return(
        <View style={{flex:1,alignItems: 'center',justifyContent:'center'}}>
          <ActivityIndicator />
        </View>
      )
    }else{
      return(
        <FlatList
            numColumns={2}
            data={this.state.data}
            renderItem = {this.renderItem}
            keyExtractor={(item,index) => {return item.id}}
          />
      )
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    console.log(this.props);
    return (
      <View style={styles.container}>
        <View style={{flex:2,flexDirection:'row'}}>
          <View style={{flex:1.5,marginLeft:15,justifyContent:'center',alignItems:'flex-start'}}>
            <Icon
                name='menu'
                color= 'grey' />
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'flex-start'}}>
            <Image
              style={{
                height :100,
                width: 100
              }}
              source={require('../../assets/logo.png')}
            />
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'flex-end'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')} style={{flexDirection:'row',marginRight:10}}>
              <Text style={{color:'orange'}}>Profile</Text>
              <Icon
                  name='account-circle'
                  color= 'orange' />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{width: windowWidth,backgroundColor: 'orange', justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: 'white',fontSize: 12, marginVertical: 8, marginHorizontal: 5}}>"Connecting you to Profrssional service near you....Whereever you are!"</Text>
        </View>
        <View style={{marginVertical: 8,flexDirection:'row',width: windowWidth-30 }}>
          <View style={{flex:0.9,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity style={{flexDirection:'row',height:40,justifyContent:'center',alignItems:'center'}}>
              <View style={{flex:0.2}}>
                <Icon
                  name='place'
                  color= 'black' />
              </View>
              <View style={{flex:0.8}}>
                <Text style={{ color:'gray' }}>Use My Location</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{flex:1.1,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity style={{ flexDirection:'row',backgroundColor:'lightgrey',height:50,justifyContent:'center',alignItems:'center'}}>
              <View style={{flex:0.2}}>
                <Icon
                  name='place'
                  color= 'black' />
              </View>
              <View style={{flex:0.8}}>
                <Text style={{ color:'gray' }}>Change My Location</Text>
                <Text style={{fontSize: 9,color: 'orange',textAlign: 'left'}}>Input City,State Or Zip/Postal Code</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex:6.5}}>
          {this.show_list()}
        </View>
        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity>
            <Icon
                name='home'
                color= 'black' />
            </TouchableOpacity>
            <Text style={{fontSize:12,color:'black'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
            <Icon
                name='search'
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
                name='favorite-border'
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
              <Icon
                  name='account-circle'
                  color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    flex:1,
    justifyContent: 'flex-start', 
    alignItems: 'flex-end',
    flexDirection : 'row'
  },
  heading: {
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputField: {
    height: 50,
    width: windowWidth-80,
    marginTop: 10,
    backgroundColor: 'lightgrey'
  },
  login_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  signup_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'navy',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10,
    height: 50
  },
  icon:{
    height:50,
    width: 50
  }
});

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}
const mapDispatchToProps = dispatch => {
    return {
        // loginUser: (route,formData, additional) => dispatch(loginUser(route,formData,additional)),
        // setUserData: (data) => dispatch(loginUser(data)),
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);