import React from 'react';
import {
  AsyncStorage,
  Image,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  Alert
} from 'react-native';
import { Form, Label, Item, Input} from 'native-base';
import { CheckBox,Icon } from 'react-native-elements';
import { loginUser, setUserData } from "../../actions/auth";
import { connect } from 'react-redux';

var windowWidth = Dimensions.get('window').width;
class Login extends React.Component {
  constructor(props){
    super(props);
    this.state={
      email: 'hello3@gmail.com',
      password: '12341234',
      show_error: false,
      error_message: '',
      remember_me: false
    }
  }

  componentDidMount = async () =>{
    try {
      const data =  await AsyncStorage.getItem("userData");
      if(data){
        await this.props.setUserData(JSON.parse(data))
            .then(()=>{
              this.props.navigation.navigate('Home');
            });
      }
    } catch (error) {
      console.log(error.message);
    }

  };

  show_error_message = () =>{
    if(this.state.show_error){
      return(
        <Text style={{margin: 10,color: 'red',textAlign: 'center'}}>{this.state.error_message}</Text>
      )
    }
  };

  form_validation = () => {
    var re = /\S+@\S+\.\S+/;

    if(this.state.email === '' || this.state.password === ''){
      this.setState({error_message: 'All Fields are required', show_error: true});
      return false;
    }else     if(!re.test(this.state.email)){
      this.setState({error_message: 'Email is invalid', show_error: true});
      return false;
    }
    return true;

  };

  login = async () =>{
    const { email, password, remember_me} = this.state;
    if(this.form_validation()){
      const formDate = new FormData();
      formDate.append('email',email);
      formDate.append('password',password);
      await this.props.loginUser('login',formDate, {remember_me: remember_me})
          .then(async ()=>{
            if(this.props.auth.failed){
              ToastAndroid.show(this.props.auth.message,ToastAndroid.SHORT );
            }else {
              this.props.navigation.navigate('Home');
            }

          });
    }
  };

  forgot_password = () =>{
    console.log("start Contest")
  };

  render() {
    // console.log(this.props);
    const { auth } = this.props;
    const { navigate } = this.props.navigation;
    return (
      <ScrollView style={{width: windowWidth}}>
        <View style={styles.container}>
        <View style={{flex:3,height :160,justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={{
              height :130,
              width: 130
            }}
            source={require('../../../assets/logo.png')}
          />
        </View>
        <View style={{flex:1.5,height: 60,width: windowWidth-45,backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: 'white',fontSize: 22}}>Banner</Text>
        </View>
        <View style={{flex:1,marginTop: 10, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 22,textAlign: 'center'}}>Provider/Subscriber Login</Text>
        </View>

        <View style={{flex:5,justifyContent: 'center', alignItems: 'center'}}>
            {this.show_error_message()}
            <Form>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{color: 'grey'}} >Email</Label>
                <Input onChangeText={(text) => this.setState({ email: text })} />
              </Item>
              <Item style={{width:windowWidth-45}} floatingLabel >
                <Label style={{color: 'grey'}} >Password</Label>
                <Input onChangeText={(text) => this.setState({ password: text })} secureTextEntry={true}  />
              </Item>
              <View style={{flex:1,width:windowWidth-30,flexDirection: 'row'}}>
                <View style={{flex:0.5}}>
                  <CheckBox
                    title='Remember Me'
                    checked={this.state.checked}
                    onPress={() => this.setState({remember_me: !this.state.remember_me})}
                    containerStyle={{borderColor: 'white',backgroundColor: 'white'}}
                  />
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Forgot')} style={{flex:0.5, justifyContent:'center',alignItems: 'flex-end'}}>
                  <Text style={{fontWeight: 'bold', color:'orange'}}>Forgot Password</Text>
                </TouchableOpacity>
              </View>
            </Form>
        </View>
        <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center'}}>
          { auth.loading ?
            <View style={{justifyContent: 'center',alignItems: 'center'}}>
              <ActivityIndicator />
            </View>
            :
            <TouchableOpacity style={styles.login_btn} onPress={this.login}>
              <Text style={{fontWeight: 'bold', color: 'white',fontSize: 18}}>LOGIN</Text>
            </TouchableOpacity>
          }
          <View style={{flex:0.5}}>
          </View>
        </View>
        <View style={{width: windowWidth-45, marginTop:15,flex:1,flexDirection:'row',justifyContent: 'center',alignItems: 'center'}}>
          <View style={{flex: 0.45,borderColor: 'lightgrey',borderWidth:1}}></View>
          <View style={{flex: 0.1,justifyContent: 'center',alignItems: 'center'}}><Text style={{color:'grey'}}>OR</Text></View>
          <View style={{flex: 0.45,borderColor: 'lightgrey',borderWidth:1}}></View>
        </View>
        <View style={{flex:2.5, alignItems: 'center', justifyContent: 'center'}}>
          <View style={{flex:1, marginTop: 10,alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity style={styles.signup_btn} onPress={() => this.props.navigation.push('Home')}>
              <View style={{flex:0.1,alignItems:'flex-end',justifyContent:'center'}}>
                <Icon
                  type="font-awesome"
                  name='facebook'
                />
              </View>
              <View style={{flex:0.9,alignItems:'center',justifyContent:'center'}}>
                <Text style={{color: 'white',fontSize: 18}}>Continue with Facebook</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{flex:1.5,marginTop:20,marginBottom:10,justifyContent:'center',alignItems: 'center'}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{color:'grey'}}>Not a subscriber? </Text>
              <TouchableOpacity onPress={() => this.props.navigation.push('Signup')} >
                <Text style={{color:'orange'}}>Subscribe Now</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginTop:8,flexDirection: 'row'}}>
              <Text style={{color:'grey'}}>Want to list your service? </Text>
              <TouchableOpacity onPress={() => this.props.navigation.push('ProviderProfile')}>
                <Text style={{color:'orange'}}>Subscribe Here</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginTop:8,flexDirection: 'row'}}>
              <Text style={{color:'grey'}}>Do you help fellow Nemads for free? </Text>
              <TouchableOpacity onPress={() => this.props.navigation.push('HelpingHand')}>
                <Text style={{color:'orange'}}>Subscribe Here</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    flex:1,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    flexDirection : 'row'
  },
  heading: {
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputField: {
    height: 50,
    width: windowWidth-80,
    marginTop: 10,
    backgroundColor: 'lightgrey'
  },
  login_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  signup_btn:{
    flex:1,
    borderRadius:5,
    flexDirection:'row',
    width: windowWidth-80,
    backgroundColor: '#3c5a99',
    height:50
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10,
    height: 50
  },
  icon:{
    height:50,
    width: 50
  }
});

function mapStateToProps(state) {
  return {
    auth: state.auth
  }
}
const mapDispatchToProps = dispatch => {
  return {
    loginUser: (route,formData, additional) => dispatch(loginUser(route,formData,additional)),
    setUserData: (data) => dispatch(loginUser(data)),
  }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
