import React from 'react';
import { TouchableWithoutFeedback,Modal,ImageBackground,AsyncStorage,Image, KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left,Picker } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import { ImagePicker,Permissions } from 'expo';
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class HelpingHand extends React.Component {
  constructor(props){
    super(props);
    this.state={
      email: '',
      password: '',
      last_name:'',
      first_name:'',
      phone:'',
      details: '',
      show_error: false,
      error_message: '',
      rv:'',
      rig: undefined,
      isLoading: false,
      base64_str: null,
      local_image_url: null,
      category : undefined,
      service : undefined,
      modalVisible: false,
      selected_social: '',
      twitter_url: '',
      facebook_url: '',
      in_url: '',
      youtube_url: '',
      insta_url: '',
      show_category: false,
      show_service: false
    }
    this.get_categories()
  }

  componentDidMount(){
  }

  get_categories = () =>{
    try{
      fetch(BasePath+'getcategory', {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({catdata: responseJson.result,show_category:true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }


  show_error_message = () =>{
    if(this.state.show_error){
      return(
        <Text style={{mrgin:15,color: 'red',textAlign: 'center'}}>{this.state.error_message}</Text>
      )
    }
  }

  storedate = async (response) =>{
    const { navigate } = this.props.navigation;

    console.log('**************************')
    console.log(response)
    try{
      await AsyncStorage.setItem('token', response.token);
      await AsyncStorage.setItem('role', response.role);
      this.props.navigation.push('Home')
    }catch(e){
      console.log(e)
    }

  }

  _pickImage = async () => {
    const { status: cameraRollPerm } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if (cameraRollPerm === 'granted') {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
        base64: true
      });
      if (!result.cancelled) {
        console.log(result)
        // filename = result.uri.substring(result.uri.lastIndexOf('/')+1);
        // this.uploadImage(result.uri, filename)
        this.setState({ base64_str: result.base64 });
        this.setState({ local_image_url: result.uri, show_image: true });
        
      }
    }
  }

  show_modal_data = () =>{
    return(
        <View style={styles.modal_container}>
          <View style={styles.modal}>
            <View style={{flex:1,flexDirection:'row'}}>
              <View style={{marginLeft:15,flex:0.5,alignItems:'flex-start',justifyContent:'center'}}>
                <Image
                  style={{
                    height :25,
                    width: 25
                  }}
                  source={require('../../assets/home-icon.png')}
                />
              </View>
              <View style={{flex:0.5,alignItems:'center',justifyContent:'flex-end'}}>
                  <Image
                    style={{
                      height :50,
                      width:100
                    }}
                    source={require('../../assets/logo.png')}
                  />
              </View>
              <View style={{marginRight:15,flex:0.5,alignItems:'flex-end',justifyContent:'center'}}>
                <Icon
                  name='highlight-off'
                  containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                  color= 'grey' />
              </View>
            </View>
            <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
              <Image
                style={{
                  height :60,
                  width: 60
                }}
                source={require('../../assets/tick.png')}
              />
              <Text style={{marginTop:10,fontSize:18,color:'orange'}}>Thanks</Text>
            </View>
            <View style={{flex:2}}>
              <View style={{flex:4.5,justifyContent: 'center',alignItems:'center'}}>
                <Image
                  style={{
                    height :120,
                    width: windowWidth-30
                  }}
                  source={require('../../assets/refer.png')}
                />
              </View>  
            </View>  
          </View>
        </View>
      );
  }

  show_url_input = () =>{
    if (this.state.selected_social === 'facebook'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ facebook_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'twitter'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ twitter_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'insta'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ insta_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'in'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ in_url: text })} />
        </Item>
      )
    }

    if (this.state.selected_social === 'youtube'){
      return(
        <Item style={{width:windowWidth-90}} floatingLabel>
          <Label style={{fontSize: 14,color: 'grey'}} >Paste Url</Label>
          <Input onChangeText={(text) => this.setState({ youtube_url: text })} />
        </Item>
      )
    }
  }

  facebook_social = () =>{
    if(this.state.facebook_url == ''){
      return(
        <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'facebook',modalText: 'Facebook Url',modalVisible: true})}>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Icon
              type="font-awesome"
              name='facebook'
              iconStyle={{color:'white'}}
            />
          </View>
          <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
            <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Facebook</Text>
          </View>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Image
              style={{
                height :25,
                width: 25
              }}
              source={require('../../assets/cross.png')}
            />
          </View>
        </TouchableOpacity>
      )
    }else{
      return(
        <View style={styles.otherdis_btn}>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Icon
              type="font-awesome"
              name='facebook'
            />
          </View>
          <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
            <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Facebook</Text>
          </View>
          <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
            <Image
              style={{
                height :25,
                width: 25
              }}
              source={require('../../assets/cross.png')}
            />
          </View>
        </View>
      )
    }
  }

  twitter_social = () =>{
      if(this.state.twitter_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'twitter',modalText: 'Twitter Url',modalVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='twitter'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Twitter</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='twitter'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Twitter</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    insta_social = () =>{
      if(this.state.insta_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'insta',modalText: 'Instagram Url',modalVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='instagram'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Instagram</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn} >
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='instagram'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Instagram</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    in_social = () =>{
      if(this.state.in_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'in',modalText: 'Linkedin Url',modalVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='linkedin'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Linkedin</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='linkedin'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Linkedin</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

    youtube_social = () =>{
      if(this.state.youtube_url == ''){
        return(
          <TouchableOpacity style={styles.other_btn} onPress={() => this.setState({selected_social: 'youtube',modalText: 'Youtube Url',modalVisible: true})}>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                iconStyle={{color:'white'}}
                name='youtube'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Youtube</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </TouchableOpacity>
        )
      }else{
        return(
          <View style={styles.otherdis_btn} >
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Icon
                type="font-awesome"
                name='youtube'
              />
            </View>
            <View style={{flex:0.4,alignItems:'center',justifyContent:'center'}}>
              <Text style={{ color: 'white',fontSize:13,fontWeight:'bold'}}>Youtube</Text>
            </View>
            <View style={{flex:0.3,alignItems:'center',justifyContent:'center'}}>
              <Image
                style={{
                  height :25,
                  width: 25
                }}
                source={require('../../assets/cross.png')}
              />
            </View>
          </View>
        )
      }
    }

  show_modal_data = () =>{
    return(
      <View style={styles.modal_container}>
        <View style={styles.modal}>
          <View style={{marginRight:10,flex:1,alignItems:'flex-end',justifyContent:'center'}}>
            <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
              <Icon
                name='highlight-off'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
          </View>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontWeight:'bold',fontSize:18}}>{this.state.modalText}</Text>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <Form>
              {this.show_url_input()}
            </Form>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
          </View>
        </View>
      </View>
    );
  }


  login_request = () =>{
    console.log(this.state.email)
    console.log(this.state.password)
    try{
      fetch(BasePath+'signup', {
        method: 'POST',
        mode: 'same-origin',
        body: JSON.stringify({
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          phone: this.state.phone,
          email: this.state.email,
          password: this.state.password,
          rv: this.state.rv,
          rig: this.state.rig,
          facebook: this.state.facebook_url,
          insta: this.state.insta_url,
          youtube: this.state.youtube_url,
          linkedin: this.state.in_url,
          twitter: this.state.twitter_url,
          profileimg: this.state.base64_str,
          category: this.state.category,
          service: this.state.service,
          detail: this.state.detail,
          role: 2

        })
      })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson)
        if(responseJson.status){
          this.setState({isLoading: false})
          this.storedate(responseJson)
        
        }else{
          this.setState({isLoading: false, error_message: responseJson.result.message, show_error: true})
        }
      })
      .catch((error) =>{
        console.log(error);
      })
    }catch(e){
      console.log('error', e);
    }
  }

  onValueChange(value: string) {
    this.setState({
      rig: value
    });
  }

  onServiceChange(value: string) {
      this.setState({
        service: value
      });
  }

  onCategoryChange(value: string) {
      this.setState({
        category: value
      });
      this.get_services(value)
  }

  get_services = (value) =>{
    console.log(value)
    try{
      fetch(BasePath+'getservice?sid='+value, {
        method: 'GET',
        mode: 'same-origin',
        
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.status){
          this.setState({servdata: responseJson.result,show_service:true})
        }
      })
      .catch((error) =>{
        console.log(error);
      });
    }catch(e){
      console.log('error', e);
    }
  }


  form_validation = () => {
    if(this.state.email === '' || this.state.password === '' || this.state.username === ''){
      this.setState({isLoading: false,error_message: 'All Fields are required', show_error: true})
    }else if(!this.state.base64_str){
      this.setState({isLoading: false,error_message: 'Please Pick An Image', show_error: true})
    }else{
      this.login_request()
    }
  }

  showcat = () =>{
    if(this.state.show_category){
      return this.state.catdata.map((item, key) =>{
        return(
          <Picker.Item key={item.id} label={item.name} value={item.id} />
        )
      })
    }else{
      return null
    }
  }

  showser = () =>{
    if(this.state.show_service){
      return this.state.servdata.map((item, key) =>{
        return(
          <Picker.Item key={item.id} label={item.name} value={item.id} />
        )
      })
    }else{
      return null
    }
  }



  signup = async() =>{
    this.setState({isLoading : true})
    await this.set_rv()
    this.form_validation()
  }

  set_rv = () =>{
    if(this.state.isNoChecked){
      this.setState({rv: 'no'})
    }
    if(this.state.isFullChecked){
      this.setState({rv: 'full'})
    }
    if(this.state.isPartChecked){
      this.setState({rv: 'part'})
    }
  }


  forgot_password = () =>{
    console.log("start Contest")
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
        <View style={styles.container}>
          <Modal
            animationType="none"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => 
              this.setState({modalVisible:false})
            }
          >
            <TouchableWithoutFeedback  onPress={() => this.setState({modalVisible:false})} >
              {this.show_modal_data()}
            </TouchableWithoutFeedback>
          </Modal>
          <View style={{flex:2,flexDirection:'row'}}>
            <View style={{flex:1.2,justifyContent: 'flex-end', alignItems: 'flex-end'}}>
              <Image
                style={{
                  height :60,
                  width: 100
                }}
                source={require('../../assets/logo.png')}
              />
            </View>
            <View style={{flex:0.8,width:windowWidth,marginRight:15,justifyContent: 'center', alignItems: 'flex-end'}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Login')}>
                <Icon
                  name='highlight-off'
                  color= 'grey' />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex:1.3,height: 30,width: windowWidth-45,backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{color: 'white',fontSize: 22}}>Banner</Text>
          </View>
          <View style={{flex:0.5,marginTop: 10, justifyContent: 'center', alignItems: 'center',flexDirection: 'row'}}>
            <Text style={{fontSize: 10}}>Already subscribed? Click here to </Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
              <Text style={{textDecorationLine: 'underline',color: 'orange',fontSize: 10,textAlign: 'center'}}>Sign in</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.7,marginTop:10,justifyContent:'flex-start',alignItems:'center'}}>
            <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
            <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize: 10,fontWeight:'bold'}}>Helping Hands</Text>
            </View>
            <View>
              <Text style={{fontSize: 10,fontWeight:'bold'}}>Provider Profile</Text>
            </View>
            <View style={{marginTop:3,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
          </View>
          <View style={{flex:8,width: windowWidth}}>
            <ScrollView>
            <View style={{marginTop:10,justifyContent: 'center',alignItems: 'center',}}>
              <View style={{justifyContent: 'center',alignItems: 'center', width: 80,height: 80,borderRadius:80/2,backgroundColor:'#F0F0F0',overflow: 'hidden'}}>
                { !this.state.show_image ? 
                  <Image
                    style={{height:40,width:40}}
                    source={require('../../assets/icon.png')}
                  /> : 
                   <Image source={{ uri: this.state.local_image_url }} style={{ width: 40, height: 40 }} />
                }
                
              </View>
              <TouchableOpacity  onPress={this._pickImage}>
                <Icon active name='add-circle' iconStyle={{position: 'relative',left: 30,top:-30,color:'orange'}}/>
              </TouchableOpacity>
              <Text style={{position:'relative',top:-20,fontSize:10,textAlign: 'center'}}>Upload Profile Picture</Text>
            </View>
            <View style={{position:'relative',top:-30,flex:5,justifyContent: 'center', alignItems: 'center'}}>
              <Content>
                {this.show_error_message()}
                <Form>
                <View style ={{flexDirection: 'row'}}>
                  <Item style={{width:(windowWidth-50)/2}} floatingLabel>
                    <Label style={{fontSize: 14,color: 'grey'}} >First Name</Label>
                    <Input onChangeText={(text) => this.setState({ first_name: text })} />
                  </Item>
                  <Item style={{width:(windowWidth-50)/2}} floatingLabel>
                    <Label style={{fontSize: 14,color: 'grey'}} >Last Name</Label>
                    <Input onChangeText={(text) => this.setState({ last_name: text })} />
                  </Item>
                </View>  
                  <Item style={{width:windowWidth-45}} floatingLabel>
                    <Label style={{fontSize: 14,color: 'grey'}} >Mobile Number</Label>
                    <Input onChangeText={(text) => this.setState({ phone: text })} />
                  </Item>
                  <Item style={{width:windowWidth-45}} floatingLabel>
                    <Label style={{fontSize: 14,color: 'grey'}} >Email</Label>
                    <Input onChangeText={(text) => this.setState({ email: text })} />
                  </Item>
                  <Item style={{width:windowWidth-45}} floatingLabel >
                    <Label style={{fontSize: 14,color: 'grey'}} >Password</Label>
                    <Input onChangeText={(text) => this.setState({ password: text })} secureTextEntry={true} />
                  </Item>
                  <View style={{marginLeft:10,marginTop:15,flex:1}}>
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Service Provider</Text>
                  </View>
                  <View style={{marginTop:15,borderBottomWidth:1,borderBottomColor: 'lightgrey',width: windowWidth-30,height: 40}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={  <Icon active name='arrow-drop-down' color= 'orange' />}
                      placeholder="Category"
                      placeholderStyle={{color: 'grey',fontSize: 14}}
                      placeholderTextColor='grey'
                      selectedValue={this.state.category}
                      onValueChange={this.onCategoryChange.bind(this)}
                    >
                      {this.showcat()}
                    </Picker>
                  </View>
                  <View style={{marginTop:15,borderBottomWidth:1,borderBottomColor: 'lightgrey',width: windowWidth-30,height: 40}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={  <Icon active name='arrow-drop-down' color= 'orange' />}
                      placeholder="Service"
                      placeholderStyle={{color: 'grey',fontSize: 14}}
                      placeholderTextColor='grey'
                      selectedValue={this.state.service}
                      onValueChange={this.onServiceChange.bind(this)}
                    >
                       {this.showser()}
                    </Picker>
                  </View>
                  <View style={{marginLeft:10,marginTop:5,flex:1}}>
                    <Text style={{fontSize:12,fontWeight:'bold'}}>RV Nomad?</Text>
                  </View>
                  <View style={{flex:1,width:windowWidth-10,flexDirection: 'row'}}>
                    <View style={{flex:0.5}}>
                      <CheckBox
                        checked={this.state.isNoChecked}
                        checkedColor='orange'
                        uncheckedColor='orange'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        title="No"
                        textStyle={{fontSize:10,color: 'grey'}}
                        onPress={() => this.setState({isNoChecked: !this.state.isNoChecked,isFullChecked:false,isPartChecked:false})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                    <View style={{flex:0.5}}>
                      <CheckBox
                        checked={this.state.isFullChecked}
                        checkedColor='orange'
                        uncheckedColor='orange'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        title="Full Time"
                        textStyle={{fontSize:10,color: 'grey'}}
                        onPress={() => this.setState({isFullChecked: !this.state.isFullChecked,isNoChecked: false,isPartChecked:false})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                    <View style={{flex:0.5}}>
                      <CheckBox
                        checked={this.state.isPartChecked}
                        checkedColor='orange'
                        uncheckedColor='orange'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        title="Part Time"
                        textStyle={{fontSize:10,color: 'grey'}}
                        onPress={() => this.setState({isPartChecked: !this.state.isPartChecked,isNoChecked: false, isFullChecked: false})}
                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                      />
                    </View>
                  </View>
                  <View style={{borderBottomWidth:1,borderBottomColor: 'lightgrey',width: windowWidth-30,height: 40}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={  <Icon active name='arrow-drop-down' color= 'orange' />}
                      placeholder="Type of Rig"
                      placeholderStyle={{color: 'grey',fontSize: 14}}
                      placeholderTextColor='grey'
                      selectedValue={this.state.rig}
                      onValueChange={this.onValueChange.bind(this)}
                    >
                      <Picker.Item label="Class A" value="0" />
                      <Picker.Item label="Class B" value="1" />
                      <Picker.Item label="Class C" value="2" />
                      <Picker.Item label="Super C" value="3" />
                      <Picker.Item label="Sprinter Chassis" value="4" />
                      <Picker.Item label="5th Wheel" value="5" />
                      <Picker.Item label="Travel Trailer" value="6" />
                      <Picker.Item label="Skoolie" value="7" />
                      <Picker.Item label="Toy Hauler" value="8" />
                      <Picker.Item label="Truck Conversion" value="9" />
                      <Picker.Item label="Car Conversion" value="10" />
                    </Picker>
                  </View>
                  <View style={{marginLeft:10,marginTop:15,flex:1}}>
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Share Media Links</Text>
                  </View>
                  <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                    {this.facebook_social()}
                    <View style={{width: 5}}></View>
                    {this.twitter_social()}
                  </View>
                  <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                    {this.insta_social()}
                    <View style={{width: 5}}></View>
                    {this.in_social()}
                  </View>
                  <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center',flexDirection:'row'}}>
                    {this.youtube_social()}
                    <View style={{flex:1,width:40}}>
                    </View>
                  </View>
                  <View style={{marginTop:15,flex:1}}>
                    <Item style={{width:windowWidth-45}} floatingLabel >
                      <Label style={{fontSize: 14,color: 'grey'}} >Share a little bit about your Services offered</Label>
                      <Input onChangeText={(text) => this.setState({ detail: text })} />
                    </Item>
                  </View>
                </Form>
              </Content>
            </View>
            <View style={{flex:1.5,marginTop: 10,justifyContent: 'center',alignItems: 'center'}}>
              { this.state.isLoading ?
                <View style={{justifyContent: 'center',alignItems: 'center'}}>
                  <ActivityIndicator />
                </View> 
                :
                <TouchableOpacity style={styles.login_btn} onPress={this.signup}>
                  <Text style={{fontWeight: 'bold', color: 'white',fontSize: 18}}>Subscribe</Text>
                </TouchableOpacity>
              }
              <View style={{flex:0.5,height:20}}>
              </View>
            </View>
            </ScrollView>
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    flex:1,
    justifyContent: 'flex-start', 
    alignItems: 'flex-end',
    flexDirection : 'row'
  },
  heading: {
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputField: {
    height: 50,
    width: windowWidth-80,
    marginTop: 10,
    backgroundColor: 'lightgrey'
  },
  login_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  other_btn:{
    flex:1,
    width: 40,
    backgroundColor: 'orange',
    height:50,
    flexDirection:'row'
  },
  otherdis_btn:{
    flex:1,
    width: 40,
    backgroundColor: 'lightgrey',
    height:50,
    flexDirection:'row'
  },
  signup_btn:{
    flex:1,
    width: windowWidth-80,
    backgroundColor: 'navy',
    alignItems: 'center',
    justifyContent: 'center',
    height:50
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10,
    height: 50
  },
  icon:{
    height:50,
    width: 50
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'}
});