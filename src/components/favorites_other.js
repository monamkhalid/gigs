import React from 'react';
import { ListView,TouchableWithoutFeedback,Modal,ImageBackground,  Image,KeyboardAvoidingView,ScrollView,TextInput,ActivityIndicator,TouchableOpacity,Dimensions,StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Label, Item, Input, Button, Left } from 'native-base';
import { CheckBox,Icon } from 'react-native-elements'
import BaseUrl from '../../config/path.js';

var windowWidth = Dimensions.get('window').width
export default class FavoriteOther extends React.Component {

  constructor(props){
    super(props);
    this.state={
      search: true
    }
    this.populateList()
  }

  componentDidMount(){
  }

  populateList() {
    var dataSource=
    [ 
      {id:1,email: 'ahtasham@email.com',show_image:true,duration:'Now',name:'Bobs RV Repair',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:2,email: 'ahtasham1@email.com',show_image:false,duration:'1 days ago',name:'OHN RV Repair',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:3,email: 'ahtasham2@email.com',show_image:true,duration:'2 days ago',name:'Bobs RV Solar',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'},
      {id:4,email: 'ahtasham3@email.com',show_image:false,duration:'3 days ago',name:'Ohm RV Solar',detail:'They understood what i needed and delivered it quickly, always with an amazing attitude.He Understood what i needed and delivered it quickly 5 stars are not enough'}
    ]
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state.dataSource = ds.cloneWithRows(dataSource);
    this.state.db = dataSource
  };
  

  search_data = () =>{
    if (this.state.search){
      return(
        <View style={{flex:7}}>
          <View style={{flex:3,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :100,
                width: 180
              }}
              source={require('../../assets/logo-with-text.png')}
            />
          </View>
          <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:20,fontWeight:'bold',color:'orange'}}>Search for a professional</Text>
            <Text style={{fontSize:20,fontWeight:'bold',color:'orange'}}>Provider near you</Text>
          </View>
          <View style={{flex:2}}>
            <Form>
              <Item style={{width:windowWidth-45}} floatingLabel>
                <Label style={{fontSize: 10,color: 'grey'}} >(Search by Name, Type of Services, Market or Zip Code)</Label>
                <Input />
              </Item>
              <Icon
                name='search'
                size={40}
                iconStyle={{position:'relative',top:-35}}
                containerStyle={{width:windowWidth-45,justifyContent:'flex-end',alignItems:'flex-end'}}
                color= 'orange' />
            </Form>
          </View>
          <View style={{flex:1}}>
          </View>
        </View>
      );
    }else{
      return(
        <View style={{flex:7}}>
          <View style={{flex:3.5,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :130,
                width: 150
              }}
              source={require('../../assets/sorrynotfound.png')}
            />
          </View>
          <View style={{flex:2.5,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:20,fontWeight:'bold'}}>Aw Shucks!</Text>
            <Text style={{fontSize:16}}>There are no result found.</Text>
            <Text style={{fontSize:16}}>Please search using another search word.</Text>
          </View>
          <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity style={{height:50,width:windowWidth-90,backgroundColor:'orange',justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:16,color:'white'}}>Search Again</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.5}}>
          </View>
        </View>
      );
    }
  }

  show_icon = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-end',alignItems:'center'}}>
          <Icon
            name='favorite'
            containerStyle={{alignItems:'flex-start'}}
            size={40}
            />
        </View>
      )
    }else{
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
          <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
          <Icon
            name='highlight-off'
            size={30}
            containerStyle={{alignItems:'flex-end'}}
            color= 'grey' />
            </TouchableOpacity>
        </View>
      )
    }
  }

  show_text = () =>{
    if (this.state.search){
      return(
        <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
          <View style={{width:70,borderWidth:1,borderColor:'lightgrey',justifyContent:'center',alignItems:'center'}}></View>
          <View><Text style={{fontSize: 14}}>Favotites</Text></View>
          <View style={{marginTop:2,width:70,borderWidth:1,borderColor:'lightgrey'}}></View>
        </View>
      );
    }else{
      return null;
    }
  }
  
  _renderRow(follow, sectionID, rowID){
    return(
      <View style={styles.listContainer}>
        <View style={{height:100,flexDirection: 'row',flex:8, borderColor:'lightgrey',borderBottomWidth:1,backgroundColor:'white',width:windowWidth}}>
          <View style={{flex: 0.3,justifyContent:'center',alignItems:'center'}}>
            <Image
              style={{
                height :50,
                width: 50
              }}
               source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
            />
          </View>
          <View style={{flex: 0.7,justifyContent:'center',alignItems:'flex-start'}}>
            <View style={{flexDirection:'row'}}>
              <Text style={{fontWeight:'bold'}}>{follow.name}</Text>
              <Text style={{marginLeft: 5,fontWeight:'bold',color:'grey',fontSize:10}}>{follow.duration}</Text>
              <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',alignItems:'flex-end',marginRight:15}}>
                <Icon
                  name='star'
                  size={12}
                />
                <Icon
                  name='star'
                  size={12}
                />
                <Icon
                  name='star'
                  size={12}
                />
                <Icon
                  name='star'
                  size={12}
                />
                <Icon
                  name='star'
                  size={12}
                />
              </View>
            </View>
            
            <Text style={{fontSize:10,color: 'grey',marginTop:5}}>{follow.detail}</Text>
          </View>
        </View>
      </View>
    )
  }

  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        
        <View style={styles.heading}>
          <View style={{flex:0.85,justifyContent:'flex-end'}}>
          <Image
            style={{
              height :60,
              width: 60
            }}
            source={require('../../assets/logo.png')}
          />
          </View>
          <View style={{flex:0.8}}>
            {
              this.show_icon()
            }
          </View>
          <View style={{flex:0.8,width:windowWidth,marginRight:15,justifyContent: 'center', alignItems: 'flex-end'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
              name='highlight-off'
              size={30}
              color= 'grey' />
              </TouchableOpacity>
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
              <Icon
                name='arrow-back'
                containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Previous</Text>
          </View>
          <View style={{flex:0.6}}>
            {this.show_text()}
          </View>
          <View style={{flex:0.2,justifyContent: 'flex-start',alignItems: 'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
              <Icon
                name='arrow-forward'
                containerStyle={{}}
                color= 'grey' />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Next</Text>
          </View>
        </View>
        <View  style={{flex:0.5,justifyContent:'center', alignItems: 'center',flexDirection:'row'}}>
          <Text style={{color:'orange',fontWeight: 'bold'}}>ALL</Text>
          <Icon
            name='arrow-drop-down'
            containerStyle={{justifyContent: 'center',alignItems: 'flex-start'}}
            color= 'orange' />
        </View>
        <View style={{flex:7.5}}>
          <View style={{flex:0.3,backgroundColor:'lightgrey'}}></View>
          <View style={{flex:1.4,flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>
            <View style={{flexDirection:'row',marginTop:5,marginLeft:10}}>
                <View style={{flexDirection:'row',height:35,width:70,justifyContent:'center',alignItems:'center',backgroundColor:'lightgreen',borderRadius:5}}>
                  <Icon
                    name='star'
                    size= {12} />
                  <Text style={{fontSize:15,fontWeight:'bold',marginLeft:10,color:'white'}}>5.0</Text>
                </View>
                <Text style={{marginLeft:5,marginTop:10,fontSize:15,color: 'grey',fontWeight:'bold'}}>21 Reviews</Text>
              </View>
          </View>
          <View style={{flex:4.8,backgroundColor:'lightgrey'}}>
            <ListView
              style={{paddingVertical:20,width:'100%'}}
              dataSource={this.state.dataSource}
              renderRow={this._renderRow.bind(this)}
            />
          </View>
          <View style={{flex:1,flexDirection:'row',width: windowWidth-40}}>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-start',marginLeft:15}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
                <Icon
                  name='arrow-back'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Previous</Text>
            </View>
            <View style={{flex:0.5,justifyContent: 'flex-end',alignItems: 'flex-end'}}>
              <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
                <Icon
                  name='arrow-forward'
                  color= 'grey' />
              </TouchableOpacity>
              <Text style={{fontSize: 10}}>Next</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection:'row',flex:1.5,justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Home')}>
            <Icon
                name='home'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>HOME</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Search')}>
            <Icon
                name='search'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>SEARCH</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Favorite')}>
            <Icon
                name='favorite-border'
                color= 'orange' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'orange'}}>FAVORITES</Text>
          </View>
          <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.push('Profile')}>
            <Icon
                name='account-circle'
                color= 'grey' />
                </TouchableOpacity>
            <Text style={{fontSize:12,color:'grey'}}>PROFILE</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer:{
    flex:8,
    backgroundColor:"lightgrey",
    width:windowWidth
  },
  container: {
    flex: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 1.5,
    flexDirection: 'row',
    width: windowWidth-20,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  iconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'lightgrey'
  },
  fbiconContainer:{
    borderRadius: 30/2,
    height:30,
    width:30,
    overflow: 'hidden',
    marginLeft:5,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'orange'
  },
  inputField: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingLeft: 10,
    width: windowWidth-45,
    marginHorizontal: 20,
    
  },
  contest_button:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B0F6',
    width: windowWidth-45,
    marginTop: 15,
    borderRadius:10
  },
  icon:{
    height: 50,
    width: 50  
  },
  modal:{
    height:300,
    width: windowWidth-40 ,
    backgroundColor:'white',
    borderRadius: 10
  },
  containerWrap: {
  marginTop:5,
  flex: 3,
  flexDirection: 'row',
  backgroundColor: 'white'
  },
  modal_container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040'}
});
